/*
 * grant permission to some user
 *
 * grant all on myth.* to `root`@`localhost` identified by '';
 */


/*
 * create database myth
 *
 */
create database sanguo_manager default charset utf8 collate utf8_general_ci;

GRANT ALL ON sanguo_manager.* TO 'mythtest'@'172.17.36.%' IDENTIFIED BY 'jkY5qmGKVcRs4nST';