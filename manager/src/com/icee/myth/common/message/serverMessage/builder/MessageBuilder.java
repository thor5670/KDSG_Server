/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.common.message.serverMessage.builder;

import com.icee.myth.common.message.serverMessage.Message;
import com.icee.myth.common.message.serverMessage.Message.MessageType;
import com.icee.myth.common.message.serverMessage.SimpleMessage;

/**
 *
 * @author liuxianke
 */
public class MessageBuilder {

    public static Message buildHeartbeatMessage() {
        return new SimpleMessage(MessageType.ALL_HEARTBEAT);
    }

}
