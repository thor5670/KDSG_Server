/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.common.channelContext;

import static com.icee.myth.utils.Consts.*;

/**
 * 为了防止channel为null时抛出nullpointer exception，尽量不将channel设为null
 * @author liuxianke
 */
public class HeartbeatChannelContext extends ChannelContext {

    private int channelLife = FIRST_HEARTBEAT_NUM;  // 心跳倒计数

    public int heartbeat() {
        return --channelLife;
    }

    public void restoreHeartbeat() {
        channelLife = NORMAL_HEARTBEAT_NUM;
    }

}
