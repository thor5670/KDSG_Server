/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icee.myth.common.encoder;

import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.MessageLite;
import com.icee.myth.common.channelBuffer.NewChannelBuffers;
import com.icee.myth.common.protobufMessage.ProtobufMessage;
import com.icee.myth.utils.LogConsts;
import com.icee.myth.utils.MLogger;
import com.icee.myth.utils.StackTraceUtil;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
//import org.jboss.netty.channel.ChannelPipelineCoverage;
import org.jboss.netty.handler.codec.oneone.OneToOneEncoder;

import java.io.IOException;
import java.nio.ByteOrder;
import java.util.ArrayList;

import static org.jboss.netty.buffer.ChannelBuffers.wrappedBuffer;

/**
 * TypeLengthFieldProtobufEncoder用于编码除GW与Map间的服务器间消息
 * @author chencheng
 */
//@ChannelPipelineCoverage("all")
public class Type4BytesLengthFieldProtobufEncoder extends OneToOneEncoder {

    public Type4BytesLengthFieldProtobufEncoder() {
        super();
    }

/*MessageToMessageEncoder抽象编码器（netty4的类，netty3叫OneToOneEncoder类）将一个POJO对象编码成另一个对象，
以HTTP+XML协议为例，它的一种实现方式是：先将POJO对象编码成XML字符串，
再将字符串编码为HTTP请求或者应答消息。对于复杂协议，往往需要经历多次编码，
为了便于功能扩展，可以通过多个编码器组合来实现相关功能。用户的解码器继承MessageToMessageEncoder解码器，
实现void encode(Channel HandlerContext ctx, I msg, List<Object> out)方法即可。
注意，它与MessageToByteEncoder的区别是输出是对象列表而不是ByteBuf。
* */

    @Override
    protected Object encode(
            ChannelHandlerContext ctx, Channel channel, Object msg) throws Exception {
        if (msg instanceof ProtobufMessage) {
            return encodeProtobufMessage(channel, (ProtobufMessage)msg);
        } else if (msg instanceof ArrayList) {
            ArrayList<Object> msgs = (ArrayList<Object>)msg;
            int size = msgs.size();
            for(int i=0; i<size; i++) {
                Object obj = msgs.get(i);
                if (!(obj instanceof ChannelBuffer)) {
                    msgs.set(i, encodeProtobufMessage(channel, (ProtobufMessage)msgs.get(i)));
                }
            }
            return NewChannelBuffers.wrappedBuffer(msgs);
        } else {
            return msg;
        }
    }

    private Object encodeProtobufMessage(Channel channel, ProtobufMessage pm) {
        if (pm.payload instanceof MessageLite) {
            MessageLite ml = (MessageLite) pm.payload;
            int mlsize = ml.getSerializedSize();
            byte[] res = new byte[6 + mlsize];
            res[0] = (byte) ((pm.type >> 8) & 0xFF);
            res[1] = (byte) (pm.type & 0xFF);
            res[2] = (byte) ((mlsize >> 24) & 0xFF);
            res[3] = (byte) ((mlsize >> 16) & 0xFF);
            res[4] = (byte) ((mlsize >> 8) & 0xFF);
            res[5] = (byte) (mlsize & 0xFF);
            CodedOutputStream cos = CodedOutputStream.newInstance(res, 6, mlsize);
            try {
                ml.writeTo(cos);
            } catch (IOException ex) {
                MLogger.getlogger().log(LogConsts.LOGTYPE_NETERR, StackTraceUtil.getStackTrace(ex));
            }
            cos = null;
            return wrappedBuffer(res);
        } else if(pm.payload == null) {
            ChannelBuffer header = channel.getConfig().getBufferFactory().getBuffer(ByteOrder.BIG_ENDIAN, 6);
            header.writeShort((short)pm.type);
            header.writeInt(0);
            return header;
        }

        return pm;
    }
}
