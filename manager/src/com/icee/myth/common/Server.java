/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.common;

/**
 *	服务器接口
 * @author liuxianke
 */
public interface Server {
	
	/**
	 * 启动网络服务
	 */
    public void startServer();
    
    /**
     * 关闭网络服务
     */
    public void closeServer();
}
