package com.icee.myth.common.protobufMessage;

/**
 * Protobuf消息实体对象
 * @author chencheng
 */
public class ProtobufMessage {
    public int type;
    public Object payload;

    public ProtobufMessage(int type, Object payload) {
        this.type = type;
        this.payload = payload;
    }
}
