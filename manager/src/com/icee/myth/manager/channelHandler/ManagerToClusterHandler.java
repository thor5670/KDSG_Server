/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.manager.channelHandler;

import com.icee.myth.common.messageQueue.ServerMessageQueue;
import com.icee.myth.common.protobufMessage.ProtobufMessageType;
import com.icee.myth.manager.message.serverMessage.builder.ManagerMessageBuilder;
import com.icee.myth.protobuf.ExternalCommonProtocol.IntValuesProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.VariableValuesProto;
import com.icee.myth.utils.LogConsts;
import com.icee.myth.utils.MLogger;
import com.icee.myth.utils.StackTraceUtil;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBufferInputStream;
import org.jboss.netty.channel.*;

import java.io.IOException;

/**
 * Manager服务器连集群处理
 * @author liuxianke
 */
public class ManagerToClusterHandler extends SimpleChannelUpstreamHandler {

	private final int serverId;

	public ManagerToClusterHandler(int serverId) {
		this.serverId = serverId;
	}

	@Override
	public void channelConnected(ChannelHandlerContext ctx, ChannelStateEvent e) {
		// Add cluster channel connected message to message queue
		ServerMessageQueue.queue().offer(
				ManagerMessageBuilder.buildClusterConnectMessage(
						e.getChannel(), serverId));
	}

	@Override
	public void channelClosed(ChannelHandlerContext ctx, ChannelStateEvent e)
			throws Exception {
		// Add cluster channel close message to message queue
		ServerMessageQueue.queue().offer(
				ManagerMessageBuilder.buildClusterCloseMessage(serverId));
	}

	@Override
	public void messageReceived(ChannelHandlerContext ctx, MessageEvent e) {
		try {
			System.out.println("ManagerToClusterHandlerReceivemessage!");
			// add cluster channel received message to message queue
			ChannelBuffer cb = (ChannelBuffer) e.getMessage();
			short type = cb.readShort();
//			int length = cb.readInt();

			switch (type) {
			case ProtobufMessageType.MANAGER2CLUSTER_HEARTBEAT: {
				ServerMessageQueue.queue().offer(
						ManagerMessageBuilder
								.buildClusterHeartbeatMessage(serverId));
				break;
			}
			case ProtobufMessageType.SERVER2MANAGER_BATTLE_WIN_RATE_RESULT: {
				IntValuesProto intValues = IntValuesProto.getDefaultInstance()
						.newBuilderForType()
						.mergeFrom(new ChannelBufferInputStream(cb)).build();
				ServerMessageQueue.queue().offer(
						ManagerMessageBuilder
								.buildBattleWinRateResultMessage(intValues));
				break;
			}
			case ProtobufMessageType.SERVER2MANAGER_CARD_DRAW_RATE_RESULT: {
				VariableValuesProto variableValuesProto = VariableValuesProto
						.getDefaultInstance().newBuilderForType()
						.mergeFrom(new ChannelBufferInputStream(cb)).build();
				ServerMessageQueue
						.queue()
						.offer(ManagerMessageBuilder
								.buildCardDrawRateResultMessage(variableValuesProto));
				break;
			}
			case ProtobufMessageType.SERVER2MANAGER_UNINIT_OCCUPY_INFO: {
				IntValuesProto intValues = IntValuesProto.getDefaultInstance()
						.newBuilderForType()
						.mergeFrom(new ChannelBufferInputStream(cb)).build();
				ServerMessageQueue.queue().offer(
						ManagerMessageBuilder
								.buildUninitOccupyInfoMessage(intValues));
				break;
			}
			case ProtobufMessageType.SERVER2MANAGER_FIGHTING_OCCUPY_INFO: {
				IntValuesProto intValues = IntValuesProto.getDefaultInstance()
						.newBuilderForType()
						.mergeFrom(new ChannelBufferInputStream(cb)).build();
				ServerMessageQueue.queue().offer(
						ManagerMessageBuilder
								.buildFightingOccupyInfoMessage(intValues));
				break;
			}
			// TODO: other message
			}
		} catch (IOException ex) {
			MLogger.getlogger().log(LogConsts.LOGTYPE_NETERR,
					StackTraceUtil.getStackTrace(ex));
		}
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, ExceptionEvent e) {

		// MLogger.getlogger().log(LogConsts.LOGTYPE_NETERR,
		// "Server["+serverId+"] is not running.");
		// TODO: 是否需要关闭与Cluster的连接
		// e.getChannel().close();
	}
}
