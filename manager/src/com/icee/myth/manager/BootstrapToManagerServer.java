/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.manager;

import com.icee.myth.common.AbstractServer;
import org.jboss.netty.bootstrap.ServerBootstrap;
import org.jboss.netty.channel.Channel;

/**
 *
 * @author liuxianke
 */
public class BootstrapToManagerServer extends AbstractServer {

    private int port; // Server的端口地址

    private ServerBootstrap bootstrap;
    private Channel bindChannel = null;

    //Singleton
    private static BootstrapToManagerServer INSTANCE = new BootstrapToManagerServer();

    //getInstance操作的第一次调用在Main的start中，因此无需同步
    public static BootstrapToManagerServer getInstance() {
        return INSTANCE;
    }

    private BootstrapToManagerServer() {
    }
}
