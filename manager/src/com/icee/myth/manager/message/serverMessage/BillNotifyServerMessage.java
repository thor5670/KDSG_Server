/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.manager.message.serverMessage;

/**
 *
 * @author yangyi
 */
public class BillNotifyServerMessage extends ServerManagerMessage {
    public final int playerId;

    public BillNotifyServerMessage(int serverId, int playerId) {
        super(MessageType.MANAGER_BILL_NOTIFY, serverId);

        this.playerId = playerId;
    }
}
