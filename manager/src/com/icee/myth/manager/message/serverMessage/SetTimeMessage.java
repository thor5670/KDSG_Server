/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.manager.message.serverMessage;

import com.icee.myth.common.message.serverMessage.SimpleMessage;

/**
 *
 * @author liuxianke
 */
public class SetTimeMessage extends SimpleMessage {
    public final long time;

    public SetTimeMessage(long time) {
        super(MessageType.MANAGER_SET_TIME);

        this.time = time;
    }
}
