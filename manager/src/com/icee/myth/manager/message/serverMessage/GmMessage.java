/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.manager.message.serverMessage;

import com.icee.myth.protobuf.ConsoleToManagerProtocol.C2MGmProto;
import java.util.List;

/**
 *
 * @author yangyi
 */
public class GmMessage extends ServerManagerMessage {
    public int playerId;
    public String gmCmd;
    public List<String> gmArgs;

    public GmMessage(C2MGmProto gmProto) {
        super(MessageType.MANAGER_GM, gmProto.getServerId());
        playerId = gmProto.getPlayerId();
        gmCmd = gmProto.getGmCmd();
        gmArgs = gmProto.getArgsList();
    }

}
