


/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.manager;

import com.icee.myth.common.AbstractHeartbeatClient;
import com.icee.myth.common.message.serverMessage.Message;
import com.icee.myth.manager.message.serverMessage.ClusterConnectManagerMessage;
import com.icee.myth.manager.message.serverMessage.GmMessage;
import com.icee.myth.manager.message.serverMessage.BillNotifyServerMessage;
import com.icee.myth.manager.message.serverMessage.builder.ManagerMessageBuilder;
import com.icee.myth.manager.server.Server;
import com.icee.myth.manager.server.state.ClosingServerState;
import com.icee.myth.manager.server.state.RunningServerState;
import com.icee.myth.manager.server.state.ShutdownServerState;
import com.icee.myth.manager.server.state.UnknownServerState;
import com.icee.myth.protobuf.builder.ManagerToClusterBuilder;
import com.icee.myth.utils.LogConsts;
import com.icee.myth.utils.MLogger;

/**
 *
 * @author liuxianke
 */
public class ManagerToServerClient extends AbstractHeartbeatClient {

    public final Server server;
//    public boolean isShuttingdown = false;

    public ManagerToServerClient(Server server) {
        this.server = server;
    }

    @Override
    protected Object buildClientHeartBeat() {
        return ManagerToClusterBuilder.buildManagerHeartbeat();
    }

    @Override
    protected Message buildServerDownMessage() {
        return ManagerMessageBuilder.buildClusterDownMessage(server.getId());
    }

    @Override
    protected void serverDownWarning() {
        MLogger.getlogger().debuglog(LogConsts.LOGLEVEL_ERROR, "Server["+server.getId()+"]'s is down!");
    }

    @Override
    protected void serverCloseWarning() {
        MLogger.getlogger().debuglog(LogConsts.LOGLEVEL_ERROR, "Server["+server.getId()+"]'s is closed!");
    }

    public void shutdown() {
        if (server.state == RunningServerState.INSTANCE) {
            // 向Cluster发送shutdown命令
            channelContext.write(ManagerToClusterBuilder.buildShutdown());
        } else {
            MLogger.getlogger().debuglog(LogConsts.LOGLEVEL_DEBUG, "Can't shut down server["+server.getId()+"] when it is not in running state");
        }
    }

    public void handleMessage(Message message) {
        switch (message.getType()) {
            case ALL_HEARTBEAT: {
                heartbeat();
                break;
            }
            case MANAGER_CLUSTER_HEARTBEAT: {
                // 对方有心跳，重置心跳计数
                restoreHeartbeat();
                break;
            }
            case MANAGER_CLUSTER_CONNECT: {
                // 处理ClusterConnect消息
                serverConnect(((ClusterConnectManagerMessage)message).channel);

                MLogger.getlogger().debuglog(LogConsts.LOGLEVEL_INFO, "Server["+server.getId()+"]'s connected!");
                break;
            }
            case MANAGER_CLUSTER_CLOSE: {
                // 处理ClusterClose消息
                serverClose();

                if ((server.state != UnknownServerState.INSTANCE) &&
                    (server.state != ShutdownServerState.INSTANCE) &&
                    (server.state != ClosingServerState.INSTANCE)) {
                    // 重新连接cluster server，每次尝试隔1秒
                    reconnect();
                }// else {
//                    Manager.getInstance().removeManagerToClusterClient(serverId);
//                }

                break;
            }
            case MANAGER_BILL_NOTIFY: {
                BillNotifyServerMessage billNotifyServerMessage = (BillNotifyServerMessage)message;
                channelContext.write(ManagerToClusterBuilder.buildBillNotifyMessage(billNotifyServerMessage.playerId));
                MLogger.getlogger().debuglog(LogConsts.LOGLEVEL_INFO, "Server["+server.getId()+"]' handle bill notify, playerId " + billNotifyServerMessage.playerId);
                break;
            }
        }
    }

    public void gm(GmMessage gmMessage) {
        if (server.state == RunningServerState.INSTANCE) {
            channelContext.write(ManagerToClusterBuilder.buildGmMessage(gmMessage));
        } else {
            MLogger.getlogger().debuglog(LogConsts.LOGLEVEL_DEBUG, "Can't send gm message to server["+ server.getId() +"] when it is not in running state");
        }
    }
}
