/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.manager.server.state;

import com.icee.myth.common.message.serverMessage.Message;
import com.icee.myth.common.message.serverMessage.Message.MessageType;
import com.icee.myth.manager.message.serverMessage.RunServerManagerMessage;
import com.icee.myth.manager.server.Server;
import com.icee.myth.utils.LogConsts;
import com.icee.myth.utils.MLogger;

/**
 * 服务关闭状态
 * @author liuxianke
 */
public class ShutdownServerState implements ServerState {
    public final static ShutdownServerState INSTANCE = new ShutdownServerState();

    private ShutdownServerState() {
    }

    @Override
    public void handleMessage(Server server, Message message) {
        MessageType msgType = message.getType();
        switch (msgType) {
            case MANAGER_RUN_SERVER: {
                // 启动服务
                RunServerManagerMessage runServerManagerMessage = (RunServerManagerMessage)message;
                if (server.start(runServerManagerMessage.needSendConfig)) {
                    server.tryConnectServer();

                    // 进入启动中状态
                    server.state = BootingServerState.INSTANCE;
                }
                break;
            }
            default: {
                // 消息异常（不作处理）,记录日志
                MLogger.getlogger().debuglog(LogConsts.LOGLEVEL_ERROR, "ShutdownServerState handle error message type[" + msgType + "].");
                break;
            }
        }
    }
}
