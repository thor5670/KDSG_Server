/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.manager.server.state;

import com.icee.myth.common.message.serverMessage.Message;
import com.icee.myth.manager.server.Server;

/**
 *
 * @author liuxianke
 */
public class DummyServerState implements ServerState {
    public final static DummyServerState INSTANCE = new DummyServerState();

    private DummyServerState() {
    }

    @Override
    public void handleMessage(Server server, Message message) {
        // nothing to do
    }
}
