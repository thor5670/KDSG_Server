/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.manager.config;

/**
 *
 * @author liuxianke
 */
public class ManagerConfig {
    public String[] trustedBrowserHosts;    //信任的浏览器机器ip
    public String[] trustedConsoleHosts;    //信任的控制台机器ip
    public ServerConfig[] serverConfigs;    //服务器配置列表
}
