/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.manager.pipelineFactory;

import com.icee.myth.common.encoder.Type4BytesLengthFieldProtobufEncoder;
import com.icee.myth.manager.channelHandler.DeamonToManagerHandler;
import static org.jboss.netty.channel.Channels.*;
import org.jboss.netty.channel.ChannelPipeline;
import org.jboss.netty.channel.ChannelPipelineFactory;
import org.jboss.netty.handler.codec.frame.LengthFieldBasedFrameDecoder;

/**
 *
 * @author liuxianke
 */
public class DeamonToManagerPipelineFactory implements ChannelPipelineFactory {
    
    @Override
    public ChannelPipeline getPipeline() throws Exception {
        ChannelPipeline p = pipeline();
        p.addLast("frameDecoder", new LengthFieldBasedFrameDecoder(Short.MAX_VALUE, 2, 2, 0, 0));
        p.addLast("customEncoder", new Type4BytesLengthFieldProtobufEncoder());

        p.addLast("handler", new DeamonToManagerHandler());
        return p;
    }
}
