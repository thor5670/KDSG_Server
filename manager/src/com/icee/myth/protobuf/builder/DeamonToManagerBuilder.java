/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.protobuf.builder;

import com.google.protobuf.ByteString;
import com.icee.myth.common.protobufMessage.ProtobufMessage;
import com.icee.myth.common.protobufMessage.ProtobufMessageType;
import com.icee.myth.protobuf.DeamonToManagerProtocol.ServerConfigData;
import com.icee.myth.protobuf.DeamonToManagerProtocol.StartServer;

/**
 *
 * @author liuxianke
 */
public class DeamonToManagerBuilder {

    public static ProtobufMessage buildManagerHeartbeat() {
        return new ProtobufMessage(ProtobufMessageType.MANAGER2DEAMON_HEARTBEAT,null);
    }

    public static ProtobufMessage buildStartServer(String vmOptionStr, String paramStr) {
        StartServer.Builder builder = StartServer.newBuilder();
        if (vmOptionStr != null) {
            builder.setVmOptionStr(vmOptionStr);
        }
        
        builder.setParamStr(paramStr);

        return new ProtobufMessage(ProtobufMessageType.MANAGER2DEAMON_STARTSERVER, builder.build());
    }

    public static ProtobufMessage buildServerConfigData(int serverId, byte[] data) {
        ServerConfigData.Builder builder = ServerConfigData.newBuilder();
        builder.setServerId(serverId);
        builder.setConfigData(ByteString.copyFrom(data));

        return new ProtobufMessage(ProtobufMessageType.MANAGER2DEAMON_SERVERCONFIGDATA, builder.build());
    }

}
