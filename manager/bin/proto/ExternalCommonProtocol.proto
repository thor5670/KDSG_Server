package myth;

option java_package = "com.icee.myth.protobuf";

option optimize_for = SPEED;

message IntValueProto {
    required int32 value = 1;
}

message LongValueProto {
    required int64 value = 1;
}

message BoolValueProto {
    required bool value = 1;
}

message StringValueProto {
    required string value = 1;
}

message IntStringProto {
    required int32 intValue = 1;
    required string stringValue = 2;
}

message IntValuesProto {
    repeated int32 values = 1;
}

message VariableValueProto {
    required int32 id = 1;   // 变量id
    required int64 value = 2;   // 变量值
}

message VariableValuesProto {
    repeated VariableValueProto values = 1;
}

message LoginProto {
    required string loginsession = 1;
}

message CreateCharProto {
    optional string name = 1;
    optional int32 job = 2;
}

message EnterGameRetProto {
    required int32 result = 1;
    optional string errmsg = 2;

    optional EnterGameCharProto enterGameChar = 3;
}

message EnterGameCharProto {
    required int32 cid = 1;     // id
    required string name = 2;   // 名字
    required int32 gold1 = 3;   // 黄金（充值）
    required int32 gold2 = 4;   // 黄金（非充值）
    required int64 silver = 5;  // 白银
    required int32 energy = 6;  // 体力
    required int32 token = 7;   // 军令
    required int32 lv = 8;      // 等级
    required int32 exp = 9;     // 经验值
    required int32 rankLv = 10;      // 军衔等级
    required int32 rankExp = 11;    // 军衔经验（功勋）
    required int32 maxPower = 12;   // 历史最高战斗力

    optional CardsProto cards = 15;  // 卡片
    optional ItemsProto items = 16;  // 物品
    optional StagesProto stages = 17;    // 关卡信息
    optional StageStateProto stageState = 18;  // 关卡状态（当玩家在关卡中时才有值）
    optional SandboxProto sandbox = 19;  // 沙盘
    optional QuestsProto quests = 20;   // 任务
    optional ContSignProto contSign = 21; // 签到

    optional int32 guideStep = 23;  // 新手引导步骤

    optional VipProto vip = 25;          // vip
    optional int32 vipGift = 26;          // vip赠礼

    optional int32 buyEnergyCount = 30;     //购买体力次数
    optional int32 lastBuyEnergyDay = 31;   //最近一次购买剩余日期

    optional int64 currentTime = 35;    // 当前系统时间
    optional int64 endForbidTalkTime = 36;   // 禁言结束时间 <0 没有禁言 单位毫秒

    optional int32 refreshHegemonyCount = 40;     // 刷新争霸次数
    optional int32 lastRefreshHegemonyDay = 41;   // 最近一次购买剩余日期
    optional int32 lastHegemonyPayDay = 42;        // 最后一次领争霸军饷日期
    optional HegemonyTargetsProto hegemonyTargets = 43; // 争霸对手

    optional IntValuesProto training = 45;  // 校场
    optional IntValuesProto mine = 46;       // 银矿
    optional IntValuesProto barrack = 47;   // 兵营
    optional IntValuesProto ordnance = 48;  // 军械所
    optional IntValuesProto council = 49;   // 军机处
}

message EnterStageProto {
    required int32 stageId = 1;
    required bool isBigStage = 2;
    optional int32 helper = 3;
}

message FighterProto {
    required int32 id = 1;      // 位置id
    required int32 cardId = 2; // 卡片id
    optional int64 maxHp = 3;  // 最大血量
    optional int64 hp = 4;     // 当前血量
    optional bool isBoss = 5;   // 是否boss
}

message BattleMessageProto {
    required int32 type = 1;    // 消息类型
    optional bytes data = 2;    // 消息数据
}

message ChangeCardProto {
    required int32 slotId = 1;
    required int32 fromCardId = 2;
    required int32 toCardId = 3;
}

// 战斗动作
message BattleActionProto {
    repeated BattleMessageProto battleMessages = 1;  // 战斗过程消息
}

// 战斗回合
message BattleRoundProto {
    required int32 round = 1;       // 回合号
    repeated BattleActionProto battleActions = 2;    // 战斗动作
}

// 战斗过程信息
message BattleResultProto {
    repeated FighterProto fighters = 1;             // 战士
    repeated ChangeCardProto changeCards = 2;      // 乱入卡片
    repeated BattleRoundProto battleRounds = 3;    // 战斗回合
    required int32 winner = 4;  // 取胜方（阵营号）
}

message BaseResistanceBattleResultProto {
    required BattleResultProto battleResult = 1;    // 战斗过程
    required bool stimulated = 2;                      // 破釜沉舟
}

message HegemonyBattleResultProto {
    required BattleResultProto battleResult = 1;    // 战斗过程
    required bool stimulated = 2;                      // 破釜沉舟
    required int32 silver = 3;                          // 白银获得
    required int32 rankExp = 4;                         // 功勋获得
    optional HegemonyBattleBoxProto box = 5;         // 宝箱
}

message HegemonyBattleBoxProto {
    required int32 cardId = 1;                          // 卡片id
    required int32 cardLevel = 2;                       // 卡片等级
    required bool isBig = 3;                            // 是否大宝箱
}

message ItemProto{
    required int32 id = 1;
    optional int32 num = 2;
}

message ItemsProto{
    repeated ItemProto items = 1;
}

message RewardItemProto {
    required int32 type = 1;    // 类型（0：卡片 1：物品 2：黄金 3：白银）
    optional int32 id = 2;      // id
    optional int32 level = 3;   // 等级
    optional int32 num = 4;     // 数量
}

message RewardProto {
    repeated RewardItemProto items = 1;  // 奖品
    optional int32 experience = 2;  // 经验
    optional int32 silver = 3;      // 白银
    optional int32 gold = 4;        // 黄金
    optional int32 energy = 5;      // 体力
}

message CardProto {
    required int32 id = 1;  // 卡片id
    required int32 instId = 2;  // 卡片实例id
    required int32 level = 3;   // 卡片等级
    optional int32 experience = 4;  // 卡片经验
}

message CardsProto {
    repeated CardProto cards = 1;    // 拥有的卡片列表
    required int32 nextCardInstId = 2;  // 下一卡片实例id（注意：该id只在玩家内唯一，在玩家间不唯一）
}

message StagesProto{
    required int32 currentNormalStageId = 1;
    required int32 currentBigStageId = 2;
    optional int32 lastBigStageDay = 3;     // 最后一次通过精英关卡的日期
    repeated VariableValueProto lastBigStages = 4;       // 在lastBigStageDay所在的一天里通过的精英副本次数
}

// 关卡状态信息
message StageStateProto {
    required int32 stageId = 1;     // 关卡号
    required int32 stageType = 2;        // 关卡类型（0普通 1精英 2活动）
    optional int32 currentBattleId = 3;    // 当前战斗场号
    repeated StageBattleEnemyProto currentBattleEnemies = 4;    // 当前战斗敌人
    optional bool needRevive = 5;   // 是否需要复活
    optional RewardProto box = 6;       // 宝箱（关卡所得）
    optional BriefPlayerProto helper = 7;   // 助战者
}

message StageBattleEnemyProto {
    required int32 slotId = 1;  // 阵型位置号
    required int32 cardId = 2;  // 卡片号
    required int32 level = 3;   // 等级
    optional bool isBoss = 4;   // 是否Boss
}

message StageLeaveProto {
    required int32 stageId = 1; // 关卡号
    required int32 stageType = 2;   // 关卡类型（0普通 1精英 2活动）
}

message C2STalkProto{
    required int32 channelType = 1;
    required string msg = 2;
    optional int32 playerId = 3; //私聊时代表好友
}

message S2CTalkProto{
    required int32 channelType = 1;
    required string msg = 2;
    optional int32 sendPlayerId = 3;
    optional string sendPlayerName = 4;
    optional int32 recvPlayerId = 5;
    optional string recvPlayerName = 6;
}

//message AddConcernProto {
//    optional int32 id = 2;    // 朋友Id
//    optional string name = 3; // 朋友名称
//}

message BriefPlayersProto{
    repeated BriefPlayerProto concerns = 1;
}

message BriefPlayerProto{
    required int32 id = 1;
    required string name = 2;
    required int32 level = 3;
    required int32 leaderCardId = 4;
    required int32 leaderCardLevel = 5;
    optional bool isFriend = 6;
    optional int64 cooldown = 7;    // 朋友冷却（只有当isFriend为true时才有效）
    optional bool inGame = 8;   // 朋友在线（只有当isFriend为true时才有效）
}

message SandboxProto{
    required VariableValuesProto slots = 1;
    required int32 leader = 2;
    required int32 helper = 3;
}

message BuyOracleCountChangeProto{
    required int32 buyOracleCount = 1;
    required int32 lastBuyOracleDay = 2;
}

message BigStageStatusChangeProto{
    required int32 stageId = 1;
    required int32 enterCount = 2;
    required int32 lastDate = 3;
}

message VipProto{
    optional int32 exp = 1;
    optional int32 level = 2;
    optional int32 lastDayExp = 3;
    optional int32 lastExpDay = 4;
}

message CountChangeProto{
    required int32 count = 1;
    required int32 lastday = 2;
}

message MailListProto{
    repeated MailBriefProto mailBriefs = 1;       // 玩家的邮件
}

message MailBriefProto{
    required int64 mailId = 1;      // 邮件号
    required string title = 2;      // 邮件标题
    required int32 status = 3;      // 邮件状态（0.未读 1.已读 2.已领取）
    required bool hasReward = 4;    // 是否有奖励
}

message MailNumProto{
    required int32 totalNum = 1;    // 邮件总数
    required int32 unreadNum = 2;   // 未读邮件数
}

message MailProto {
    required int64 mailId = 1;  // 邮件id
    required MailContentProto content = 2;  // 邮件内容
    required int32 status = 3;  // 邮件状态
}

message MailContentProto {
    required string title = 1;
    required string description = 2;
    optional RewardProto reward = 3;
}

message EnterActivityStageProto {
    required int32 activityId = 1;
    required int32 itemId = 2;
    optional int32 helperId = 3;
}

message NormalActivityItemProto {
    required int32 id = 1;
    required int32 type = 2;    // 0关卡 1充值 2消费 3兑换
    optional string title = 3;
    optional string icon = 4;
    optional RewardProto reward = 5;    // 奖励（过关活动没有活动奖励，在活动关卡中产生奖励）
    optional int32 value1 = 6;       // 值1（过关活动为剩余次数，充值活动为需要金额，兑换活动无意义）
    optional int32 value2 = 7;       // 值2（过关活动为关卡号，充值活动为现有金额，兑换活动无意义）
    repeated VariableValueProto exchangeItems = 8;  // 兑换物品表（兑换活动有效，过关和充值活动无效）
    required bool gotten = 9;
}

message NormalActivityItemsProto {
    required int32 id = 1;  // 活动号
    repeated NormalActivityItemProto items = 2; // 活动项
}

message NormalActivityProto {
    required int32 id = 1;
    optional int32 recommend = 2;
    optional string title = 3;
    optional string icon = 4;
    optional int64 startTime = 5;
    optional int64 endTime = 6;
}

message NormalActivitiesProto {
    repeated NormalActivityProto activities = 1;
}

message CardFoodsProto {
    required int32 cardInstId = 1;
    repeated int32 foodCardInstIds = 2;
}

message CardLevelExperienceProto {
    required int32 cardInstId = 1;
    required int32 level = 2;
    required int32 experience = 3;
}

message FriendLeaderCardChangeProto {
    required int32 friendId = 1;
    required int32 leaderCardId = 2;
    required int32 leaderCardLevel = 3;
}

message QuestProto {
    required int32 questId = 1;
    optional bool finished = 2;
}

message QuestsProto {
    repeated QuestProto quests = 1;
}

message ContSignProto{
    required int32 cumulativeNum = 1;   // 累计登陆天数
    required int32 cumulativeRewardNum = 2;   // 可领取的奖励天数
    required int32 consecutiveNum = 3;  // 连续登陆天数
    required int32 consecutiveReceiveFlag = 4;  // 连续签到领取标签
    required int32 liveness = 5; //活跃度
    required int32 livenessReceiveFlag = 6; // 活跃奖励领取标签
    required LivenessTargetProto livenessTarget = 7;    // 活跃度目标
}

message LivenessTargetProto{
    required int32 sign = 1; //签到
    // TODO: 活跃度项目
    required int32 normalStage = 2;  //普通副本
    required int32 heroStage = 3;    //英雄副本
}

message LivenessChangeProto{
    required int32 liveness = 1;
    required int32 type = 2;
    required int32 value = 3;
}

message CardDrawedProto{
    repeated VariableValueProto cards = 1;
}

message CardDrawActivitiesProto{
    repeated CardDrawActivityProto cardDrawActivities = 1;
}

message CardDrawActivityProto{
    required int32 cardDrawId = 1;  // 卡包号
    required int32 priority = 2;    // 优先级
    optional CardDrawDiscountProto discountInfo = 3;    // 打折活动信息
    optional CardDrawAddCardProto addCardInfo = 4;      // 卡片活动信息
}

message CardDrawDiscountProto{
    required int32 discountId = 1;  // 打折号
    required int64 startTime = 2;   // 起始时间
    required int64 endTime = 3;     // 结束时间
}

message CardDrawAddCardProto{
    repeated CardDrawAddCardItemProto cardItems = 1;    // 卡片信息
    required int64 startTime = 2;   // 起始时间
    required int64 endTime = 3;     // 结束时间
}

message CardDrawAddCardItemProto{
    required int32 cardId = 1;  // 卡片号
    required int32 type = 2;    // 添加类型（用于客户端显示“概率翻倍”或“乱入卡片”）
}

message StageActivitiesProto{
    repeated StageActivityProto stageActivities = 1;
    optional StageCardDropMultipleProto cardDropMultipleInfo = 2;    // 全局掉卡倍率信息
    optional StageHalfEnergyProto halfEnergyInfo = 3;                  // 全局体力减半信息
}

message StageActivityProto{
    required int32 stageId = 1; // 关卡id
    optional StageCardDropMultipleProto cardDropMultipleInfo = 2;    // 掉卡倍率信息
    optional StageHalfEnergyProto halfEnergyInfo = 3;   // 体力减半信息
}

message StageCardDropMultipleProto{
    required int32 multiple = 1;     // 倍数
    required int64 startTime = 2;   // 起始时间
    required int64 endTime = 3;     // 结束时间
}

message StageHalfEnergyProto{
    required int64 startTime = 1;   // 起始时间
    required int64 endTime = 2;     // 结束时间
}

message TargetSandboxProto{
    repeated TargetSandboxSlotProto slots = 1;
    required int32 leader = 2;
}

message TargetSandboxSlotProto{
    required int32 slotId = 1;  // 槽位
    required int32 cardId = 2;  // 卡片id
    required int32 cardlevel = 3;   // 卡片等级
}

message HegemonyTargetProto{
    required string name = 1;   // 目标玩家姓名
    required int32 id = 2;      // 目标玩家id（为0时为机器人，否则为玩家id）
    required int32 level = 3;   // 目标玩家等级
    required int32 rank = 4;    // 目标玩家军衔
    required int32 status = 5;  // 状态（0.未战 1.胜利 2.失败）
    required TargetSandboxProto sandbox = 6;    // 目标玩家阵型
    optional int32 power = 7;   // 战斗力
}

message HegemonyTargetsProto{
    repeated HegemonyTargetProto targets = 1;   // 争霸目标列表
}

message HegemonyGetPayProto{
    required int32 lastHegemonyPayDay = 1;  // 最近争霸领军饷日期
    optional int32 silver = 2;  // 白银
    optional int32 gold = 3;    // 黄金
}

message BaseTargetProto{
    required string name = 1;   // 目标玩家姓名
    required int32 id = 2;      // 目标玩家id（为0时为机器人，否则为玩家id）
    required int32 level = 3;   // 目标玩家等级
    required int32 rank = 4;    // 目标玩家军衔
    required bool hasKing = 5;  // 是否有君主
    required int32 leaderCardId = 6;    // 目标玩家队长卡片号
    required int32 leaderCardLevel = 7; // 目标玩家队长卡片等级
}

message BaseTargetsProto{
    repeated BaseTargetProto targets = 1;   // 据点目标列表
    required int64 targetsCreateTime = 2;   // 目标产生时间
}

message OccupyItemProto{
    required int32 id = 1;
    required string name = 2;
    required int32 level = 3;
    required int32 rank = 4;
    required int32 leaderCardId = 5;
    required int32 leaderCardLevel = 6;
    required int32 production = 7;
    required int64 startTime = 8;
    optional int64 freeTime = 9;
}

message OccupyProto{
    optional OccupyItemProto king = 1;      // 君主
    repeated OccupyItemProto capes = 2;     // 臣属
}

message MineIncomeProto{
    required int32 silver = 1;
    required int64 startTime = 2;
}

// ************ 开始通知消息定义 ************ //
message NoteProto{
    required int32 type = 1;    // 通知消息类型
    optional bytes data = 2;    // 消息数据
}

message BeAddFriendNoteProto {
    required int32 id = 1;
    required string name = 2;
}
// ************ 结束通知消息定义 ************ //
