CREATE DATABASE IF NOT EXISTS `sanguo_manager1` DEFAULT CHARACTER SET utf8  COLLATE utf8_general_ci;

USE `sanguo_manager1`;

/*Table structure for table `firstlogin` */
CREATE TABLE `firstlogin` (
  `thirdPartyUserId` varchar(32) DEFAULT NULL,
  `serverId` int(11) DEFAULT NULL,
  `createtime` datetime DEFAULT NULL,
  `ip` int(11) DEFAULT NULL,
  KEY `createtimeIdx` (`createtime`),
  KEY `thirdPartyUserIdIdx` (`thirdPartyUserId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Table structure for table `thirdpartyuser` */
CREATE TABLE `thirdpartyuser` (
  `thirdPartyUserId` varchar(32) NOT NULL,
  `cid` int(11) DEFAULT NULL,
  `forbiddenTalkEndTime` datetime DEFAULT NULL,
  `forbiddenLoginEndTime` datetime DEFAULT NULL,
  `createtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`thirdPartyUserId`),
  KEY `createtimeIdx` (`createtime`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Table structure for table `serverconfig` */
CREATE TABLE `serverconfig` (
  `id` int(11) NOT NULL,
  `regionid` varchar(20) NOT NULL,
  `name` varchar(50) NOT NULL,
  `dbhost` varchar(20) NOT NULL,
  `dbname` varchar(20) NOT NULL,
  `logdbhost` varchar(20) NOT NULL,
  `logdbnameprefix` varchar(20) NOT NULL,
  `host` varchar(20) NOT NULL,
  `externalhost` varchar(50) NOT NULL,
  `externalport` int(11) NOT NULL,
  `managerport` int(11) NOT NULL,
  `rpcport` int(11) NOT NULL,
  `billServerGetAssetAddress` varchar(255) NOT NULL,
  `billServerTransactionAddress` varchar(255) NOT NULL,
  `couponServerApplyAddress` varchar(255) NOT NULL,
  `billApiKey` varchar(100) NOT NULL,
  `billApiSecret` varchar(100) NOT NULL,
  `opentime` datetime NOT NULL,
  `jvmoption` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Table structure for table `trustedconsoleips` */
CREATE TABLE `trustedconsoleips` (
  `ip` varchar(20) NOT NULL,
  PRIMARY KEY (`ip`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DELIMITER $$
CREATE PROCEDURE `getserviceshaslogin`(
	pthirdpartyuserid VARCHAR(32)
    )
BEGIN
	SELECT serverId
	FROM `firstlogin` WHERE  `thirdpartyuserid`=`pthirdpartyuserid`;
    END $$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `get_firstlogin`(
	pthirdpartyuserid VARCHAR(32),
	pserverid INT(11)
    )
BEGIN
	SELECT serverId
	FROM `firstlogin` WHERE `thirdpartyuserid`=`pthirdpartyuserid` AND `serverid` = `pserverid`;
    END $$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `get_maxcid`(
    )
BEGIN
	SELECT MAX(cid) AS maxcid
	FROM `thirdpartyuser`;
    END $$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `get_playerinfo`(
	pthirdpartyuserid VARCHAR(32)
    )
BEGIN
	SELECT cid,UNIX_TIMESTAMP(forbiddenTalkEndTime) AS forbiddenTalkEndTime,UNIX_TIMESTAMP(forbiddenLoginEndTime) AS forbiddenLoginEndTime
	FROM `thirdpartyuser`
	WHERE  `thirdpartyuserid` = `pthirdpartyuserid`;
    END $$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `get_playerinfobycid`(
	pcid	INT(11)
    )
BEGIN
	SELECT thirdPartyUserId,UNIX_TIMESTAMP(forbiddenTalkEndTime) AS forbiddenTalkEndTime,UNIX_TIMESTAMP(forbiddenLoginEndTime) AS forbiddenLoginEndTime
	FROM `thirdpartyuser`
	WHERE `cid`=`pcid`;
    END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `insert_firstlogin`(
	pthirdpartyuserid  VARCHAR(32),
	pserverid    INT(11),
	pip 	INT(11)
    )
BEGIN
	INSERT INTO `firstlogin` VALUES(`pthirdpartyuserid`,`pserverid`,NOW(),`pip`);
    END $$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `insert_playerinfo`(
	pthirdpartyuserid VARCHAR(32),
	pcid       INT(11)
    )
BEGIN
	INSERT INTO thirdpartyuser(`thirdpartyuserid`,`cid`) VALUES(`pthirdpartyuserid`,pcid);
    END $$
DELIMITER ;
