package com.icee.myth.deamon.messageQueue;

import com.icee.myth.deamon.message.serverMessage.Message;
import com.icee.myth.deamon.utils.LinkedTransferQueue;

/**
 * 服务器消息队列
 * @author liuxianke
 */
public class ServerMessageQueue {

    // The messageQueue queue is a resource shared by channel handler thread and the game thread.
    // Channel handler thread put received messages in the queue and the game thread get
    // messages out of the queue.
    private static final LinkedTransferQueue<Message> messageQueue = new LinkedTransferQueue<Message>();

    /**
     * messageQueue队列是channel handler(IO线程)和游戏逻辑处理线程的一个共享资源。
     * 通道处理程序线程把接收到的消息放在队列中,游戏线程拿出队列中的消息进行游戏逻辑处理。
     * */
    public static LinkedTransferQueue<Message> queue() {
        return messageQueue;
    }
}
