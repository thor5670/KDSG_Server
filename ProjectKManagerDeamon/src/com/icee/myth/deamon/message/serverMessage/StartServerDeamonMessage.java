/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.deamon.message.serverMessage;

/**
 *
 * @author liuxianke
 */
public class StartServerDeamonMessage extends SimpleMessage {
    public final String vmOptionStr;
    public final String paramStr;

    public StartServerDeamonMessage(String vmOptionStr, String paramStr) {
        super(MessageType.DEAMON_STARTSERVER);

        this.vmOptionStr = vmOptionStr;
        this.paramStr = paramStr;
    }

}
