CREATE DATABASE IF NOT EXISTS `koudaisanguo`  default charset utf8 collate utf8_general_ci;
USE `koudaisanguo`;
/*
 * table character
 *
 */
CREATE TABLE `consumerlog` (
  `cid` INT(11) NOT NULL,
  `time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `consumertype` INT(11) NOT NULL,
  `goldnum` INT(11) DEFAULT NULL,
  `goldtype` INT(11) DEFAULT NULL,
  `productid` varchar(256) DEFAULT NULL,
  `productnum` INT(11) DEFAULT NULL
) ENGINE=MYISAM DEFAULT CHARSET=utf8;

CREATE TABLE `charactor` (
    `mid` int(11) NOT NULL,
    `name` varchar(36) NOT NULL,
    `gold1` int(11) NOT NULL DEFAULT '0',
    `gold2` int(11) NOT NULL DEFAULT '0',
    `silver` bigint(20) NOT NULL DEFAULT '0',
    `energy` int(11) NOT NULL DEFAULT '0',
    `token` int(11) NOT NULL DEFAULT '3',
    `level` int(11) NOT NULL DEFAULT '1',
    `experience` int(11) NOT NULL DEFAULT '0',
    `rankLevel` int(11) NOT NULL DEFAULT '1',
    `rankExperience` int(11) NOT NULL DEFAULT '0',
    `leaderCardId` int(11) NOT NULL,
    `leaderCardLevel` int(11) NOT NULL DEFAULT '1',
    `vipLevel` int(11) NOT NULL DEFAULT '0',
    `vipExperience` int(11) NOT NULL DEFAULT '0',
    `maxPower` int(11) NOT NULL DEFAULT '0',
    `detail` mediumblob,
    `relation` mediumblob,
    `mail` mediumblob,
    `occupyInfo` tinyblob,
    `totalOnlineTime` bigint(20) NOT NULL DEFAULT '0',
    `createtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `leavetime` timestamp NULL DEFAULT NULL,
    PRIMARY KEY (`mid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `battle` (
    `bid` bigint(20) NOT NULL,
    `detail` mediumblob,
    `createtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`bid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `info` (
    `id` INT(11) NOT NULL,
    `detail` MEDIUMBLOB,
    PRIMARY KEY(`id`)
) ENGINE = INNODB DEFAULT CHARSET = utf8;

CREATE TABLE `bill` (
  `cid` int(11) DEFAULT NULL,
  `thirdpartyUserId` varchar(32) NOT NULL,
  `orderno` varchar(64) DEFAULT NULL,
  `currencyid` int(11) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `memo` varchar(128) DEFAULT NULL,
  `status` int(11) DEFAULT NULL COMMENT 'transaction处理结果。transaction前没有意思，填0',
  `step` tinyint(4) DEFAULT NULL COMMENT '0生成订单1处理订单完成',
  `time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  KEY `ordernoIdx` (`orderno`)
) ENGINE = INNODB DEFAULT CHARSET = utf8;

CREATE TABLE `coupon` (
  `cid` int(11) DEFAULT NULL,
  `thirdpartyUserId` varchar(64) DEFAULT NULL,
  `coupon` varchar(64) DEFAULT NULL,
  `time` datetime DEFAULT NULL
) ENGINE=INNODB DEFAULT CHARSET=utf8;

CREATE TABLE `mail` (
  `id` bigint(20) NOT NULL,
  `cid` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` mediumtext NOT NULL,
  `reward` tinyblob,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `activity` (
  `id` int(11) NOT NULL,
  `detail` mediumtext,
  `createtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `enable` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP PROCEDURE IF EXISTS `get_char_num`;
DELIMITER $$
    CREATE PROCEDURE `get_char_num`(
        pmid	INT(11)
    )
    BEGIN
        SELECT COUNT(*) AS numberOfChar
        FROM charactor
        WHERE `mid`=pmid;
    END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS `create_char`;
DELIMITER $$
    CREATE PROCEDURE `create_char`(
        pmid	 INT(11),
        pname    VARCHAR(36),
        pleaderCardId     INT(11)
    )
    BEGIN
        INSERT INTO charactor(`mid`,`name`,`leaderCardId`) VALUES(pmid,pname,pleaderCardId);
    END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS `get_char_detail`;
DELIMITER $$
    CREATE PROCEDURE `get_char_detail`(
        pmid	INT(11)
    )
    BEGIN
        SELECT `mid`,`name`,`gold1`,`gold2`,`silver`,`energy`,`token`,`level`,`experience`,`rankLevel`,`rankExperience`,`leaderCardId`,`leaderCardLevel`,`vipLevel`,`vipExperience`,`maxPower`,`detail`,`mail`,`totalOnlineTime`,`leavetime`
        FROM charactor
        WHERE `mid`=pmid;
    END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS `update_char_detail_info`;
DELIMITER $$
    CREATE PROCEDURE `update_char_detail_info`(
        pmid                INT(11),
        pgold1              INT(11),
        pgold2              INT(11),
        psilver             BIGINT(20),
        penergy             INT(11),
        ptoken              INT(11),
        plevel              INT(11),
        pexperience         INT(11),
        prankLevel          INT(11),
        prankExperience     INT(11),
        pleaderCardId       INT(11),
        pleaderCardLevel    INT(11),
        pvipLevel           INT(11),
        pvipExperience      INT(11),
        pmaxPower           INT(11),
        pdetail             MEDIUMBLOB,
        pmail               MEDIUMBLOB,
        ptotalOnlineTime    BIGINT(20),
        pleavetime          TIMESTAMP
    )
    BEGIN
        UPDATE charactor
        SET gold1=pgold1, gold2=pgold2, silver=psilver, energy=penergy, token=ptoken, level=plevel, experience=pexperience, rankLevel=prankLevel, rankExperience=prankExperience, leaderCardId=pleaderCardId, leaderCardLevel=pleaderCardLevel, vipLevel=pvipLevel, vipExperience=pvipExperience, maxPower=pmaxPower, detail=pdetail, mail=pmail, totalOnlineTime=ptotalOnlineTime, leavetime=pleavetime
        WHERE `mid`=pmid;
    END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS `get_relation`;
DELIMITER $$
    CREATE PROCEDURE `get_relation`(
        pmid             int(11)
    )
    BEGIN
        SELECT relation
        FROM charactor
        WHERE `mid`=pmid;
    END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS `update_relation`;
DELIMITER $$
    CREATE PROCEDURE `update_relation`(
        pmid		INT(11),
        prelation       MEDIUMBLOB
    )
    BEGIN
        UPDATE charactor
        SET relation=prelation
        WHERE `mid`=pmid;
    END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS `get_occupy_info`;
DELIMITER $$
    CREATE PROCEDURE `get_occupy_info`(
        pmid             int(11)
    )
    BEGIN
        SELECT `mid`,`name`,`level`,`rankLevel`,`leaderCardId`,`leaderCardLevel`,`occupyInfo`
        FROM charactor
        WHERE `mid`=pmid;
    END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS `update_occupy_info`;
DELIMITER $$
    CREATE PROCEDURE `update_occupy_info`(
        pmid		INT(11),
        poccupyInfo     TINYBLOB
    )
    BEGIN
        UPDATE charactor
        SET occupyInfo=poccupyInfo
        WHERE `mid`=pmid;
    END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS `get_max_human_level`;
DELIMITER $$
    CREATE PROCEDURE `get_max_human_level`()
    BEGIN
        Select max(level)
        From charactor;
    END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS `get_unfinishbill`;
DELIMITER $$
    CREATE PROCEDURE `get_unfinishbill`()
    BEGIN
        SELECT * FROM bill WHERE orderno NOT IN(SELECT orderno FROM bill WHERE step =1) ;
    END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS `save_bill`;
DELIMITER $$
    CREATE PROCEDURE `save_bill`(
        pcid int(11),
        pthirdpartyuserid  varchar(32),
        porderno    varchar(64),
        pcurrencyid 	int(11),
        pamount int,
        pmemo   varchar(128),
        pstatus int,
        pstep int
    )
    BEGIN
        insert into `bill` values(`pcid`,`pthirdpartyuserid`,`porderno`,`pcurrencyid`,`pamount`,`pmemo`,`pstatus`,`pstep`,now());
    END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS `save_coupon` ;
DELIMITER $$
    CREATE PROCEDURE `save_coupon`(
        pcid INT(11),
        pthirdpartyuserid  VARCHAR(32),
        pcode  VARCHAR(64),
        ptime DATETIME
    )
    BEGIN
        INSERT INTO `coupon` VALUES(`pcid`,`pthirdpartyuserid`,`pcode`,`ptime`);
    END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS `save_mail` ;
DELIMITER $$
    CREATE PROCEDURE `save_mail`(
        pid bigint(20),
        pcid    INT(11),
        ptitle  VARCHAR(50),
        pdescription  MEDIUMTEXT,
        preward tinyblob,
        ptime   timestamp
    )
    BEGIN
        INSERT INTO `mail`(`id`,`cid`,`title`,`description`,`reward`,`time`) VALUES(`pid`,`pcid`,`ptitle`,`pdescription`,`preward`,`ptime`);
    END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS `get_mails`;
DELIMITER $$
    CREATE PROCEDURE `get_mails`(
        pid     bigint(11),
        pcid    INT(11)
    )
    BEGIN
        SELECT `id`,`title`,`description`,`reward`
        FROM `mail`
        WHERE (`id`>pid) AND ((`cid`=pcid) OR (`cid`=-1))
        ORDER BY `id` DESC
        LIMIT 32;
    END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS `get_max_mail_id`;
DELIMITER $$
    CREATE PROCEDURE `get_max_mail_id`()
    BEGIN
        Select max(`id`)
        From `mail`;
    END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS `save_consumer_log`;
DELIMITER $$
    CREATE PROCEDURE `save_consumer_log`(
        cid INT(11),
        consumertype INT(11),
        goldnum  INT(11),
        goldtype INT(11),
        productid VARCHAR(256),
        productnum INT(11)
    )
    BEGIN
        INSERT INTO `consumerlog`(`cid`,`consumertype`,`goldnum`,`goldtype`,`productid`,`productnum`,`time`)
        VALUES(`cid`,`consumertype`,`goldnum`,`goldtype`,`productid`,`productnum`,NOW());
    END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS `get_consumer_log`;
DELIMITER $$
CREATE PROCEDURE `get_consumer_log`(
        cid INT(11),
        consumertypeval INT(11),
        begintime  TIMESTAMP,
        endtime TIMESTAMP
    )
    BEGIN
	IF (consumertypeval != 5 AND consumertypeval != 6) THEN
            SELECT `cid`,`consumertype`,`goldnum`,`goldtype`,`productid`,`productnum`,`time` FROM consumerlog
            WHERE  `time` >=  begintime AND `time` < endtime AND `consumertype` = consumertypeval;
        ELSEIF (consumertypeval = 5) THEN
	    SELECT `cid`,`consumertype`,`goldnum`,`goldtype`,`productid`,`productnum`,`time` FROM consumerlog
            WHERE  `consumertype` >= 0 AND `consumertype` < 3 AND `time` >=  begintime AND `time` < endtime;
        ELSE
            SELECT `cid`,`consumertype`,`goldnum`,`goldtype`,`productid`,`productnum`,`time` FROM consumerlog
            WHERE  `consumertype` >= 3 AND `consumertype` < 5 AND `time` >=  begintime AND `time` < endtime;
        END IF;
    END$$

DELIMITER ;