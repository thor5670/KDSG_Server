/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icee.myth.log;

import com.icee.myth.config.MapConfig;
import com.icee.myth.log.message.GameLogMessage;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 *
 * @author chencheng
 */
public class GameLogger {

    private static GameLogger INSTANCE;

    private FileLogThreadHandle fileLogThreadHandle;
    private DBLogThreadHandle dbLogThreadHandle;
    private FileLogCYThreadHandle fileLogCYThreadHandle;

    private Thread fileLogThread;
    private Thread dbLogThread;
    private Thread fileLogCYThread;

    public static void init(String rootPath, String pathName, int flush_interval, int bufferSize, boolean needConsolePrint, String dbHost, String dbNamePrefix) {
        if (INSTANCE == null) {
            INSTANCE = new GameLogger(rootPath, pathName, flush_interval, bufferSize, needConsolePrint, dbHost, dbNamePrefix);
            INSTANCE.start();
        }
    }

    public static GameLogger getlogger() {
        return INSTANCE;
    }

    private GameLogger(String rootPath, String pathName, int flush_interval, int bufferSize, boolean needConsolePrint, String dbHost, String dbNamePrefix) {
        fileLogThreadHandle = new FileLogThreadHandle(rootPath, pathName, flush_interval, bufferSize, needConsolePrint);
        dbLogThreadHandle = new DBLogThreadHandle(dbHost, dbNamePrefix);
        fileLogCYThreadHandle = new FileLogCYThreadHandle(rootPath, pathName, flush_interval, bufferSize);

        fileLogThread = new Thread(fileLogThreadHandle, "FileLogThread");
        dbLogThread = new Thread(dbLogThreadHandle, "DBLogThread");
        fileLogCYThread = new Thread(fileLogCYThreadHandle, "FileLogCYThread");
    }

    private void start() {
        fileLogThread.start();
        dbLogThread.start();
        if (MapConfig.INSTANCE.useCYLog == true){
            fileLogCYThread.start();
        }
    }

    public void log(GameLogMessage message) {
        if (message != null) {
            switch (message.type) {
                case GAMELOGTYPE_F_DEBUG:
                case GAMELOGTYPE_F_DBERR:
                case GAMELOGTYPE_F_NETERR:
                case GAMELOGTYPE_F_DBDOWN_CACHE: {
                    fileLogThreadHandle.log(message);
                    break;
                }
                case GAMELOGTYPE_DB_BEHAVIOR:
                case GAMELOGTYPE_DB_CREATECHAR:
                case GAMELOGTYPE_DB_LOGINLOGOUT:
                case GAMELOGTYPE_DB_ONLINENUM:
                case GAMELOGTYPE_DB_CASH:
                case GAMELOGTYPE_DB_GOLD:
                case GAMELOGTYPE_DB_SILVER: {
                    dbLogThreadHandle.log(message);
                    break;
                }
                case GAMELOGTYPE_CY_LOGIN:
                case GAMELOGTYPE_CY_REGISTER:
                case GAMELOGTYPE_CY_BEHAVIOR:
                case GAMELOGTYPE_CY_RECHARGE:
                case GAMELOGTYPE_CY_ROLE:
                case GAMELOGTYPE_CY_ONLINE:
                case GAMELOGTYPE_CY_ONLINE_TIME:
                case GAMELOGTYPE_CY_USER:
                case GAMELOGTYPE_CY_COMMODITY_BUY:
                case GAMELOGTYPE_CY_COMMODITY_COST:
                case GAMELOGTYPE_CY_DIAMOND:
                case GAMELOGTYPE_CY_TASK:
                case GAMELOGTYPE_CY_DOWNLOAD:
                case GAMELOGTYPE_CY_CARD:
                case GAMELOGTYPE_CY_INSTANCE:
                case GAMELOGTYPE_CY_EVOLUTION:
                case GAMELOGTYPE_CY_ACTIVATE:
                {
                    fileLogCYThreadHandle.log(message);
                    break;
                }
               
            }
        }
    }

    public void shutdown() {
        dbLogThreadHandle.shutdown = true;
        
        try {
            dbLogThread.join();
        } catch (InterruptedException ex) {
            Logger.getLogger(GameLogger.class.getName()).log(Level.SEVERE, null, ex);
        }

        fileLogThreadHandle.shutdown = true;
        fileLogCYThreadHandle.shutdown = true;
        
        try {
            fileLogThread.join();
        } catch (InterruptedException ex) {
            Logger.getLogger(GameLogger.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
