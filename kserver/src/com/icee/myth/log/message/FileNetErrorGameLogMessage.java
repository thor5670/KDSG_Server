/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.log.message;

/**
 *
 * @author liuxianke
 */
public class FileNetErrorGameLogMessage extends GameLogMessage {
    public String msg;

    public FileNetErrorGameLogMessage(String msg) {
        super(GameLogType.GAMELOGTYPE_F_NETERR);
        this.msg = msg;
    }
}
