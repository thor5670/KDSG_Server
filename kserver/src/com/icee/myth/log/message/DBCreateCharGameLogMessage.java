/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.log.message;

/**
 * 
 * @author liuxianke
 */
public class DBCreateCharGameLogMessage extends GameLogMessage {
    public final int playerId;
    public final int leaderCardId;
    public final long time;
    
    public DBCreateCharGameLogMessage(int playerId, int leaderCardId, long time) {
        super(GameLogType.GAMELOGTYPE_DB_CREATECHAR);
        this.playerId = playerId;
        this.leaderCardId = leaderCardId;
        this.time = time;
    }
}
