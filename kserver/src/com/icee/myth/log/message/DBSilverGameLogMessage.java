/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.log.message;

/**
 *
 * @author liuxianke
 */
public class DBSilverGameLogMessage extends GameLogMessage {
    public final int playerId;
    public final int mainType;
    public final int subtype;
    public final int change;
    public final long remain;
    public final long time;

    public DBSilverGameLogMessage(int playerId, int mainType, int subType, int change, long remain, long time) {
        super(GameLogType.GAMELOGTYPE_DB_SILVER);
        this.playerId = playerId;
        this.mainType = mainType;
        this.subtype = subType;
        this.change = change;
        this.remain = remain;
        this.time = time;
    }
}
