/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.log.message;

import java.util.List;

/**
 *
 * @author lidonglin
 */
public class FileCYGameLogMessage extends GameLogMessage {
    public List<String> valueList;

    public FileCYGameLogMessage(List<String> valueList, GameLogType gameLogType) {
        super(gameLogType);
        this.valueList = valueList;
    }
}
