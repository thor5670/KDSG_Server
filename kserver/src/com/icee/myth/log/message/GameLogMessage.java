/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.log.message;

/**
 *
 * @author liuxianke
 */
public abstract class GameLogMessage {
    // GameLogType是按日志写入位置分类
    public enum GameLogType {
        GAMELOGTYPE_F_DEBUG,
        GAMELOGTYPE_F_DBERR,
        GAMELOGTYPE_F_NETERR,
        GAMELOGTYPE_F_DBDOWN_CACHE,
        GAMELOGTYPE_DB_BEHAVIOR,
        GAMELOGTYPE_DB_CREATECHAR,
        GAMELOGTYPE_DB_LOGINLOGOUT,
        GAMELOGTYPE_DB_ONLINENUM,
        GAMELOGTYPE_DB_CASH,
        GAMELOGTYPE_DB_GOLD,
        GAMELOGTYPE_DB_SILVER,
        GAMELOGTYPE_CY_LOGIN,
        GAMELOGTYPE_CY_REGISTER,
        GAMELOGTYPE_CY_BEHAVIOR,
        GAMELOGTYPE_CY_RECHARGE,
        GAMELOGTYPE_CY_ROLE,
        GAMELOGTYPE_CY_ONLINE,
        GAMELOGTYPE_CY_ONLINE_TIME,
        GAMELOGTYPE_CY_USER,
        GAMELOGTYPE_CY_COMMODITY_BUY,
        GAMELOGTYPE_CY_COMMODITY_COST,
        GAMELOGTYPE_CY_DIAMOND,
        GAMELOGTYPE_CY_TASK,
        GAMELOGTYPE_CY_DOWNLOAD,
        GAMELOGTYPE_CY_CARD,
        GAMELOGTYPE_CY_INSTANCE,
        GAMELOGTYPE_CY_EVOLUTION,
        GAMELOGTYPE_CY_ACTIVATE,

    };
    
    public final GameLogType type;

    public GameLogMessage(GameLogType type) {
        this.type = type;
    }
}
