/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.log.message;

/**
 *
 * @author liuxianke
 */
public class DBCashGameLogMessage extends GameLogMessage {
    public final int playerId;
    public final int qbNum;
    public final long time;

    public DBCashGameLogMessage(int playerId, int qbNum, long time) {
        super(GameLogType.GAMELOGTYPE_DB_CASH);
        this.playerId = playerId;
        this.qbNum = qbNum;
        this.time = time;
    }
}
