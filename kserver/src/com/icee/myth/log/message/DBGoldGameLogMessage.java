/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.log.message;

/**
 *
 * @author liuxianke
 */
public class DBGoldGameLogMessage extends GameLogMessage {
    public final int playerId;
    public final int mainType;
    public final int subtype;
    public final int change1;
    public final int change2;
    public final int remain1;
    public final int remain2;
    public final int lv;
    public final long time;

    public DBGoldGameLogMessage(int playerId, int mainType, int subType, int change1, int change2, int remain1, int remain2, int lv, long time) {
        super(GameLogType.GAMELOGTYPE_DB_GOLD);
        this.playerId = playerId;
        this.mainType = mainType;
        this.subtype = subType;
        this.change1 = change1;
        this.change2 = change2;
        this.remain1 = remain1;
        this.remain2 = remain2;
        this.lv = lv;
        this.time = time;
    }
}
