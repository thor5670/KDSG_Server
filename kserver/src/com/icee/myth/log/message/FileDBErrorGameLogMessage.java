/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.log.message;

/**
 *
 * @author liuxianke
 */
public class FileDBErrorGameLogMessage extends GameLogMessage {
    public String msg;

    public FileDBErrorGameLogMessage(String msg) {
        super(GameLogType.GAMELOGTYPE_F_DBERR);
        this.msg = msg;
    }
}
