/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.common.player.state;


import com.icee.myth.common.player.Player;
import com.icee.myth.common.message.serverMessage.Message;

/**
 *
 * @author liuxianke
 */
public interface PlayerState {
    public boolean handleMessage(Player player, Message message);
}
