/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.common.charInfo;

import com.icee.myth.protobuf.InternalCommonProtocol.DBOccupyInfoProto;

/**
 *
 * @author liuxianke
 */
public class CharOccupyInfo {
    public int mid;
    public String name;
    public int level;
    public int rank;
    public int leaderCardId;
    public int leaderCardLevel;
    public DBOccupyInfoProto occupyInfo;
}
