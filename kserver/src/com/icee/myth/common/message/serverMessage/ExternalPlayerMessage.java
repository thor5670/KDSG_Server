/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.common.message.serverMessage;

import org.jboss.netty.channel.Channel;

/**
 * 从游戏客户端来的消息
 * @author liuxianke
 */
public class ExternalPlayerMessage extends SimpleMessage {
    public final Channel channel;     // The channel which message is received from

    public ExternalPlayerMessage(MessageType type, Channel channel) {
        super(type);
        this.channel = channel;
    }

}
