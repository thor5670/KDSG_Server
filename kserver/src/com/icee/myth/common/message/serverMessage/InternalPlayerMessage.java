/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.common.message.serverMessage;

/**
 * 内部服务器间产生的与player相关的消息
 * @author liuxianke
 */
public class InternalPlayerMessage extends SimpleMessage {
    public final int playerId;  // The player's id

    public InternalPlayerMessage(MessageType type, int playerId) {
        super(type);
        this.playerId = playerId;
    }

}
