/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.common.message.dbMessage;

/**
 *
 * @author liuxianke
 */
public interface DBMessage {
    /**
     * 消息类型(前缀表示消息所在服务器)   -- 消息说明
     *
     */
    public enum DBMessageType {
        GET_CHAR_NUM,           // 获取角色数量
        CREATE_CHAR,            // 创建角色
        GET_CHAR_DETAIL,        // 获取角色信息
        SAVE_CHAR_INFO,         // 保存角色信息
        GET_CHAR_OCCUPY_INFO,   // 获取角色臣属信息
        SAVE_CHAR_OCCUPY_INFO,  // 保存角色臣属信息
        GET_RELATION,           // 获取好友关系
        SAVE_RELATION,          // 保存好友关系
        SAVE_BATTLE_RESULT,     // 保存战斗结果
        GET_BATTLE_RESULT,      // 过去战斗录像
        GET_BRIEF_PLAYER_INFOS, // 获取列表中玩家简略信息
        SAVE_BILL,              // 保存订单处理结果
        CONSUMER_LOG,           //保存充值消费 记录
        GET_COUPON,             // 获取礼券
        SAVE_MAIL,              // 保存补偿记录
        GET_NEW_MAIL,           // 获取新补偿信息
        SAVE_RELIC,             //保存遗迹
        SAVE_LAST_MILESTONE_REWARD_TIME,  // 保存最后一次排名赛和军团赛发奖时间
        CHANGE_NAME,            //修改名称
        ADD_NORMALACTIVITY,     //添加活动
        DELETE_NORMALACTIVITY,  //删除活动
        SHUTDOWN                // 关闭DB线程
    }

    public DBMessageType getType();
}
