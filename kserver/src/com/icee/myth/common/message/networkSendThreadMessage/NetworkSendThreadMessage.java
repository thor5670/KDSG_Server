/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.common.message.networkSendThreadMessage;

/**
 *
 * @author liuxianke
 */
public interface NetworkSendThreadMessage {
    /**
     * 消息类型(前缀表示消息所在服务器)   -- 消息说明
     *
     */
    public enum NetworkSendThreadMessageType {
        NSTMT_ADDCHANNELCONTEXT     // 加入新channel
    }

    public NetworkSendThreadMessageType getType();
}
