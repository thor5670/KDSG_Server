/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icee.myth.protobuf.builder;

import com.icee.myth.common.protobufmessage.ProtobufMessage;
import com.icee.myth.common.protobufmessage.ProtobufMessageType;
import com.icee.myth.protobuf.ExternalCommonProtocol.BaseResistanceBattleResultProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.BaseTargetsProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.BattleResultProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.BigStageStatusChangeProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.BriefPlayerProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.BriefPlayersProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.CardDrawActivitiesProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.CardDrawedProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.CardLevelExperienceProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.CardProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.MailProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.ContSignProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.CountChangeProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.EnterGameCharProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.EnterGameRetProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.FriendLeaderCardChangeProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.HegemonyBattleBoxProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.HegemonyBattleResultProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.HegemonyGetPayProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.HegemonyTargetsProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.IntValueProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.IntValuesProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.LivenessChangeProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.LongValueProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.MailListProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.MailNumProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.MineIncomeProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.NormalActivitiesProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.NormalActivityItemsProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.NoteProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.OccupyItemProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.OccupyProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.QuestsProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.SandboxProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.StageActivitiesProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.StageLeaveProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.StageStateProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.VariableValueProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.VipProto;
import com.icee.myth.server.base.occupy.CapeCollinson;
import com.icee.myth.server.base.occupy.OccupyInfo;
import com.icee.myth.server.card.cardDraw.CardDrawItem;
import com.icee.myth.server.quest.Quest;
import com.icee.myth.server.reward.CertainRewardInfo;
import com.icee.myth.server.social.BriefPlayerInfo;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * 构造服务端发送给客户端的所有消息（ProtobufMessage）
 * @author liuxianke
 */
public class ClientToMapBuilder {

    public static ProtobufMessage buildLoginRetNoChar() {
        return new ProtobufMessage(ProtobufMessageType.S2C_NOCHARRET, null);
    }

    public static ProtobufMessage buildLoginFail() {
        return new ProtobufMessage(ProtobufMessageType.S2C_LOGINERROR, null);
    }

    public static ProtobufMessage buildCreateCharError() {
        return new ProtobufMessage(ProtobufMessageType.S2C_CREATECHARERROR, null);
    }

    public static ProtobufMessage buildEnterGameRet(EnterGameCharProto enterGameCharProto) {
        EnterGameRetProto.Builder builder = EnterGameRetProto.newBuilder();

        builder.setResult(0);
        builder.setEnterGameChar(enterGameCharProto);

        return new ProtobufMessage(ProtobufMessageType.S2C_ENTERGAMERET, builder.build());
    }

    public static ProtobufMessage buildGuideStepChange(int guideStep) {
        IntValueProto.Builder builder = IntValueProto.newBuilder();

        builder.setValue(guideStep);

        return new ProtobufMessage(ProtobufMessageType.S2C_GUIDE_STEP_CHANGE, builder.build());
    }

    public static ProtobufMessage buildCardAdd(int cardInstId, int id, int level) {
        CardProto.Builder builder = CardProto.newBuilder();
        builder.setId(id);
        builder.setInstId(cardInstId);
        builder.setLevel(level);

        return new ProtobufMessage(ProtobufMessageType.S2C_CARD_ADD, builder.build());
    }

    public static ProtobufMessage buildCardRemove(int cardInstId) {
        IntValueProto.Builder builder = IntValueProto.newBuilder();
        builder.setValue(cardInstId);

        return new ProtobufMessage(ProtobufMessageType.S2C_CARD_REMOVE, builder.build());
    }

    public static ProtobufMessage buildCardLevelExperienceChange(int cardInstId, int cardLevel, int cardExp) {
        CardLevelExperienceProto.Builder builder = CardLevelExperienceProto.newBuilder();

        builder.setCardInstId(cardInstId);
        builder.setLevel(cardLevel);
        builder.setExperience(cardExp);

        return new ProtobufMessage(ProtobufMessageType.S2C_CARD_LEVEL_EXPERIENCE_CHANGE, builder.build());
    }

    public static ProtobufMessage buildCardTransform(int cardInstId, int cardId) {
        VariableValueProto.Builder builder = VariableValueProto.newBuilder();

        builder.setId(cardInstId);
        builder.setValue(cardId);

        return new ProtobufMessage(ProtobufMessageType.S2C_CARD_TRANSFORM, builder.build());
    }

    public static ProtobufMessage buildCardDrawed(CardDrawItem cardDrawItem) {
        CardDrawedProto.Builder builder1 = CardDrawedProto.newBuilder();

        VariableValueProto.Builder builder2 = VariableValueProto.newBuilder();
        builder2.setId(cardDrawItem.cardId);
        builder2.setValue(cardDrawItem.level);

        builder1.addCards(builder2);

        return new ProtobufMessage(ProtobufMessageType.S2C_CARD_DRAWED, builder1.build());
    }

    public static ProtobufMessage buildCardDrawed(ArrayList<CardDrawItem> cardDrawItems) {
        CardDrawedProto.Builder builder1 = CardDrawedProto.newBuilder();

        for (CardDrawItem cardDrawItem : cardDrawItems) {
            VariableValueProto.Builder builder2 = VariableValueProto.newBuilder();
            builder2.setId(cardDrawItem.cardId);
            builder2.setValue(cardDrawItem.level);

            builder1.addCards(builder2);
        }

        return new ProtobufMessage(ProtobufMessageType.S2C_CARD_DRAWED, builder1.build());
    }

    public static ProtobufMessage buildTalk(byte[] info) {
        return new ProtobufMessage(ProtobufMessageType.S2C_TALK, info);
    }

    public static ProtobufMessage buildLevelup(int lv) {
        IntValueProto.Builder builder = IntValueProto.newBuilder();
        builder.setValue(lv);

        return new ProtobufMessage(ProtobufMessageType.S2C_LEVELUP, builder.build());
    }

    public static ProtobufMessage buildRankLevelup(int rankLv) {
        IntValueProto.Builder builder = IntValueProto.newBuilder();
        builder.setValue(rankLv);

        return new ProtobufMessage(ProtobufMessageType.S2C_RANK_LEVELUP, builder.build());
    }

    public static ProtobufMessage buildStageState(StageStateProto stageStateProto) {
        return new ProtobufMessage(ProtobufMessageType.S2C_STAGE_STATE, stageStateProto);
    }

    public static ProtobufMessage buildLeaveStage(int stageId, int stageType) {
        StageLeaveProto.Builder builder = StageLeaveProto.newBuilder();
        builder.setStageId(stageId);
        builder.setStageType(stageType);

        return new ProtobufMessage(ProtobufMessageType.S2C_STAGE_LEAVE, builder.build());
    }

    public static ProtobufMessage buildPveBattleResult(BattleResultProto battleResult) {
        return new ProtobufMessage(ProtobufMessageType.S2C_PVEBATTLERESULT, battleResult);
    }

    public static ProtobufMessage buildBagSizeChange(int size) {
        IntValueProto.Builder builder = IntValueProto.newBuilder();
        builder.setValue(size);

        return new ProtobufMessage(ProtobufMessageType.S2C_BAGSIZE_CHANGE, builder.build());
    }

    public static ProtobufMessage buildSilverChange(long value) {
        LongValueProto.Builder builder = LongValueProto.newBuilder();
        builder.setValue(value);

        return new ProtobufMessage(ProtobufMessageType.S2C_SILVER_CHANGE, builder.build());
    }

    public static ProtobufMessage buildGold1Change(int value) {
        IntValueProto.Builder builder = IntValueProto.newBuilder();
        builder.setValue(value);

        return new ProtobufMessage(ProtobufMessageType.S2C_GOLD1_CHANGE, builder.build());
    }

    public static ProtobufMessage buildGold2Change(int value) {
        IntValueProto.Builder builder = IntValueProto.newBuilder();
        builder.setValue(value);

        return new ProtobufMessage(ProtobufMessageType.S2C_GOLD2_CHANGE, builder.build());
    }

    public static ProtobufMessage buildEnergyChange(int value) {
        IntValueProto.Builder builder = IntValueProto.newBuilder();
        builder.setValue(value);

        return new ProtobufMessage(ProtobufMessageType.S2C_ENERGY_CHANGE, builder.build());
    }

    public static ProtobufMessage buildTokenChange(int value) {
        IntValueProto.Builder builder = IntValueProto.newBuilder();
        builder.setValue(value);

        return new ProtobufMessage(ProtobufMessageType.S2C_TOKEN_CHANGE, builder.build());
    }

    public static ProtobufMessage buildExperienceChange(int experience) {
        IntValueProto.Builder builder = IntValueProto.newBuilder();
        builder.setValue(experience);

        return new ProtobufMessage(ProtobufMessageType.S2C_EXPERIENCE_CHANGE, builder.build());
    }

    public static ProtobufMessage buildRankExperienceChange(int experience) {
        IntValueProto.Builder builder = IntValueProto.newBuilder();
        builder.setValue(experience);

        return new ProtobufMessage(ProtobufMessageType.S2C_RANK_EXPERIENCE_CHANGE, builder.build());
    }

    public static ProtobufMessage buildCurrentBigStageIdChange(int stageId) {
        IntValueProto.Builder builder = IntValueProto.newBuilder();
        builder.setValue(stageId);

        return new ProtobufMessage(ProtobufMessageType.S2C_CURRENT_BIG_STAGE_ID_CHANGE, builder.build());
    }

    public static ProtobufMessage buildCurrentNormalStageIdChange(int stageId) {
        IntValueProto.Builder builder = IntValueProto.newBuilder();
        builder.setValue(stageId);

        return new ProtobufMessage(ProtobufMessageType.S2C_CURRENT_NORMAL_STAGE_ID_CHANGE, builder.build());
    }

    public static ProtobufMessage buildBigStageStatusChange(int stageId, int enterNum, int lastDate) {
        BigStageStatusChangeProto.Builder builder = BigStageStatusChangeProto.newBuilder();
        builder.setStageId(stageId);
        builder.setEnterCount(enterNum);
        builder.setLastDate(lastDate);

        return new ProtobufMessage(ProtobufMessageType.S2C_BIGSTAGE_STATUS_CHANGE, builder.build());
    }

    public static ProtobufMessage buildSandBoxInfo(SandboxProto sandboxProto) {
        return new ProtobufMessage(ProtobufMessageType.S2C_SANDBOX_INFO, sandboxProto);
    }

    public static ProtobufMessage buildRelationInfo(BriefPlayersProto briefPlayersProto) {
        return new ProtobufMessage(ProtobufMessageType.S2C_RELATION_INFO, briefPlayersProto);
    }

    public static ProtobufMessage buildConcernsInfo(BriefPlayersProto briefPlayersProto) {
        return new ProtobufMessage(ProtobufMessageType.S2C_CONCERNS_INFO, briefPlayersProto);
    }

    public static ProtobufMessage buildFriendEntered(int playerId) {
        IntValueProto.Builder builder = IntValueProto.newBuilder();
        builder.setValue(playerId);

        return new ProtobufMessage(ProtobufMessageType.S2C_FRIEND_ENTERED, builder.build());
    }

    public static ProtobufMessage buildFriendLeft(int playerId) {
        IntValueProto.Builder builder = IntValueProto.newBuilder();
        builder.setValue(playerId);

        return new ProtobufMessage(ProtobufMessageType.S2C_FRIEND_LEFT, builder.build());
    }

    public static ProtobufMessage buildSocialRemoveConcern(int playerId) {
        IntValueProto.Builder builder = IntValueProto.newBuilder();
        builder.setValue(playerId);

        return new ProtobufMessage(ProtobufMessageType.S2C_SOCIAL_REMOVE_CONCERN, builder.build());
    }

    public static ProtobufMessage buildBecomeFriend(BriefPlayerProto briefPlayerProto) {
        return new ProtobufMessage(ProtobufMessageType.S2C_SOCIAL_BECOME_FRIEND, briefPlayerProto);
    }

    public static ProtobufMessage buildBecomeConcern(int playerId) {
        IntValueProto.Builder builder = IntValueProto.newBuilder();
        builder.setValue(playerId);

        return new ProtobufMessage(ProtobufMessageType.S2C_SOCIAL_BECOME_CONCERN, builder.build());
    }


//    public static ProtobufMessage buildOtherPlayerDetail(OtherPlayerInfoProto buildOtherPlayerInfoProto) {
//        return new ProtobufMessage(ProtobufMessageType.S2C_SOCIAL_OTHERPLAYERDETAIL, buildOtherPlayerInfoProto);
//    }

    public static ProtobufMessage buildNotification(NoteProto noteProto) {
        return new ProtobufMessage(ProtobufMessageType.S2C_NOTIFICATION, noteProto);
    }

    public static ProtobufMessage buildVipChange(VipProto vipProto) {
        return new ProtobufMessage(ProtobufMessageType.S2C_VIP_CHANGE, vipProto);
    }

    public static ProtobufMessage buildGetCoupon(CertainRewardInfo reward) {
        return new ProtobufMessage(ProtobufMessageType.S2C_GET_COUPON, reward.buildRewardProto());
    }

    public static ProtobufMessage buildGetCouponError() {
        return new ProtobufMessage(ProtobufMessageType.S2C_GET_COUPON_ERROR, null);
    }

    public static ProtobufMessage buildBuyEnergy(int buyOracleCount, int lastBuyOracleDay) {
        CountChangeProto.Builder builder = CountChangeProto.newBuilder();
        builder.setCount(buyOracleCount);
        builder.setLastday(lastBuyOracleDay);
        return new ProtobufMessage(ProtobufMessageType.S2C_BUY_ENERGY_COUNT_CHANGE, builder.build());
    }

    public static ProtobufMessage buildRealTime(long realTime) {
        LongValueProto.Builder builder = LongValueProto.newBuilder();
        builder.setValue(realTime);

        return new ProtobufMessage(ProtobufMessageType.S2C_REALTIME, builder.build().toByteArray());
    }

    public static ProtobufMessage buildMailList(MailListProto mailListProto) {
        return new ProtobufMessage(ProtobufMessageType.S2C_MAIL_LIST, mailListProto);
    }

    public static ProtobufMessage buildMailNumChange(MailNumProto mailNumProto) {
        return new ProtobufMessage(ProtobufMessageType.S2C_MAIL_NUM_CHANGE, mailNumProto);
    }

    public static ProtobufMessage buildMailInfo(MailProto mailProto) {
        return new ProtobufMessage(ProtobufMessageType.S2C_MAIL_INFO, mailProto);
    }

    public static ProtobufMessage buildMailGotten(long mailId) {
        LongValueProto.Builder builder = LongValueProto.newBuilder();
        builder.setValue(mailId);

        return new ProtobufMessage(ProtobufMessageType.S2C_MAIL_GOTTEN, builder.build());
    }

    public static ProtobufMessage buildMailRemove(long mailId) {
        LongValueProto.Builder builder = LongValueProto.newBuilder();
        builder.setValue(mailId);

        return new ProtobufMessage(ProtobufMessageType.S2C_MAIL_REMOVE, builder.build());
    }

    public static ProtobufMessage buildNormalActivityRewardGotten(int activityId, int itemId) {
        VariableValueProto.Builder builder = VariableValueProto.newBuilder();
        builder.setId(activityId);
        builder.setValue(itemId);

        return new ProtobufMessage(ProtobufMessageType.S2C_NORMAL_ACTIVITY_GOTTEN, builder.build());
    }

    public static ProtobufMessage buildNormalActivityOperateTime(long nextAllowOperateTime) {
        LongValueProto.Builder builder = LongValueProto.newBuilder();
        builder.setValue(nextAllowOperateTime);

        return new ProtobufMessage(ProtobufMessageType.S2C_NORMAL_ACTIVITY_OPERATE_TIME, builder.build());
    }

    public static ProtobufMessage buildNormalActivityList(NormalActivitiesProto normalActivitiesProto) {
        return new ProtobufMessage(ProtobufMessageType.S2C_NORMAL_ACTIVITY_LIST, normalActivitiesProto);
    }

    public static ProtobufMessage buildNormalActivityItemList(NormalActivityItemsProto normalActivityItemsProto) {
        return new ProtobufMessage(ProtobufMessageType.S2C_NORMAL_ACTIVITY_ITEM_LIST, normalActivityItemsProto);
    }

    public static ProtobufMessage buildCardDrawActivityList(CardDrawActivitiesProto cardDrawActivitiesProto) {
        return new ProtobufMessage(ProtobufMessageType.S2C_CARD_DRAW_ACTIVITY_LIST, cardDrawActivitiesProto);
    }

    public static ProtobufMessage buildStageActivityList(StageActivitiesProto stageActivitiesProto) {
        return new ProtobufMessage(ProtobufMessageType.S2C_STAGE_ACTIVITY_LIST, stageActivitiesProto);
    }

    public static ProtobufMessage buildForbidTalk(long endForbidTalkTime) {
        LongValueProto.Builder builder = LongValueProto.newBuilder();
        builder.setValue(endForbidTalkTime);

        return new ProtobufMessage(ProtobufMessageType.S2C_FORBIDTALK, builder.build());
    }

    public static ProtobufMessage buildItemNumChange(int id, int num) {
        VariableValueProto.Builder builder = VariableValueProto.newBuilder();
        builder.setId(id);
        builder.setValue(num);

        return new ProtobufMessage(ProtobufMessageType.S2C_ITEM_NUM_CHANGE, builder.build());
    }

    public static ProtobufMessage buildItemCombined(int id) {
        IntValueProto.Builder builder = IntValueProto.newBuilder();
        builder.setValue(id);

        return new ProtobufMessage(ProtobufMessageType.S2C_ITEM_COMBINED, builder.build());
    }

    public static ProtobufMessage buildStrangers(LinkedList<BriefPlayerInfo> strangers, int myId) {
        BriefPlayersProto.Builder builder = BriefPlayersProto.newBuilder();

        for (BriefPlayerInfo briefPlayerInfo : strangers) {
        	if(briefPlayerInfo.id == myId) {
        		continue;
        	}
            builder.addConcerns(briefPlayerInfo.buildBriefPlayerProto(false, 0));
        }

        return new ProtobufMessage(ProtobufMessageType.S2C_STRANGERS, builder.build());
    }

    public static ProtobufMessage buildFriendHelpCooldown(int friendId, long cooldown) {
        VariableValueProto.Builder builder = VariableValueProto.newBuilder();

        builder.setId(friendId);
        builder.setValue(cooldown);

        return new ProtobufMessage(ProtobufMessageType.S2C_FRIEND_HELP_COOLDOWN, builder.build());
    }

    public static ProtobufMessage buildFriendLeaderCardChange(int friendId, int leaderCardId, int leaderCardLevel) {
        FriendLeaderCardChangeProto.Builder builder = FriendLeaderCardChangeProto.newBuilder();

        builder.setFriendId(friendId);
        builder.setLeaderCardId(leaderCardId);
        builder.setLeaderCardLevel(leaderCardLevel);

        return new ProtobufMessage(ProtobufMessageType.S2C_FRIEND_LEADER_CARD_CHANGE, builder.build().toByteArray());
    }

    public static ProtobufMessage buildFriendLevelChange(int friendId, int level) {
        VariableValueProto.Builder builder = VariableValueProto.newBuilder();

        builder.setId(friendId);
        builder.setValue(level);

        return new ProtobufMessage(ProtobufMessageType.S2C_FRIEND_LEVEL_CHANGE, builder.build().toByteArray());
    }

    public static ProtobufMessage buildAddConcernSuccess(int concernId) {
        IntValueProto.Builder builder = IntValueProto.newBuilder();

        builder.setValue(concernId);

        return new ProtobufMessage(ProtobufMessageType.S2C_ADD_CONCERN_SUCCESS, builder.build());
    }

    public static ProtobufMessage buildAddConcernFail(int concernId) {
        IntValueProto.Builder builder = IntValueProto.newBuilder();

        builder.setValue(concernId);

        return new ProtobufMessage(ProtobufMessageType.S2C_ADD_CONCERN_FAIL, builder.build());
    }

    public static ProtobufMessage buildQuestFinished(int questId) {
        IntValueProto.Builder builder = IntValueProto.newBuilder();

        builder.setValue(questId);

        return new ProtobufMessage(ProtobufMessageType.S2C_QUEST_FINISHED, builder.build());
    }

    public static ProtobufMessage buildQuestSubmitted(int questId) {
        IntValueProto.Builder builder = IntValueProto.newBuilder();

        builder.setValue(questId);

        return new ProtobufMessage(ProtobufMessageType.S2C_QUEST_SUBMITTED, builder.build());
    }

    public static ProtobufMessage buildQuestAddList(LinkedList<Quest> quests) {
        QuestsProto.Builder builder = QuestsProto.newBuilder();

        for (Quest quest : quests) {
            builder.addQuests(quest.buildQuestProto());
        }

        return new ProtobufMessage(ProtobufMessageType.S2C_QUEST_ADD_LIST, builder.build());
    }

    public static ProtobufMessage buildCumulativeRewardReceived() {
        return new ProtobufMessage(ProtobufMessageType.S2C_CONTSIGN_CUMULATIVE_REWARD_RECEIVED, null);
    }

    public static ProtobufMessage buildReceivedConsecutiveSignReward(int day) {
        IntValueProto.Builder builder = IntValueProto.newBuilder();
        builder.setValue(day);
        return new ProtobufMessage(ProtobufMessageType.S2C_CONTSIGN_CONSECUTIVE_REWARD_RECEIVED, builder.build());
    }

    public static ProtobufMessage buildReceiveLivenessReward(int index) {
        IntValueProto.Builder builder = IntValueProto.newBuilder();
        builder.setValue(index);
        return new ProtobufMessage(ProtobufMessageType.S2C_CONTSIGN_LIVENESSREWARD, builder.build());
    }

    public static ProtobufMessage buildContSign(ContSignProto contSignProto) {
        return new ProtobufMessage(ProtobufMessageType.S2C_CONTSIGN, contSignProto);
    }

    public static ProtobufMessage buildLivenessChange(int liveness, int type, int value) {
        LivenessChangeProto.Builder builder = LivenessChangeProto.newBuilder();
        builder.setLiveness(liveness);
        builder.setType(type);
        builder.setValue(value);
        return new ProtobufMessage(ProtobufMessageType.S2C_CONTSIGN_LIVENESS_CHANGE, builder.build());
    }

    public static ProtobufMessage buildVipReceived(int receiveFlag) {
        IntValueProto.Builder builder = IntValueProto.newBuilder();
        builder.setValue(receiveFlag);

        return new ProtobufMessage(ProtobufMessageType.S2C_VIPGIFT_RECEIVE, builder.build());
    }

    public static ProtobufMessage buildStageRevived() {
        return new ProtobufMessage(ProtobufMessageType.S2C_STAGE_REVIVED, null);
    }

    public static ProtobufMessage buildHegemonyBattleResult(BattleResultProto battleResult, boolean stimulate, int rewardSilver, int rewardRankExp, CardDrawItem cardDrawItem, boolean isBigDraw) {
        HegemonyBattleResultProto.Builder builder1 = HegemonyBattleResultProto.newBuilder();

        builder1.setBattleResult(battleResult);
        builder1.setStimulated(stimulate);
        builder1.setSilver(rewardSilver);
        builder1.setRankExp(rewardRankExp);
        
        if (cardDrawItem != null) {
            HegemonyBattleBoxProto.Builder builder2 = HegemonyBattleBoxProto.newBuilder();
            
            builder2.setCardId(cardDrawItem.cardId);
            builder2.setCardLevel(cardDrawItem.level);
            
            builder2.setIsBig(isBigDraw);

            builder1.setBox(builder2);
        }

        return new ProtobufMessage(ProtobufMessageType.S2C_HEGEMONY_BATTLERESULT, builder1.build());
    }

    public static ProtobufMessage buildHegemonyTargetStatusChange(int targetSlot, int status) {
        VariableValueProto.Builder builder = VariableValueProto.newBuilder();

        builder.setId(targetSlot);
        builder.setValue(status);

        return new ProtobufMessage(ProtobufMessageType.S2C_HEGEMONY_TARGET_STATUS_CHANGE, builder.build());
    }

    public static ProtobufMessage buildRefreshHegemony(int refreshHegemonyCount, int lastRefreshHegemonyDay) {
        CountChangeProto.Builder builder = CountChangeProto.newBuilder();
        builder.setCount(refreshHegemonyCount);
        builder.setLastday(lastRefreshHegemonyDay);
        return new ProtobufMessage(ProtobufMessageType.S2C_REFRESH_HEGEMONY_COUNT_CHANGE, builder.build());
    }

    public static ProtobufMessage buildHegemonyTargetsChange(HegemonyTargetsProto hegemonyTargetsProto) {
        return new ProtobufMessage(ProtobufMessageType.S2C_HEGEMONY_TARGETS_CHANGE, hegemonyTargetsProto);
    }

    public static ProtobufMessage buildHegemonyGetPay(int lastHegemonyPayDay, int silver, int gold) {
        HegemonyGetPayProto.Builder builder = HegemonyGetPayProto.newBuilder();
        builder.setLastHegemonyPayDay(lastHegemonyPayDay);

        if (silver > 0) {
            builder.setSilver(silver);
        }

        if (gold > 0) {
            builder.setGold(gold);
        }

        return new ProtobufMessage(ProtobufMessageType.S2C_HEGEMONY_GET_PAY, builder.build());
    }

    public static ProtobufMessage buildTrainingChange(IntValuesProto intValuesProto) {
        return new ProtobufMessage(ProtobufMessageType.S2C_BASE_TRAINING_INFO, intValuesProto);
    }

    public static ProtobufMessage buildMineChange(IntValuesProto intValuesProto) {
        return new ProtobufMessage(ProtobufMessageType.S2C_BASE_MINE_INFO, intValuesProto);
    }

    public static ProtobufMessage buildBarrackChange(IntValuesProto intValuesProto) {
        return new ProtobufMessage(ProtobufMessageType.S2C_BASE_BARRACK_INFO, intValuesProto);
    }

    public static ProtobufMessage buildOrdnanceChange(IntValuesProto intValuesProto) {
        return new ProtobufMessage(ProtobufMessageType.S2C_BASE_ORDNANCE_INFO, intValuesProto);
    }

    public static ProtobufMessage buildCouncilChange(IntValuesProto intValuesProto) {
        return new ProtobufMessage(ProtobufMessageType.S2C_BASE_COUNCIL_INFO, intValuesProto);
    }

    public static ProtobufMessage buildOccupyInfo(OccupyInfo selfOccupyInfo, OccupyInfo kingOccupyInfo, List<OccupyInfo> capeOccupyInfos) {
        OccupyProto.Builder builder1 = OccupyProto.newBuilder();
        
        if (selfOccupyInfo.king != null) {
            OccupyItemProto.Builder builder2 = OccupyItemProto.newBuilder();
            builder2.setId(kingOccupyInfo.id);
            builder2.setName(kingOccupyInfo.name);
            builder2.setLevel(kingOccupyInfo.level);
            builder2.setRank(kingOccupyInfo.rank);
            builder2.setLeaderCardId(kingOccupyInfo.leaderCardId);
            builder2.setLeaderCardLevel(kingOccupyInfo.leaderCardLevel);
            
            CapeCollinson capeCollinson = kingOccupyInfo.getCapeCollinson(selfOccupyInfo.id);
            builder2.setProduction(capeCollinson.production);
            builder2.setStartTime(capeCollinson.startTime);

            builder1.setKing(builder2);
        }

        if (capeOccupyInfos != null) {
            for (OccupyInfo capeOccupyInfo : capeOccupyInfos) {
                OccupyItemProto.Builder builder3 = OccupyItemProto.newBuilder();
                builder3.setId(capeOccupyInfo.id);
                builder3.setName(capeOccupyInfo.name);
                builder3.setLevel(capeOccupyInfo.level);
                builder3.setRank(capeOccupyInfo.rank);
                builder3.setLeaderCardId(capeOccupyInfo.leaderCardId);
                builder3.setLeaderCardLevel(capeOccupyInfo.leaderCardLevel);

                CapeCollinson capeCollinson = selfOccupyInfo.getCapeCollinson(capeOccupyInfo.id);
                builder3.setProduction(capeCollinson.production);
                builder3.setStartTime(capeCollinson.startTime);
                builder3.setFreeTime(capeCollinson.freeTime);

                builder1.addCapes(builder3);
            }
        }

        return new ProtobufMessage(ProtobufMessageType.S2C_BASE_OCCUPY_INFO, builder1.build());
    }

    public static ProtobufMessage buildMineIncomeInfo(OccupyInfo occupyInfo) {
        MineIncomeProto.Builder builder = MineIncomeProto.newBuilder();

        builder.setSilver(occupyInfo.silver);
        builder.setStartTime(occupyInfo.lastHarvestTime);

        return new ProtobufMessage(ProtobufMessageType.S2C_BASE_MINE_INCOME_INFO, builder.build());
    }

    public static ProtobufMessage buildBaseAddCapeCollinson(OccupyInfo occupyInfo, CapeCollinson capeCollinson) {
        OccupyItemProto.Builder builder = OccupyItemProto.newBuilder();
        builder.setId(occupyInfo.id);
        builder.setName(occupyInfo.name);
        builder.setLevel(occupyInfo.level);
        builder.setRank(occupyInfo.rank);
        builder.setLeaderCardId(occupyInfo.leaderCardId);
        builder.setLeaderCardLevel(occupyInfo.leaderCardLevel);

        builder.setProduction(capeCollinson.production);
        builder.setStartTime(capeCollinson.startTime);

        return new ProtobufMessage(ProtobufMessageType.S2C_BASE_ADD_CAPECOLLINSON, builder.build());
    }

    public static ProtobufMessage buildRemoveCapecollinson(int capeId) {
        IntValueProto.Builder builder = IntValueProto.newBuilder();

        builder.setValue(capeId);

        return new ProtobufMessage(ProtobufMessageType.S2C_BASE_REMOVE_CAPECOLLINSON, builder.build());
    }
    
    public static ProtobufMessage buildBaseTargets(BaseTargetsProto baseTargetsProto) {
        return new ProtobufMessage(ProtobufMessageType.S2C_BASE_TARGETS, baseTargetsProto);
    }

    public static ProtobufMessage buildBaseAttackBattleResult(BattleResultProto battleResult) {
        return new ProtobufMessage(ProtobufMessageType.S2C_BASE_ATTACK_BATTLE_RESULT, battleResult);
    }

    public static ProtobufMessage buildBaseResistanceBattleResult(BattleResultProto battleResult, boolean stimulate) {
        BaseResistanceBattleResultProto.Builder builder = BaseResistanceBattleResultProto.newBuilder();

        builder.setBattleResult(battleResult);
        builder.setStimulated(stimulate);

        return new ProtobufMessage(ProtobufMessageType.S2C_BASE_RESISTANCE_BATTLE_RESULT, builder.build());
    }

    public static ProtobufMessage buildBaseFree() {
        return new ProtobufMessage(ProtobufMessageType.S2C_BASE_FREE, null);
    }
}
