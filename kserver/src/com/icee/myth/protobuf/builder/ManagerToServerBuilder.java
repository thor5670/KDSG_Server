/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.protobuf.builder;

import com.icee.myth.common.protobufmessage.ProtobufMessage;
import com.icee.myth.common.protobufmessage.ProtobufMessageType;
import com.icee.myth.protobuf.ExternalCommonProtocol.IntValuesProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.VariableValueProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.VariableValuesProto;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.TreeMap;

/**
 *
 * @author liuxianke
 */
public class ManagerToServerBuilder {

    public static ProtobufMessage buildServerHeartbeat() {
        return new ProtobufMessage(ProtobufMessageType.SERVER2MANAGER_HEARTBEAT, null);
    }

    public static ProtobufMessage buildBattleWinRateResult(int winNum, int lostNum, int drawNum) {
        IntValuesProto.Builder builder = IntValuesProto.newBuilder();
        builder.addValues(winNum);
        builder.addValues(lostNum);
        builder.addValues(drawNum);

        return new ProtobufMessage(ProtobufMessageType.SERVER2MANAGER_BATTLE_WIN_RATE_RESULT, builder.build());
    }

    public static ProtobufMessage buildCardDrawRateResult(TreeMap<Integer, Integer> result) {
        VariableValuesProto.Builder builder1 = VariableValuesProto.newBuilder();

        for (Iterator<Entry<Integer, Integer>> iter = result.entrySet().iterator(); iter.hasNext(); ) {
            Entry<Integer, Integer> entry = iter.next();
            VariableValueProto.Builder builder2 = VariableValueProto.newBuilder();
            builder2.setId(entry.getKey());
            builder2.setValue(entry.getValue());

            builder1.addValues(builder2);
        }

        return new ProtobufMessage(ProtobufMessageType.SERVER2MANAGER_CARD_DRAW_RATE_RESULT, builder1.build());
    }

    public static ProtobufMessage buildUnintOccupyInfo(LinkedList<Integer> result) {
        IntValuesProto.Builder builder = IntValuesProto.newBuilder();

        for (Integer id : result) {
            builder.addValues(id);
        }

        return new ProtobufMessage(ProtobufMessageType.SERVER2MANAGER_UNINIT_OCCUPY_INFO, builder.build());
    }

    public static ProtobufMessage buildFightingOccupyInfo(LinkedList<Integer> result) {
        IntValuesProto.Builder builder = IntValuesProto.newBuilder();

        for (Integer id : result) {
            builder.addValues(id);
        }

        return new ProtobufMessage(ProtobufMessageType.SERVER2MANAGER_FIGHTING_OCCUPY_INFO, builder.build());
    }
}
