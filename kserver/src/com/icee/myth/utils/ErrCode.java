/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.utils;

/**
 *
 * @author yangyi
 */
public class ErrCode {
//    public final static int EQUIPMENT_LEVELPROMOTED_NO_BLUESOUL  = -1;  //装备升级蓝魂不够
//    public final static int EQUIPMENT_LEVELPROMOTED_NO_COOLDOWNCHANNEL  = -2;  //装备升级蓝魂不够
//    public final static int EQUIPMENT_LEVELPROMOTED_NO_LEVEL  = -3;  //装备升级不能超过玩家等级
//    public final static int EQUIPMENT_GRADEPROMOTED_NO_MATERIAL  = -4;  //装备升阶材料不够
//    public final static int EQUIPMENT_GRADEPROMOTED_NO_SCROLL  = -5;  //装备升阶卷轴不够
//    public final static int EQUIPMENT_GRADEPROMOTED_GREATESTGRADE  = -6;  //装备阶已经最高级
//    public final static int EQUIPMENT_COOLDOWN_NO_YELLOWSOUL  = -7;  //强制冷却没有黄魂或者买冷却通道黄魂不够
    public final static int BUYSLOT_NO_MONEY  = -11;  //扩展槽位黄魂不够
//    public final static int OPENBOX_BAG_NO_SPACE  = -12;  //打开宝箱,背包没有足够空间
//    public final static int OPENBOX_WAREHOUSE_NO_SPACE  = -13;  //打开宝箱,仓库没有足够空间
//    public final static int FORMATION_LEVELUP_NO_REDSOUL  = -21;  //阵型升级红魂不够
//    public final static int FORMATION_LEVELUP_NO_COOLDWONCHANNEL  = -22;  //阵型升级cooldown不够
//    public final static int STORE_BUYITEM_NOBLUESOUL  = -31;  //买商品蓝魂不够
//    public final static int STORE_BUYITEM_NOBAGSPACE  = -32;  //买商品背包空间不够
//    public final static int STORE_BUYITEM_NOWAREHOUSESPACE  = -33;  //买商品仓库空间不够
//    public final static int TACTICS_LEVELPROMOTED_NO_LEVEL  = -41;  //战术升级不能超过玩家等级的1/5
//    public final static int TACTICS_LEVELUP_NO_REDSOUL  = -51;  //战术升级红魂不够
//    public final static int TACTICS_LEVELUP_NO_COOLDWONCHANNEL  = -52;  //战术升级cooldown不够
//    public final static int QUESTSUBMIT_NO_BAGSPACE  = -61;  //交任务时背包已满
//    public final static int TRAIN_LEVELPROMOTED_NO_LEVEL  = -71;  //训练升级不能超过玩家等级
//    public final static int TRAIN_LEVELUP_NO_REDSOUL  = -72;  //训练升级红魂不够
//    public final static int TRAIN_LEVELUP_NO_COOLDWONCHANNEL  = -73;  //训练升级cooldown不够
//    public final static int ARITIFACT_OPENARMSTORE_NOBLUESOUL = -81;  //开武库蓝魂不足
//    public final static int ARITIFACT_OPENARMSTORE_NOSPACE = -82;     //开武库空间不足
////    public final static int ARITIFACT_NOCOST = -83;     //宝具挂在角色身上cost不够
////    public final static int ARITIFACT_MAXSAMETYPE = -84;     //角色身上统一类型宝具达到最大
//    public final static int ARITIFACT_ARMSTORE_LEVELUP_NOYELLOWSOUL = -85;     //升级武库黄魂不够
//    public final static int ARITIFACT_LEVELUP_NORESOUNACE = -86;     //宝具升级共鸣点不够
//    public final static int UNION_CREATE_REPEATNAME = -91;     //创建军团时军团名重复
//    public final static int UNION_INVITE_MEMBERINTHISUNION  = -92;  //团长邀请别人加入军团时，这个人已经在这个军团中
//    public final static int UNION_INVITE_REPEATEINVITE  = -93;  //团长邀请别人加入军团时，重复邀请
//    public final static int FOSTER_PROPMAXLIMIT = -101;        //培养属性值达到最大
////    public final static int FOSTER_UNLOCK_NOSCROLL = -102;        //培养解锁没有卷轴
//    public final static int DAILYQUEST_SUBMIT_NOBAGSPACE = -111;  //日常任务因为没有背包空间不能提交
//    public final static int HEROQUEST_NOYELLOWSOUL = -121;  //历练任务没有黄魂
//    public final static int AUTOFIGHT_BAGFULL = -123;     //清剿时背包满
//    public final static int TOWER_AUTOCLIMB_BAGFULL = - 131;  //自动爬巴别塔背包满
}
