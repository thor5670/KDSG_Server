///*
// * To change this template, choose Tools | Templates
// * and open the template in the editor.
// */
//
//package com.icee.myth.utils;
//
///**
// *
// * @author liuxianke
// */
//public class Square {
//    public int left;
//    public int top;
//    public int width;
//    public int height;
//
//    public Square(int left, int top, int width, int height) {
//        this.left = left;
//        this.top = top;
//        this.width = width;
//        this.height = height;
//    }
//
//    public boolean contain(Position pos) {
//        return (pos.x>left) && (pos.y>top) && (pos.x-left<width) && (pos.y-top<height);
//    }
//
//    public Position getRandomPosition() {
//        int randx = left + RandomGenerator.INSTANCE.generator.nextInt(width);
//        int randy = top + RandomGenerator.INSTANCE.generator.nextInt(height);
//        return new Position(randx, randy);
//    }
//}
