/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icee.myth.utils;

import java.io.File;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author chencheng
 */
public class XMLHelper {

    private Document document;

    public XMLHelper(String filename) {
        try {
            document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new File(filename));
        } catch (SAXException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (ParserConfigurationException ex) {
            ex.printStackTrace();
        }
    }

    public String getFirstlevelString(String firstlevel) {
        return getFirstlevelStringByIdx(firstlevel, 0);
    }

    public int getFirstlevelInt(String firstlevel) {
        return getFirstlevelIntByIdx(firstlevel, 0);
    }

    public String getFirstlevelStringByIdx(String firstlevel, int idx) {
        Element docEle = document.getDocumentElement();
        return ((Element) docEle.getElementsByTagName(firstlevel).item(idx)).getFirstChild().getNodeValue();
    }

    public int getFirstlevelIntByIdx(String firstlevel, int idx) {
        Element docEle = document.getDocumentElement();
        return Integer.parseInt(((Element) docEle.getElementsByTagName(firstlevel).item(idx)).getFirstChild().getNodeValue());
    }

    public int getFirstlevelLength(String firstlevel) {
        Element docEle = document.getDocumentElement();
        return docEle.getElementsByTagName(firstlevel).getLength();
    }

    public String getSecondlevelString(String firstlevel, String secondlevel) {
        return getSecondlevelStringByIdx(firstlevel, secondlevel, 0, 0);
    }

    public int getSecondlevelInt(String firstlevel, String secondlevel) {
        return getSecondlevelIntByIdx(firstlevel, secondlevel, 0, 0);
    }

    public String getSecondlevelStringByIdx1(String firstlevel, String secondlevel, int idx1) {
        return getSecondlevelStringByIdx(firstlevel, secondlevel, idx1, 0);
    }

    public int getSecondlevelIntByIdx1(String firstlevel, String secondlevel, int idx1) {
        return getSecondlevelIntByIdx(firstlevel, secondlevel, idx1, 0);
    }

    public String getSecondlevelStringByIdx(String firstlevel, String secondlevel, int idx1, int idx2) {
        Element docEle = document.getDocumentElement();
        Element elem = (Element) docEle.getElementsByTagName(firstlevel).item(idx1);

        NodeList nl;
        nl = elem.getElementsByTagName(secondlevel);
        return ((Element) nl.item(idx2)).getFirstChild().getNodeValue();
    }

    public int getSecondlevelIntByIdx(String firstlevel, String secondlevel, int idx1, int idx2) {
        Element docEle = document.getDocumentElement();
        Element elem = (Element) docEle.getElementsByTagName(firstlevel).item(idx1);

        NodeList nl;
        nl = elem.getElementsByTagName(secondlevel);
        return Integer.parseInt(((Element) nl.item(idx2)).getFirstChild().getNodeValue());
    }

    public int getSecondlevelLength(String firstlevel, String secondlevel) {
        Element docEle = document.getDocumentElement();
        Element elem = (Element) docEle.getElementsByTagName(firstlevel).item(0);

        return elem.getElementsByTagName(secondlevel).getLength();
    }

    public String getThirdlevelString(String firstlevel, String secondlevel, String thirdlevel) {
        return getThirdlevelStringByIdx(firstlevel, secondlevel, thirdlevel, 0, 0, 0);
    }

    public int getThirdlevelInt(String firstlevel, String secondlevel, String thirdlevel) {
        return getThirdlevelIntByIdx(firstlevel, secondlevel, thirdlevel, 0, 0, 0);
    }

    public String getThirdlevelStringByIdx1(String firstlevel, String secondlevel, String thirdlevel, int idx1) {
        return getThirdlevelStringByIdx(firstlevel, secondlevel, thirdlevel, idx1, 0, 0);
    }

    public int getThirdlevelIntByIdx1(String firstlevel, String secondlevel, String thirdlevel, int idx1) {
        return getThirdlevelIntByIdx(firstlevel, secondlevel, thirdlevel, idx1, 0, 0);
    }
    
    public String getThirdlevelStringByIdx2(String firstlevel, String secondlevel, String thirdlevel, int idx1, int idx2) {
        return getThirdlevelStringByIdx(firstlevel, secondlevel, thirdlevel, idx1, idx2, 0);
    }

    public int getThirdlevelIntByIdx2(String firstlevel, String secondlevel, String thirdlevel, int idx1, int idx2) {
        return getThirdlevelIntByIdx(firstlevel, secondlevel, thirdlevel, idx1, idx2, 0);
    }

    public String getThirdlevelStringByIdx(String firstlevel, String secondlevel, String thirdlevel, int idx1, int idx2, int idx3) {
        Element docEle = document.getDocumentElement();
        Element elem = (Element) docEle.getElementsByTagName(firstlevel).item(idx1);

        elem = (Element) elem.getElementsByTagName(secondlevel).item(idx2);

        NodeList nl;
        nl = elem.getElementsByTagName(thirdlevel);
        return ((Element) nl.item(idx3)).getFirstChild().getNodeValue();
    }

    public int getThirdlevelIntByIdx(String firstlevel, String secondlevel, String thirdlevel, int idx1, int idx2, int idx3) {
        Element docEle = document.getDocumentElement();
        Element elem = (Element) docEle.getElementsByTagName(firstlevel).item(idx1);

        elem = (Element) elem.getElementsByTagName(secondlevel).item(idx2);

        NodeList nl;
        nl = elem.getElementsByTagName(thirdlevel);
        return Integer.parseInt(((Element) nl.item(idx3)).getFirstChild().getNodeValue());
    }

    public int getThirdlevelLength(String firstlevel, String secondlevel, String thirdlevel) {
        Element docEle = document.getDocumentElement();
        Element elem = (Element) docEle.getElementsByTagName(firstlevel).item(0);

        elem = (Element) elem.getElementsByTagName(secondlevel).item(0);

        return elem.getElementsByTagName(thirdlevel).getLength();
    }

    public int getIdx2ByThirdlevelInt(String firstlevel, String secondlevel, String thirdlevel, int value) {
        Element docEle = document.getDocumentElement();
        Element elem = (Element) docEle.getElementsByTagName(firstlevel).item(0);
        NodeList nodelist = elem.getElementsByTagName(secondlevel);
        NodeList nl;
        Element e = null;

        int v;
        for (int i = 0; i < nodelist.getLength(); i++) {
            e = (Element) nodelist.item(i);
            nl = e.getElementsByTagName(thirdlevel);
            v = Integer.parseInt(((Element) nl.item(0)).getFirstChild().getNodeValue());
            if (v == value) {
                return i;
            }
        }

        return -1;
    }

    public void close() {
        document = null;
    }
}
