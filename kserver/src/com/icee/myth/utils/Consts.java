/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.utils;

/**
 *
 * @author liuxianke
 */
public class Consts {
//    public static final int ENERGY_INCREASE_PERIOD = 180000;   // 3分钟恢复1点体力
    public static final int FLUSH_PLAYER_DATA_PERIOD = 300000; // 五分钟刷一次玩家数据到数据库
//    public static final int MAX_MAIL_SIZE = 30; // 玩家身上最多补偿数
    public static final int FIRST_HEARTBEAT_NUM = 99999;    //服务启动时，channel心跳初始值
    public static final int NORMAL_HEARTBEAT_NUM = 10;  //正常情况下，channel心跳最大值
    public static final int RECONNECT_PERIOD = 2000;    //断线重连间隔时间，2秒
    public static final int HEARTBEAT_PERIOD = 1000;    //心跳间隔时间，1秒
    public static final int FIRST_STAGE_ID = 20397;     // 第一个关卡号
    public static final int ALLOW_TALK_LEVEL = 10;      // 允许世界频道聊天的等级

    public static final int THREAD_SERVER_THREAD_NUM = 8;  // 线程池线程数

    public static final int MILSECOND_1MINITE = 1*60*1000;  // 1分钟的毫秒数
    public static final int MILSECOND_1SECOND = 1000;  // 1秒的毫秒数
    public static final int MILSECOND_5MINITE = 5*60*1000;    // 5分钟的毫秒数
    public static final int MILSECOND_10MINITE = 10*60*1000;    // 10分钟的毫秒数
    public static final int MILSECOND_15MINITE = 15*60*1000;    // 15分钟的毫秒数
    public static final int MILSECOND_30MINITE = 30*60*1000;    // 30分钟的毫秒数
    public static final int MILSECOND_45MINITE = 45*60*1000;    // 45分钟的毫秒数
    public static final int MILSECOND_7POINT5MINITE = 75*6*1000;    // 7.5分钟的毫秒数
    public static final int MILSECOND_1HOUR = 1*3600*1000;  // 1小时的毫秒数
    public static final int MILSECOND_3HOUR = 3*3600*1000;  // 3小时的毫秒数（用于防沉迷）
    public static final int MILSECOND_5HOUR = 5*3600*1000;  // 5小时的毫秒数（用于防沉迷）
    public static final int MILSECOND_8HOUR = 8*3600*1000;  // 8小时的毫秒数（用于时差）
    public static final int MILSECOND_12HOUR_45MINUTE = (12*3600+45*60)*1000;   // 12小时45分的毫秒数（用于讨伐活动）
    public static final int MILSECOND_18HOUR_45MINUTE = (18*3600+45*60)*1000;   // 18小时45分的毫秒数（用于讨伐活动）
    public static final int MILSECOND_21HOUR_45MINUTE = (21*3600+45*60)*1000;   // 21小时45分的毫秒数（用于讨伐活动）
    public static final int MILSECOND_20HOUR = (20*3600+0*60)*1000;   // 20小时的毫秒数（用于军团战活动）
    public static final int MILSECOND_ONE_DAY = 24*3600*1000;   // 一天毫秒数
    public static final int MILSECOND_TWO_DAY = 2 * MILSECOND_ONE_DAY;  // 两天的毫秒数
    public static final int JET_LAG = MILSECOND_8HOUR;
    
    public static final int CLEAR_OFFLINE_PLAYER_TIME = MILSECOND_1HOUR;  // 清除离线玩家的时间
    public static final int CLEAR_RELATION_TIME = MILSECOND_1HOUR;  // 清除关系的时间
    public static final int CLEAR_BRIEF_PLAYERINFO_TIME = MILSECOND_1HOUR;  // 清除玩家简略信息的时间

    public static final int LOG_ONLINE_PLAYER_COUNT_TIME = MILSECOND_1MINITE;   // 采样在线人数时间周期

    public static final int MAX_GUIDE_STEP = 64;            // 最大引导步骤
    public static final int MAX_BATTLE_ROUND = 30;          // 最长战斗回合数
    public static final int MAX_FOODCARD_NUM = 8;           // 强化最大食物卡
    public static final int MAX_SANDBOX_SLOT_NUM = 6;       // 阵型最大槽位数
    public static final int HALF_SANDBOX_SLOT_NUM = 3;      // 阵型一半槽位数
    public static final int MAX_HEGEMONY_TARGET_NUM = 9;    // 争霸目标最大数
    public static final int MAX_BASE_TARGET_NUM = 4;        // 据点目标最大数
    public static final int MAX_NOTIFY_CACHE_NUM = 100;     // 离线消息最大存储数量
    public static final int MAX_CAPECOLLINSON_NUM = 2;      // 最大臣属数

    public static final int ROBOT_NUM_PER_RANK = 10;    // 军衔中的机器人数

    public static final int NORMAL_ACTIVITY_REFRESH_PERIOD = MILSECOND_1SECOND;  // 普通活动刷新周期

    public static final int FRIEND_HELP_COOLDOWN_TIME = MILSECOND_1HOUR;  // 好友助战冷却

    public static final int ADD_CONCERN_BY_NAME_COOLDOWN = MILSECOND_1SECOND;   // 按名字搜索添加关注的冷却时间
    
    public static final String HUMANLEVELSCONFIG_FILEPATH = "humanLevelsConfig.json";   // 玩家等级文件
    public static final String CARDLEVELSCONFIG_FILEPATH = "cardLevelsConfig.json";     // 卡片等级文件
    public static final String HUMANRANKSCONFIG_FILEPATH = "humanRanksConfig.json";   // 玩家功勋等级文件
    public static final String SKILLSCONFIG_FILEPATH = "skillsConfig.json"; // 技能模板文件
    public static final String QUESTSCONFIG_FILEPATH = "questsConfig.json"; // 任务模板文件
    public static final String TALENTSCONFIG_FILEPATH = "talentsConfig.json"; // 天赋模板文件
    public static final String STAGESCONFIG_FILEPATH = "stagesConfig.json";     // 关卡配置文件
    public static final String CARDSCONFIG_FILEPATH = "cardsConfig.json";   // 卡片配置文件
    public static final String CARDDRAWSCONFIG_FILEPATH = "cardDrawsConfig.json";   // 抽卡卡包配置文件
    public static final String BATTLESCONFIG_FILEPATH = "battlesConfig.json";   // 战斗模板文件
    public static final String ITEMSCONFIG_FILEPATH = "itemsConfig.json";       // 物品配置文件
    public static final String CONTSIGN_TEMPLATES_FILEPATH = "contSignTemplates.json";    // 签到活跃度
    public static final String VIPREWARD_TEMPLATES_FILEPATH = "vipRewardTemplates.json";    // vip赠礼配置
    public static final String ROBOTSCONFIG_FILEPATH = "robotsConfig.json";     // 机器人配置
    public static final String NAMESCONFIG_FILEPATH = "namesConfig.json";       // 姓名配置
    public static final String CONFIG_FILEPATH = "mapConfig.json";   // map常量相关配置
    public static final String BILL_STORE_TEMPLATES_FILEPATH = "billStoreTemplates.json";

    public static final int GAME_SLEEP_CONST = 50;  // Unit ms, that is 0.05 second
    public static final int LOG_SLEEP_CONST = 100;  // Unit ms, that is 0.05 second

    public static final int QUEST_TYPE_STAGE = 1;   // 任务类型：过关
    public static final int QUEST_TYPE_LEVEL = 2;   // 任务类型：升级
    
    public static final int BATTLE_LEFT_CAMP = 0;   // 战斗左方阵营
    public static final int BATTLE_RIGHT_CAMP = 1;  // 战斗右方阵营

    public static final int STAGE_TYPE_NORMAL = 0;  // 普通副本
    public static final int STAGE_TYPE_BIG = 1;     // 精英副本
    public static final int STAGE_TYPE_ACTIVITY = 2;     // 活动副本

    public static final int LIVENESS_NORMALSTAGE = 1;  // 活跃度类型：普通副本
    public static final int LIVENESS_HEROSTAGE = 2;    // 活跃度类型：英雄副本

    public static final int REWARD_ITEM_TYPE_CARD = 0;   // 卡片
    public static final int REWARD_ITEM_TYPE_ITEM = 1;   // 物品
    public static final int REWARD_ITEM_TYPE_GOLD = 2;   // 黄金
    public static final int REWARD_ITEM_TYPE_SILVER = 3; // 白银

    public static final int ATTACK_TYPE_HIT = 0;  // 攻击结果类型：命中
    public static final int ATTACK_TYPE_CRI = 1;// 攻击结果类型：暴击
    public static final int ATTACK_TYPE_DOD = 2;// 攻击结果类型：躲闪
    public static final int ATTACK_TYPE_PAR = 3;// 攻击结果类型：格挡

    public static final int HURT_TYPE_PHYSICS = 0;  // 伤害类型：物理伤害
    public static final int HURT_TYPE_MAGIC = 1;    // 伤害类型：魔法伤害
    public static final int HURT_TYPE_HEAL = 2;     // 伤害类型：治疗
    
    public static final int SKILL_TARGET_TYPE_SELF = 0;       // 目标类型：自身
    public static final int SKILL_TARGET_TYPE_ACTOR = 1;      // 目标类型：角色

    public static final int SKILL_TARGET_CHOOSETYPE_FRONT_SINGLE_ENEMY = 0;   //前排单体敌人
    public static final int SKILL_TARGET_CHOOSETYPE_BACK_SINGLE_ENEMY = 1;    //后排单体敌人
    public static final int SKILL_TARGET_CHOOSETYPE_MIN_HP_PERCENT_ENEMY = 2; //生命最少敌人
    public static final int SKILL_TARGET_CHOOSETYPE_MIN_HP_PERCENT_FRIEND = 3; //生命最少友军
    public static final int SKILL_TARGET_CHOOSETYPE_RANDOM_ENEMY = 4;         //随机敌军
    public static final int SKILL_TARGET_CHOOSETYPE_RANDOM_FRIEND = 5;        //随机友军

    public static final int SKILL_AREA_TYPE_TARGET = 0;                            // 技能目标区域类型：目标
    public static final int SKILL_AREA_TYPE_SELF = 1;                              // 技能目标区域类型：自己
    public static final int SKILL_AREA_TYPE_TARGET_ROW = 2;                        // 技能目标区域类型：目标一排
    public static final int SKILL_AREA_TYPE_SELF_ROW = 3;                          // 技能目标区域类型：自己一排
    public static final int SKILL_AREA_TYPE_TARGET_COLUMN = 4;                     // 技能目标区域类型：目标一列
    public static final int SKILL_AREA_TYPE_SELF_COLUMN = 5;                       // 技能目标区域类型：自己一列
    public static final int SKILL_AREA_TYPE_TARGET_ALL = 6;                        // 技能目标区域类型：目标全部
    public static final int SKILL_AREA_TYPE_SELF_ALL = 7;                          // 技能目标区域类型：自己全部
    public static final int SKILL_AREA_TYPE_TARGET_CROSS = 8;                      // 技能目标区域类型：目标十字
    public static final int SKILL_AREA_TYPE_SELF_CROSS = 9;                        // 技能目标区域类型：自己十字
    public static final int SKILL_AREA_TYPE_TARGET_RANDOM = 10;                    // 技能目标区域类型：目标随机
    public static final int SKILL_AREA_TYPE_SELF_RANDOM = 11;                      // 技能目标区域类型：自己随机

    public static final int SKILL_TYPE_NORMAL = 0;  // 技能类型：普攻
    public static final int SKILL_TYPE_SPECIAL = 1;      // 技能类型：CP技

    public static final int WORLDCHANNEL = 0;   //聊天世界频道
    public static final int PRIVATECHANNEL= 2 ;   //聊天私聊频道
    public static final int SYSTEMCHANNEL = 4;   //聊天系统频道

    public static final int MAX_TALKMESSAGE_LENGTH = 100;   //聊天最长长度

    public static final int DB_KEEPALIVE_INTERVAL = 3600000; // ms, 1 hour
    public static final String DB_KEEPALIVE_TEST_STATEMENT = "SELECT 1 FROM INFORMATION_SCHEMA.VIEWS WHERE table_name IS NULL";
        
    // 数据库断线消息类型
    public static final int DB_DOWN_CACHE_MESSAGE_TYPE_CHARINFO = 1;        // 角色信息
    public static final int DB_DOWN_CACHE_MESSAGE_TYPE_RELATION = 2;        // 关系信息
    public static final int DB_DOWN_CACHE_MESSAGE_TYPE_BILL = 7;            // 账单
    public static final int DB_DOWN_CACHE_MESSAGE_TYPE_COUPON = 8;          // 礼券
    public static final int DB_DOWN_CACHE_MESSAGE_TYPE_MAIL = 9;            // 补偿
    public static final int DB_DOWN_CACHE_MESSAGE_TYPE_CHAROCCUPYINFO = 10; // 臣属信息
    public static final int DB_DOWN_CACHE_MESSAGE_TYPE_CONSUMRE_LOG = 11;   // 消费记录
    
    // 玩家魂改变日志类型定义
    // 扣魂类型号
    public static final int SOUL_CHANGE_LOG_TYPE_DRAW_CARD = 15;                  // 抽卡
    public static final int SOUL_CHANGE_LOG_TYPE_BAG_BUYSLOT = 20;                // 购买背包槽位
    public static final int SOUL_CHANGE_LOG_TYPE_GM_DECREASE = 21;                // GM指令扣黄魂
    public static final int SOUL_CHANGE_LOG_TYPE_BIGSTAGE_REFRESH = 24;           // 刷新精英副本
    public static final int SOUL_CHANGE_LOG_TYPE_REVIVE = 30;                     // 复活
    public static final int SOUL_CHANGE_LOG_TYPE_STRENGTHEN_CARD = 35;            // 强化卡片
    public static final int SOUL_CHANGE_LOG_TYPE_BUY_ENERGY = 45;                 // 购买体力
    public static final int SOUL_CHANGE_LOG_TYPE_BUY_TOKEN = 46;                  // 购买军令
    public static final int SOUL_CHANGE_LOG_TYPE_REFRESH_HEGEMONY = 47;           // 刷新争霸列表

    // 加魂类型号
    public static final int SOUL_CHANGE_LOG_TYPE_QUEST = 101;                       // 任务
    public static final int SOUL_CHANGE_LOG_TYPE_GUIDE = 102;                       // 新手引导
    public static final int SOUL_CHANGE_LOG_TYPE_GM_INCREASE = 103;                 // GM指令加黄魂
    public static final int SOUL_CHANGE_LOG_TYPE_PICK_BOX = 106;                    // 捡关底宝箱
    public static final int SOUL_CHANGE_LOG_TYPE_GM_INCREASE_YELLOWSOUL1 = 108;     // GM指令加黄魂1
    public static final int SOUL_CHANGE_LOG_TYPE_GETREWARD_UNKNOWN = 109;           // 获取奖励（未知）
    public static final int SOUL_CHANGE_LOG_TYPE_GETREWARD_PVE = 110;               // 获取奖励（普通打怪）
    public static final int SOUL_CHANGE_LOG_TYPE_SOLD_CARD = 113;                   // 卖卡
    public static final int SOUL_CHANGE_LOG_TYPE_CUMULATIVE_SIGN_REWARD = 114;      // 累计签到奖
    public static final int SOUL_CHANGE_LOG_TYPE_CONSECUTIVE_SIGN_REWARD = 115;     // 连续签到奖
    public static final int SOUL_CHANGE_LOG_TYPE_CONTSIGN_LIVENESS_REWARD = 116;    // 签到活跃度
    public static final int SOUL_CHANGE_LOG_TYPE_VIP_REWARD = 120;                  // vip奖励
    public static final int SOUL_CHANGE_LOG_TYPE_HARVEST_SELF_MINE = 125;           // 收获自己银矿
    public static final int SOUL_CHANGE_LOG_TYPE_HARVEST_CAPE_MINE = 126;           // 收获臣属缴税
//    public static final int SOUL_CHANGE_LOG_TYPE_RESISTANCE = 127;                  // 反抗退税
    public static final int SOUL_CHANGE_LOG_TYPE_BILL = 130;                        // 充值
    public static final int SOUL_CHANGE_LOG_TYPE_GM_INCREASE_YELLOWSOUL2 = 131;     // GM指令加黄魂2
    public static final int SOUL_CHANGE_LOG_TYPE_HEGEMONY = 135;                    // 争霸战
    public static final int SOUL_CHANGE_LOG_TYPE_HEGEMONY_PAY = 136;                // 领军饷
    public static final int SOUL_CHANGE_LOG_TYPE_GETREWARD_COUPON = 139;            // 获得奖励（激活码）
    public static final int SOUL_CHANGE_LOG_TYPE_GETREWARD_NORMAL_ACTIVITY = 143;   // 获得奖励（普通活动）
    public static final int SOUL_CHANGE_LOG_TYPE_MAIL = 150;    // 邮件补偿

    // 玩家魂刷新日志子类型定义
    public static final int SOUL_CHANGE_LOG_SUBTYPE_NONE = 0; // 无

    // 玩家行为日志类型定义
//    public static final int BEHAVIOR_LOG_TYPE_INCREASE_BAG_SPACE = 9;// 增加背包空间数量
    public static final int BEHAVIOR_LOG_TYPE_STRENGTHEN_CARD = 15;     // 卡片强化
    public static final int BEHAVIOR_LOG_TYPE_TRANSFORM_CARD = 16;      // 卡片转生
    public static final int BEHAVIOR_LOG_TYPE_SOLD_CARD = 17;           // 卡片出售
    public static final int BEHAVIOR_LOG_TYPE_DRAW_CARD = 18;           // 卡片开包
    public static final int BEHAVIOR_LOG_TYPE_GET_CARD_EXPERIENCE = 19; // 获得卡片经验
    public static final int BEHAVIOR_LOG_TYPE_CARD_LEVELUP = 20;        // 卡片升级
    public static final int BEHAVIOR_LOG_TYPE_REFRESH_HEGEMONY = 24;    // 争霸列表刷新
    public static final int BEHAVIOR_LOG_TYPE_ENTER_STAGE = 28;         // 进入关卡
    public static final int BEHAVIOR_LOG_TYPE_LEAVE_STAGE = 29;         // 离开关卡
    public static final int BEHAVIOR_LOG_TYPE_FINISH_STAGE = 30;        // 完成关卡
    public static final int BEHAVIOR_LOG_TYPE_STAGE_REVIVED = 31;       // 复活关卡
    public static final int BEHAVIOR_LOG_TYPE_PRIVATE_TALK = 36;        // 私聊
    public static final int BEHAVIOR_LOG_TYPE_BROADCAST = 37;           // 世界频道聊天
    public static final int BEHAVIOR_LOG_TYPE_ADD_CONCERN = 38;         // 加好友
    public static final int BEHAVIOR_LOG_TYPE_GET_PLAYER_INFO = 39;     // 查看目标资料
    public static final int BEHAVIOR_LOG_TYPE_LEVELUP = 102;            // 升级
    public static final int BEHAVIOR_LOG_TYPE_RANK_LEVELUP = 103;       // 升军衔
    public static final int BEHAVIOR_LOG_TYPE_REFRESH_BIGSTAGE = 104;   // 刷新精英关卡
    public static final int BEHAVIOR_LOG_TYPE_NO_CHAR_ENTER = 111;      // 无角色进入
    public static final int BEHAVIOR_LOG_TYPE_GET_EXPERIENCE = 114;     // 获得经验
    public static final int BEHAVIOR_LOG_TYPE_GET_RANK_EXPERIENCE = 115;// 获得军衔经验（功勋）
    public static final int BEHAVIOR_LOG_TYPE_QUEST_SUBMIT = 120;       // 提交任务
    public static final int BEHAVIOR_LOG_TYPE_CUMULATIVE_SIGN_REWARD = 124;     // 累计签到奖励
    public static final int BEHAVIOR_LOG_TYPE_CONSECUTIVE_SIGN_REWARD = 125;    // 连续签到奖励
    public static final int BEHAVIOR_LOG_TYPE_LIVENESS_REWARD = 126;    // 活跃度奖励
    public static final int BEHAVIOR_LOG_TYPE_VIPGIFT_REWARD = 127;     // VIP赠礼
    public static final int BEHAVIOR_LOG_TYPE_BUY_ENERGY = 128;         // 购买体力
    public static final int BEHAVIOR_LOG_TYPE_COMBINE_ITEM = 129;       // 合成物品
    public static final int BEHAVIOR_LOG_TYPE_HANDLE_MAIL = 130;        // 处理邮件（领取邮件奖励）
    public static final int BEHAVIOR_LOG_TYPE_NORMAL_ACTIVITY_ITEM_REWARD = 132;    // 领取普通活动奖励
    public static final int BEHAVIOR_LOG_TYPE_BUY_TOKEN = 133;          // 购买军令

    public static final int CONSUMER_LOG_TYPE_CHARGE = 0;               //充值消费记录 - 充值
    public static final int CONSUMER_LOG_TYPE_GM_CHARGE = 1;            //充值消费记录 - GM充值
    public static final int CONSUMER_LOG_TYPE_TASK_CHARGE = 2;          //充值消费记录 - 任务充值
    public static final int CONSUMER_LOG_TYPE_CONSUMER = 3;             //充值消费记录 - 消费
    public static final int CONSUMER_LOG_TYPE_GM_CONSUMER = 4;          //充值消费记录 - GM消费

    public static final int CONSUMER_LOG_TYPE_SYS_GOLD = 0;             //充值消费记录 - 系统黄魂
    public static final int CONSUMER_LOG_TYPE_CHARGE_GOLD = 0;          //充值消费记录 - 充值黄魂

    public static final int BILL_STEP_START =  0;          //生成新订单
    public static final int BILL_STEP_FINISH = 1;          //订单处理完成

    public static final int PLACE_TYPE_NONE = 0;        // 位置类型：无
    public static final int PLACE_TYPE_SANDBOX = 1;     // 位置类型：阵型中
    public static final int PLACE_TYPE_TRAINING = 2;    // 位置类型：训练场中
    public static final int PLACE_TYPE_MINE = 3;        // 位置类型：银矿上
    public static final int PLACE_TYPE_BARRACK = 4;     // 位置类型：兵营中
    public static final int PLACE_TYPE_ORDNANCE = 5;    // 位置类型：军械所中
    public static final int PLACE_TYPE_COUNCIL = 6;     // 位置类型：军机处中

    public static final int CARD_DRAW_TYPE_SILVER_ONE_DRAW = 0;     // 银币一抽
    public static final int CARD_DRAW_TYPE_GOLD_ONE_DRAW = 1;       // 金币一抽
    public static final int CARD_DRAW_TYPE_SILVER_SIX_DRAW = 2;     // 银币六抽
    public static final int CARD_DRAW_TYPE_GOLD_SIX_DRAW = 3;       // 金币六抽
//    public static final int BUY_ENERGY_NUM = 5;

    public static final int NORMAL_ACTIVITY_ITEM_TYPE_STAGE = 0;            // 过关活动（不能被领取）
    public static final int NORMAL_ACTIVITY_ITEM_TYPE_CHARGE = 1;           // 日累计充值
    public static final int NORMAL_ACTIVITY_ITEM_TYPE_CONSUME = 2;          // 日累计消费
    public static final int NORMAL_ACTIVITY_ITEM_TYPE_EXCHANGE = 3;         // 兑换

    public static final int MAIL_STATUS_UNREAD = 0;     // 未读
    public static final int MAIL_STATUS_READED = 1;     // 已读
    public static final int MAIL_STATUS_GOTTEN = 2;     // 已领取

    public static final int HEGEMONY_TARGET_STATUS_NOTFIGHT = 0;    // 争霸对手状态：未战
    public static final int HEGEMONY_TARGET_STATUS_WIN = 1;         // 争霸对手状态：胜利
    public static final int HEGEMONY_TARGET_STATUS_LOST = 2;        // 争霸对手状态：失败

    public static final int SYSTEM_BEHAVIOR_PLAYERID = -1;         // 系统行为
    public static final int ALL_PLAYER_ID = -1;                    // 对服中所有玩家

    public static final String CY_LOGIN_MC_LOG = "loginMC.log";
    public static final String CY_REGISTER_MC_LOG = "registerMC.log";
    public static final String CY_RECHARGE_MC_LOG = "rechargeMC.log";
    public static final String CY_ROLE_MC_LOG = "roleMC.log";
    public static final String CY_ONLINE_MC_LOG = "onlineMC.log";
    public static final String CY_ONLINE_TIME_MC_LOG = "onlineTimeMC.log";
    public static final String CY_BEHAVIOR_MC_LOG = "behaviorMC.log";
    public static final String CY_USER_MC_LOG = "userMC.log";
    public static final String CY_COMMODITY_BUY_MC_LOG = "commodityBuyMC.log";
    public static final String CY_COMMODITY_COST_MC_LOG = "commodityCostMC.log";
    public static final String CY_DIAMOND_MC_LOG = "diamondMC.log";
    public static final String CY_TASK_MC_LOG = "taskMc.log";
    public static final String CY_DOWNLOAD_MC_LOG = "downloadMC.log";
    public static final String CY_CARD_LOG = "card.log";
    public static final String CY_INSTANCE_LOG = "instance.log";
    public static final String CY_EVOLUTION_LOG = "evolution.log";
    public static final String CY_ACTIVATE_LOG = "activate.log";


}
