/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.battle;

/**
 * 战斗Action type
 * @author liuxianke
 */
public class BattleMessageType {
    public static final int STARTSKILL = 1;// 某角色发出技能
    public static final int BEATTACK = 2;  // 某角色受到伤害
    public static final int BECURE = 3;    // 某角色恢复生命值
}
