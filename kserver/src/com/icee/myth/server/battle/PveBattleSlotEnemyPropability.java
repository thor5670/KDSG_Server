/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.battle;

/**
 * 对应battlesConfig.json里的"enemyPropabilitys"
 * @author liuxianke
 */
public class PveBattleSlotEnemyPropability {
    public int id;      // 怪物卡片号
    public int level;   // 怪物等级
    public int propabilityRange; // 出现概率区间上沿
    public int realId;  // 乱入怪物卡片号（0表示无乱入）
}
