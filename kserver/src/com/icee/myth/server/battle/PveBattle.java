/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.battle;

import com.icee.myth.common.messageQueue.ServerMessageQueue;
import com.icee.myth.log.GameLogger;
import com.icee.myth.log.message.FileDebugGameLogMessage;
import com.icee.myth.log.message.builder.GameLogMessageBuilder;
import com.icee.myth.server.message.serverMessage.builder.MapMessageBuilder;
import com.icee.myth.utils.StackTraceUtil;

/**
 *
 * @author liuxianke
 */
public class PveBattle extends Battle {
    private final int playerId;  // 发起战斗的玩家id（用于在结果计算完成后返回给playerId对应的玩家）
    private final int stageId;    // 战斗所属关卡号（用于记录战斗录像发生场景以及返回玩家状态机处理时进行校验）
    private final int battleId;   // 关卡中的战斗号（用于返回玩家状态机处理时进行校验）

    public PveBattle(int playerId, int stageId, int battleId) {
        super(true);
        this.playerId = playerId;
        this.stageId = stageId;
        this.battleId = battleId;
    }

    @Override
    public void run() {
        try {
            calculateBattleResult();

            // 发送战斗结果消息给主逻辑线程
            ServerMessageQueue.queue().offer(MapMessageBuilder.buildPveBattleResultMessage(playerId, stageId, battleId, rewardItemInfos, battleResultBuilder.build()));
        } catch (Exception e) {
            GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(
                    FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_ERROR,
                    StackTraceUtil.getStackTrace(e)));
        }
    }
}
