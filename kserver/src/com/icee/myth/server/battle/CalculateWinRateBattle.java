/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.battle;

import com.icee.myth.common.messageQueue.ServerMessageQueue;
import com.icee.myth.log.GameLogger;
import com.icee.myth.log.message.FileDebugGameLogMessage;
import com.icee.myth.log.message.builder.GameLogMessageBuilder;
import com.icee.myth.server.fighter.Fighter;
import com.icee.myth.server.message.serverMessage.builder.MapMessageBuilder;
import com.icee.myth.utils.StackTraceUtil;

/**
 * CalculateWinRateBattle用于为策划计算战斗胜率
 * @author liuxianke
 */
public class CalculateWinRateBattle extends Battle {

    public CalculateWinRateBattle() {
        super(false);
    }

    private void recover() {
        for (Fighter fighter : fighters) {
            if (fighter != null) {
                fighter.recover();
            }
        }
    }

    @Override
    public void run() {
        try {
            int winNum = 0;     // 胜局数
            int lostNum = 0;    // 败局数
            int drawNum = 0;    // 平局数

            for (int i=0; i<100; i++) {
                int winner = calculateBattleResult();
                switch (winner) {
                    case 0: {
                        winNum++;
                        break;
                    }
                    case 1: {
                        lostNum++;
                        break;
                    }
                    case -1: {
                        drawNum++;
                        break;
                    }
                }
                recover();
            }

            // 发送战斗结果消息给主逻辑线程
            ServerMessageQueue.queue().offer(MapMessageBuilder.buildCalculateWinRateBattleResultMessage(winNum, lostNum, drawNum));
        } catch (Exception e) {
            GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(
                    FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_ERROR,
                    StackTraceUtil.getStackTrace(e)));
        }
    }
}
