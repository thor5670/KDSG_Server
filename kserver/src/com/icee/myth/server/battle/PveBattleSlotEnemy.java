/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.battle;

/**
 * 
 * @author liuxianke
 */
public class PveBattleSlotEnemy {
    public final int slotId;
    public final int cardId;
    public final int realCardId;
    public final int level;
    public final boolean isBoss;

    public PveBattleSlotEnemy(int slotId, int cardId, int realCardId, int level, boolean isBoss) {
        this.slotId = slotId;
        this.cardId = cardId;
        this.realCardId = realCardId;
        this.level = level;

        this.isBoss = isBoss;
    }
}
