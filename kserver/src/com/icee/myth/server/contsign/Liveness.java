/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.contsign;

import com.icee.myth.protobuf.ExternalCommonProtocol.LivenessTargetProto;
import com.icee.myth.protobuf.builder.ClientToMapBuilder;
import com.icee.myth.server.actor.Human;
import com.icee.myth.utils.Consts;

/**
 * 玩家活跃度
 * @author yangyi
 */
public class Liveness {
    public Human human;
    
    public LivenessTarget target ; //活跃度目标
    public int liveness;

    public Liveness(Human human, LivenessTargetProto livenessTarget) {
        this.human = human;
        target = new LivenessTarget();
        
        target.sign = livenessTarget.getSign();
        target.normalStage = livenessTarget.getNormalStage();
        target.heroStage = livenessTarget.getHeroStage();
//        target.dailyQuest = livenessTarget.getDailyQuest();
//        target.heroQuest = livenessTarget.getHeroQuest();
//        target.equipments = livenessTarget.getEquipments();
//        target.huntBoss = livenessTarget.getHuntBoss();
//        target.artifactHunter = livenessTarget.getArtifactHunter();
//        target.train = livenessTarget.getTrain();
//        target.ring = livenessTarget.getRing();
//        target.tower = livenessTarget.getTower();
//        target.relic = livenessTarget.getRelic();
//        target.unionPray = livenessTarget.getUnionPray();

        sign(livenessTarget.getSign());
        normalStage(livenessTarget.getNormalStage(), false);
        heroStage(livenessTarget.getHeroStage(), false);
//        dailyQuest(livenessTarget.getDailyQuest(), false);
//        heroQuest(livenessTarget.getHeroQuest(), false);
//        equipments(livenessTarget.getEquipments(), false);
//        huntBoss(livenessTarget.getHuntBoss(), false);
//        artifactHunter(livenessTarget.getArtifactHunter(), false);
//        train(livenessTarget.getTrain(), false);
//        ring(livenessTarget.getRing(), false);
//        tower(livenessTarget.getTower(), false);
//        relic(livenessTarget.getRelic(), false);
//        unionPray(livenessTarget.getUnionPray(), false);
    }

    public Liveness(Human human) {
        target = new LivenessTarget();
        this.human = human;
        
        target.sign = 0;
        target.normalStage = 0;
        target.heroStage = 0;
//        target.dailyQuest = 0;
//        target.heroQuest = 0;
//        target.equipments = 0;
//        target.huntBoss = 0;
//        target.artifactHunter = 0;
//        target.train = 0;
//        target.ring = 0;
//        target.tower = 0;
//        target.relic = 0;
//        target.unionPray = 0;

        liveness = 0;
        sign(1);
    }

    public void sign(int num) {
        if(num > 0 && target.sign < ContSignTemplates.INSTANCE.livenessTarget.signMax){
            target.sign += num ;
            if(target.sign >= ContSignTemplates.INSTANCE.livenessTarget.signMax){
                target.sign = ContSignTemplates.INSTANCE.livenessTarget.signMax;
                liveness += ContSignTemplates.INSTANCE.livenessTarget.signValue;
            }
        }
    }

//    public void artifactHunter(int num, boolean needSend) {
//        if(num > 0 && target.artifactHunter < ContSignTemplates.INSTANCE.livenessTarget.artifactHunterMax){
//            target.artifactHunter += num ;
//            if(target.artifactHunter >= ContSignTemplates.INSTANCE.livenessTarget.artifactHunterMax){
//                target.artifactHunter = ContSignTemplates.INSTANCE.livenessTarget.artifactHunterMax;
//                liveness += ContSignTemplates.INSTANCE.livenessTarget.artifactHunterValue;
//            }
//            if(needSend){
//                human.onLivenessChange(liveness, Consts.LIVENESS_ARITIFACTHUNTER, target.artifactHunter);
//            }
//        }
//    }

    public void normalStage(int num, boolean needSend) {
        if(num > 0 && target.normalStage < ContSignTemplates.INSTANCE.livenessTarget.normalStageMax){
            target.normalStage += num ;
            if(target.normalStage >= ContSignTemplates.INSTANCE.livenessTarget.normalStageMax){
                target.normalStage = ContSignTemplates.INSTANCE.livenessTarget.normalStageMax;
                liveness += ContSignTemplates.INSTANCE.livenessTarget.normalStageValue;
            }
            if(needSend){
                human.sendMessage(ClientToMapBuilder.buildLivenessChange(liveness, Consts.LIVENESS_NORMALSTAGE, target.normalStage));
            }
        }
    }

    public void heroStage(int num, boolean needSend) {
        if(num > 0 && target.heroStage < ContSignTemplates.INSTANCE.livenessTarget.heroStageMax){
            target.heroStage += num ;
            if(target.heroStage >= ContSignTemplates.INSTANCE.livenessTarget.heroStageMax){
                target.heroStage = ContSignTemplates.INSTANCE.livenessTarget.heroStageMax;
                liveness += ContSignTemplates.INSTANCE.livenessTarget.heroStageValue;
            }
            if(needSend){
                human.sendMessage(ClientToMapBuilder.buildLivenessChange(liveness, Consts.LIVENESS_HEROSTAGE, target.heroStage));
            }
        }
    }

//    public void dailyQuest(int num, boolean needSend) {
//        if(num > 0 && target.dailyQuest < ContSignTemplates.INSTANCE.livenessTarget.dailyQuestMax){
//            target.dailyQuest += num ;
//            if(target.dailyQuest >= ContSignTemplates.INSTANCE.livenessTarget.dailyQuestMax){
//                target.dailyQuest = ContSignTemplates.INSTANCE.livenessTarget.dailyQuestMax;
//                liveness += ContSignTemplates.INSTANCE.livenessTarget.dailyQuestValue;
//            }
//            if(needSend){
//                human.onLivenessChange(liveness, Consts.LIVENESS_DAILYQUEST, target.dailyQuest);
//            }
//        }
//    }

//    public void heroQuest(int num, boolean needSend) {
//        if(num > 0 && target.heroQuest < ContSignTemplates.INSTANCE.livenessTarget.heroQuestMax){
//            target.heroQuest += num ;
//            if(target.heroQuest >= ContSignTemplates.INSTANCE.livenessTarget.heroQuestMax){
//                target.heroQuest = ContSignTemplates.INSTANCE.livenessTarget.heroQuestMax;
//                liveness += ContSignTemplates.INSTANCE.livenessTarget.heroQuestValue;
//            }
//            if(needSend){
//                human.onLivenessChange(liveness, Consts.LIVENESS_HEROQUEST, target.heroQuest);
//            }
//        }
//    }

//    public void equipments(int num, boolean needSend) {
//        if(num > 0 && target.equipments < ContSignTemplates.INSTANCE.livenessTarget.equipmentsMax){
//            target.equipments += num ;
//            if(target.equipments >= ContSignTemplates.INSTANCE.livenessTarget.equipmentsMax){
//                target.equipments = ContSignTemplates.INSTANCE.livenessTarget.equipmentsMax;
//                liveness += ContSignTemplates.INSTANCE.livenessTarget.equipmentsValue;
//            }
//            if(needSend){
//                human.onLivenessChange(liveness, Consts.LIVENESS_EQUIPMENTS, target.equipments);
//            }
//        }
//    }

//    public void huntBoss(int num, boolean needSend) {
//        if(num > 0 && target.huntBoss < ContSignTemplates.INSTANCE.livenessTarget.huntBossMax){
//            target.huntBoss += num ;
//            if(target.huntBoss >= ContSignTemplates.INSTANCE.livenessTarget.huntBossMax){
//                target.huntBoss = ContSignTemplates.INSTANCE.livenessTarget.huntBossMax;
//                liveness += ContSignTemplates.INSTANCE.livenessTarget.huntBossValue;
//            }
//            if(needSend){
//                human.onLivenessChange(liveness, Consts.LIVENESS_HUNTBOSS, target.huntBoss);
//            }
//        }
//    }

//    public void train(int num, boolean needSend) {
//        if(num > 0 && target.train < ContSignTemplates.INSTANCE.livenessTarget.trainMax){
//            target.train += num ;
//            if(target.train >= ContSignTemplates.INSTANCE.livenessTarget.trainMax){
//                target.train = ContSignTemplates.INSTANCE.livenessTarget.trainMax;
//                liveness += ContSignTemplates.INSTANCE.livenessTarget.trainValue;
//            }
//            if(needSend){
//                human.onLivenessChange(liveness, Consts.LIVENESS_TRAIN, target.train);
//            }
//        }
//    }

//    public void ring(int num, boolean needSend) {
//        if(num > 0 && target.ring < ContSignTemplates.INSTANCE.livenessTarget.ringMax){
//            target.ring += num ;
//            if(target.ring >= ContSignTemplates.INSTANCE.livenessTarget.ringMax){
//                target.ring = ContSignTemplates.INSTANCE.livenessTarget.ringMax;
//                liveness += ContSignTemplates.INSTANCE.livenessTarget.ringValue;
//            }
//            if(needSend){
//                human.onLivenessChange(liveness, Consts.LIVENESS_RING, target.ring);
//            }
//        }
//    }

//    public void tower(int num, boolean needSend) {
//        if(num > 0 && target.tower < ContSignTemplates.INSTANCE.livenessTarget.towerMax){
//            target.tower += num ;
//            if(target.tower >= ContSignTemplates.INSTANCE.livenessTarget.towerMax){
//                target.tower = ContSignTemplates.INSTANCE.livenessTarget.towerMax;
//                liveness += ContSignTemplates.INSTANCE.livenessTarget.towerValue;
//            }
//            if(needSend){
//                human.onLivenessChange(liveness, Consts.LIVENESS_TOWER, target.tower);
//            }
//        }
//    }

//    public void relic(int num, boolean needSend) {
//        if(num > 0 && target.relic < ContSignTemplates.INSTANCE.livenessTarget.relicMax){
//            target.relic += num ;
//            if(target.relic >= ContSignTemplates.INSTANCE.livenessTarget.relicMax){
//                target.relic = ContSignTemplates.INSTANCE.livenessTarget.relicMax;
//                liveness += ContSignTemplates.INSTANCE.livenessTarget.relicValue;
//            }
//            if(needSend){
//                human.onLivenessChange(liveness, Consts.LIVENESS_RELIC, target.relic);
//            }
//        }
//    }

//    public void unionPray(int num, boolean needSend) {
//        if(num > 0 && target.unionPray < ContSignTemplates.INSTANCE.livenessTarget.unionPrayMax){
//            target.unionPray += num ;
//            if(target.unionPray >= ContSignTemplates.INSTANCE.livenessTarget.unionPrayMax){
//                target.unionPray = ContSignTemplates.INSTANCE.livenessTarget.unionPrayMax;
//                liveness += ContSignTemplates.INSTANCE.livenessTarget.unionPrayValue;
//            }
//            if(needSend){
//                human.onLivenessChange(liveness, Consts.LIVENESS_UNIONPRAY, target.unionPray);
//            }
//        }
//    }

    
}
