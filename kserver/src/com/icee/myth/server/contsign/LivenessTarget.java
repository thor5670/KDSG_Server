/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.contsign;

import com.icee.myth.protobuf.ExternalCommonProtocol.LivenessTargetProto;

/**
 *
 * @author yangyi
 */
public class LivenessTarget {
    
    public int sign ; //签到
    public int normalStage ;  //普通副本
    public int heroStage ;    //英雄副本
//    public int dailyQuest ;    //日常任务
//    public int heroQuest ;    //历练任务
//    public int equipments ;    //强化装备
//    public int huntBoss ;    //参加讨伐
//    public int artifactHunter ;    //搜索宝具
//    public int train ;    //升级训练
//    public int ring ;    //升级训练
//    public int tower ;    //挑战巴别塔
//    public int relic ;    //参与神战
//    public int unionPray ;    //军团祈祷

    public LivenessTargetProto buildProto(){
        LivenessTargetProto.Builder builder = LivenessTargetProto.newBuilder();
        builder.setSign(sign);
        builder.setNormalStage(normalStage);
        builder.setHeroStage(heroStage);
//        builder.setDailyQuest(dailyQuest);
//        builder.setHeroQuest(heroQuest);
//        builder.setEquipments(equipments);
//        builder.setHuntBoss(huntBoss);
//        builder.setArtifactHunter(artifactHunter);
//        builder.setTrain(train);
//        builder.setRing(ring);
//        builder.setTower(tower);
//        builder.setRelic(relic);
//        builder.setUnionPray(unionPray);
        return builder.build();
    }


}
