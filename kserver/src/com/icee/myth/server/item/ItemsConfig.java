/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.item;

import com.icee.myth.log.GameLogger;
import com.icee.myth.log.message.FileDebugGameLogMessage;
import com.icee.myth.log.message.builder.GameLogMessageBuilder;
import java.util.HashMap;

/**
 *
 * @author liuxianke
 */
public class ItemsConfig {
    public final HashMap<Integer, ItemStaticInfo> itemStaticInfos = new HashMap<Integer, ItemStaticInfo>();

    public static final ItemsConfig INSTANCE = new ItemsConfig();

    private ItemsConfig() {
    }

    public void addItemStaticInfo(ItemStaticInfo itemStaticInfo) {
        assert (!itemStaticInfos.containsKey(itemStaticInfo.id));
        itemStaticInfos.put(itemStaticInfo.id, itemStaticInfo);
    }

    public ItemStaticInfo getItemStaticInfo(int id) {
        ItemStaticInfo itemStaticInfo = itemStaticInfos.get(id);
        if (itemStaticInfo == null) {
            GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(
                    FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_ERROR,
                    "Item["+id+"] static info not found."));
        }
        return itemStaticInfo;
    }
}
