/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.item;

/**
 *
 * @author liuxianke
 */
public class JSONItemsConfig {
    public ItemStaticInfo[] itemStaticInfos;

    public void buildItemsConfig() {
        if ((itemStaticInfos != null) && (itemStaticInfos.length > 0)) {
            for (ItemStaticInfo staticInfo : itemStaticInfos) {
                ItemsConfig.INSTANCE.addItemStaticInfo(staticInfo);
            }
        }
    }
}
