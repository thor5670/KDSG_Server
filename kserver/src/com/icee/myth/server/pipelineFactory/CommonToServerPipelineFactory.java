/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.pipelineFactory;

import com.icee.myth.common.encoder.Type4BytesLengthFieldProtobufEncoder;
import org.jboss.netty.channel.ChannelPipeline;
import org.jboss.netty.channel.ChannelPipelineFactory;
import org.jboss.netty.channel.ChannelUpstreamHandler;
import org.jboss.netty.handler.codec.frame.LengthFieldBasedFrameDecoder;
import org.jboss.netty.handler.execution.ExecutionHandler;
import org.jboss.netty.handler.logging.LoggingHandler;
import org.jboss.netty.logging.InternalLogLevel;

import static org.jboss.netty.channel.Channels.pipeline;

/**
 * ClientToGWPipelineFactory用于GW接收Client的消息和GW向Client发送消息
 * @author liuxianke
 */
public class CommonToServerPipelineFactory implements ChannelPipelineFactory {
    // stateless handlers
    private final ExecutionHandler executionHandler;
    private final ChannelUpstreamHandler targetHandler;
    private final Type4BytesLengthFieldProtobufEncoder type4BytesLenthFieldProtobufEncoder = new Type4BytesLengthFieldProtobufEncoder();

    public CommonToServerPipelineFactory(ExecutionHandler executionHandler,ChannelUpstreamHandler targetHandler) {
        this.executionHandler = executionHandler;
        this.targetHandler = targetHandler;
    }

    @Override
    public ChannelPipeline getPipeline() throws Exception {
        ChannelPipeline p = pipeline();
        p.addLast("logging", new LoggingHandler(InternalLogLevel.INFO));
        p.addLast("frameDecoder", new LengthFieldBasedFrameDecoder(1024, 2, 2, 0, 0));
        p.addLast("customEncoder", type4BytesLenthFieldProtobufEncoder);

        if(executionHandler != null)
            p.addLast("pipelineExecutor", executionHandler);

        p.addLast("handler", targetHandler);
        return p;
    }
}
