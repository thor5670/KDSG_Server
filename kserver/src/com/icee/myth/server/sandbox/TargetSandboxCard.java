/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.sandbox;

import com.icee.myth.protobuf.ExternalCommonProtocol.TargetSandboxSlotProto;

/**
 *
 * @author liuxianke
 */
public class TargetSandboxCard {
    public int cardId;
    public int cardLevel;
    
    public TargetSandboxCard() {
        
    }

    public TargetSandboxCard(int cardId, int cardLevel) {
        this.cardId = cardId;
        this.cardLevel = cardLevel;
    }

    public TargetSandboxSlotProto buildTargetSandboxSlotProto(int slotId) {
        TargetSandboxSlotProto.Builder builder = TargetSandboxSlotProto.newBuilder();
        builder.setSlotId(slotId);
        builder.setCardId(cardId);
        builder.setCardlevel(cardLevel);

        return builder.build();
    }
}
