/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.bill;

import com.icee.myth.server.GameServer;
import com.icee.myth.utils.Consts;
import com.icee.myth.utils.JSONHelper;

/**
 *
 * @author lidonglin
 */
public class BillStoreTemplates {
    public BillStoreTemplate[] billStoreTemplates;

    public static BillStoreTemplates INSTANCE = JSONHelper.parseFileNoException(GameServer.INSTANCE.serverId + "/" + Consts.BILL_STORE_TEMPLATES_FILEPATH, BillStoreTemplates.class);

    private BillStoreTemplates(){};
}
