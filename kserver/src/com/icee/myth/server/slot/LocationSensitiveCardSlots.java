/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.slot;

import com.icee.myth.log.GameLogger;
import com.icee.myth.log.message.FileDebugGameLogMessage;
import com.icee.myth.log.message.builder.GameLogMessageBuilder;
import com.icee.myth.protobuf.ExternalCommonProtocol.VariableValueProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.VariableValuesProto;
import com.icee.myth.server.actor.Human;
import com.icee.myth.server.card.Card;
import com.icee.myth.utils.Consts;
import java.util.List;

/**
 *
 * @author liuxianke
 */
public abstract class LocationSensitiveCardSlots {
    public final Human human;
    public final int type;  // 位置类型
    public final int maxSlotNum;
    public Card[] slots;  // 当对应槽位上有卡片时，槽位值为卡片实例id，否则槽位值为0

    public LocationSensitiveCardSlots(Human human, int type, int maxSlotNum, VariableValuesProto slotsProto) {
        this.human = human;
        this.type = type;
        this.maxSlotNum = maxSlotNum;
        
        slots = new Card[maxSlotNum];

        if (slotsProto != null) {
            List<VariableValueProto> slotList = slotsProto.getValuesList();

            for (VariableValueProto slot : slotList) {
                int slotId = slot.getId();
                int cardId = (int) slot.getValue();
                Card card = human.cards.getCard(cardId);

                if (card != null) {
                    slots[slotId] = card;
                    card.place = type;
                } else {
                    GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(
                            FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_ERROR,
                            "Player[" + human.id + "] place[" + type + "] slot[" + slotId + "] card[" + cardId + "] not found when init place."));
                }
            }
        }
    }

    public VariableValuesProto buildSlotsProto() {
        VariableValuesProto.Builder builder1 = VariableValuesProto.newBuilder();

        for (int i=0; i<maxSlotNum; i++) {
            if (slots[i] != null) {
                VariableValueProto.Builder builder2 = VariableValueProto.newBuilder();
                builder2.setId(i);
                builder2.setValue(slots[i].id);

                builder1.addValues(builder2);
            }
        }

        return builder1.build();
    }

    protected boolean sameSlots(Card[] slot1, Card[] slot2) {
        for (int i=0; i<maxSlotNum; i++) {
            if (slot1[i] != slot2[i]) {
                return false;
            }
        }

        return true;
    }

    protected void change(Card[] newSlots) {
        for (int i=0; i<maxSlotNum; i++) {
            if (slots[i] != null) {
                slots[i].place = Consts.PLACE_TYPE_NONE;
            }
        }
        slots = newSlots;
        for (int i=0; i<maxSlotNum; i++) {
            if (slots[i] != null) {
                slots[i].place = type;
            }
        }
    }

}
