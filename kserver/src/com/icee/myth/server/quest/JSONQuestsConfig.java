/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.quest;

/**
 *
 * @author liuxianke
 */
public class JSONQuestsConfig {
    public QuestStaticInfo[] questStaticInfos;
    
    public void buildQuestsConfig() {
        if ((questStaticInfos != null) && (questStaticInfos.length > 0)) {
            for (QuestStaticInfo staticInfo : questStaticInfos) {
                QuestsConfig.INSTANCE.addQuestStaticInfo(staticInfo);
            }
        }
    }
}
