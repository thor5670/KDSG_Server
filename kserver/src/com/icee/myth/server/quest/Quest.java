/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.quest;

import com.icee.myth.protobuf.ExternalCommonProtocol.QuestProto;
import com.icee.myth.protobuf.builder.ClientToMapBuilder;
import com.icee.myth.server.actor.Human;
import com.icee.myth.utils.Consts;

/**
 * 任务
 * 
 * @author liuxianke
 */
public class Quest {
    public final Human human;
    public final QuestStaticInfo staticInfo;

    public boolean finished;

    public Quest(Human human, QuestStaticInfo staticInfo) {
        this.human = human;
        this.staticInfo = staticInfo;
    }

    public QuestProto buildQuestProto() {
        QuestProto.Builder builder = QuestProto.newBuilder();

        builder.setQuestId(staticInfo.id);
        builder.setFinished(finished);

        return builder.build();
    }

    public void levelup(int level) {
        assert (staticInfo.type == Consts.QUEST_TYPE_LEVEL);
        if (!finished && (level >= staticInfo.value1)) {
            finished = true;

            human.sendMessage(ClientToMapBuilder.buildQuestFinished(staticInfo.id));
        }
    }

    public void finishStage(int stageId, boolean isBigStage, boolean needSend) {
        assert (staticInfo.type == Consts.QUEST_TYPE_STAGE);
        if (!finished && (isBigStage == staticInfo.isBigStage) && (stageId >= staticInfo.value1)) {
            finished = true;

            if (needSend) {
                human.sendMessage(ClientToMapBuilder.buildQuestFinished(staticInfo.id));
            }
        }
    }
}
