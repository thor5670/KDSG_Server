/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server;

import com.icee.myth.common.message.serverMessage.Message;
import com.icee.myth.server.actor.Human;
import com.icee.myth.server.player.MapPlayer;
import java.util.LinkedList;

/**
 *
 * @author liuxianke
 */
public class PlayerContext {
    public long lastVisitTime;
    public MapPlayer player;
    public Human human;
    public LinkedList<Message> waitingMessages;

    public PlayerContext() {
    }

    public void addWaitingMessage(Message message) {
        if (waitingMessages == null) {
            waitingMessages = new LinkedList<Message>();
        }

        waitingMessages.add(message);
    }
}
