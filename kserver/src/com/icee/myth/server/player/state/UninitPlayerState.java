/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icee.myth.server.player.state;

import com.icee.myth.common.player.Player;
import com.icee.myth.common.player.state.PlayerState;
import com.icee.myth.common.message.serverMessage.Message;
import com.icee.myth.common.message.serverMessage.Message.MessageType;
import com.icee.myth.server.GameServer;
import com.icee.myth.server.player.MapPlayer;
import com.icee.myth.log.GameLogger;
import com.icee.myth.log.message.FileDebugGameLogMessage;
import com.icee.myth.log.message.builder.GameLogMessageBuilder;

/**
 *
 * @author liuxianke
 */
public class UninitPlayerState implements PlayerState {

    public final static UninitPlayerState INSTANCE = new UninitPlayerState();

    private UninitPlayerState() {
    }

    @Override
    public boolean handleMessage(Player player, Message message) {
        MapPlayer p = (MapPlayer) player;
        int playerId = p.getId();
        MessageType msgType = message.getType();
        switch (msgType) {
            case MAP_GET_CHAR_DETAIL_INFO_RET: {
                GameServer.INSTANCE.enterGame(p);
                p.state = NormalPlayerState.INSTANCE;

                return true;
            }
            default: {
                // 消息异常（不作处理）,记录日志
                GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(
                        FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_ERROR,
                        "UninitState handle error message type[" + msgType + "]."));

                return false;
            }
        }
    }
}
