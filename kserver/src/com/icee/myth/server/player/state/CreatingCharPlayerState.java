/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icee.myth.server.player.state;

import com.icee.myth.common.player.Player;
import com.icee.myth.common.player.state.PlayerState;
import com.icee.myth.common.message.serverMessage.Message;
import com.icee.myth.common.message.serverMessage.Message.MessageType;
import com.icee.myth.server.GameServer;
import com.icee.myth.server.message.serverMessage.CreateCharRetMessage;
import com.icee.myth.server.player.MapPlayer;
import com.icee.myth.protobuf.builder.ClientToMapBuilder;
import com.icee.myth.log.GameLogger;
import com.icee.myth.log.message.FileDebugGameLogMessage;
import com.icee.myth.log.message.builder.GameLogMessageBuilder;

/**
 *
 * @author liuxianke
 */
public class CreatingCharPlayerState implements PlayerState {

    public final static CreatingCharPlayerState INSTANCE = new CreatingCharPlayerState();

    private CreatingCharPlayerState() {
    }

    @Override
    public boolean handleMessage(Player player, Message message) {
        MapPlayer p = (MapPlayer)player;
        int playerId = p.getId();
        MessageType msgType = message.getType();
        switch (msgType) {
            case MAP_CREATE_CHAR_RET: {
                CreateCharRetMessage createCharRetMsg = (CreateCharRetMessage)message;

                if (createCharRetMsg.result == 1) {
                    GameServer.INSTANCE.loadPlayerData(playerId);
                    
                    p.state = UninitPlayerState.INSTANCE;
                } else {
                    // 通知玩家无法创建角色（重名）
                    p.channelContext.write(ClientToMapBuilder.buildCreateCharError());
                    
                    p.state = Login2PlayerState.INSTANCE;
                }

                return true;
            }
            default: {
                // 不作处理，记录日志
                GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(
                        FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_ERROR,
                        "Player["+playerId+"] CreatingCharState handle error message type["+msgType+"]."));

                return false;
            }
        }
    }
}
