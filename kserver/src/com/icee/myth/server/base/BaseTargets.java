/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.base;

import com.icee.myth.config.MapConfig;
import com.icee.myth.protobuf.ExternalCommonProtocol.BaseTargetsProto;
import com.icee.myth.protobuf.builder.ClientToMapBuilder;
import com.icee.myth.server.GameServer;
import com.icee.myth.server.actor.Human;
import com.icee.myth.server.base.occupy.OccupyInfo;
import com.icee.myth.server.base.occupy.OccupyInfos;

/**
 * 据点目标
 * @author liuxianke
 */
public class BaseTargets {
    public final Human human;
    public BaseTarget[] targets;

    public long targetsCreateTime;      // 目标列表产生的时间（过期时需要重新生成）
    public OccupyInfo fightingOccupyInfo;   // 为true表示正在计算战斗过程

    public BaseTargets(Human human) {
        this.human = human;
    }

    public BaseTargetsProto buildBaseTargetsProto() {
        BaseTargetsProto.Builder builder = BaseTargetsProto.newBuilder();

        if (targets != null) {
            for (BaseTarget baseTarget : targets) {
                builder.addTargets(baseTarget.buildBaseTargetProto());
            }
        }

        builder.setTargetsCreateTime(targetsCreateTime);

        return builder.build();
    }

    public boolean isTargetCorrect(int targetIndex, int targetId) {
        return ((targets != null) && (targetIndex >= 0) && (targetIndex < targets.length) && (targets[targetIndex].id == targetId));
    }

    public void getInfo() {
        if ((targets == null) || (targetsCreateTime < GameServer.INSTANCE.getCurrentTime() - MapConfig.INSTANCE.baseTargetsRefreshCooldown)) {
            targets = OccupyInfos.INSTANCE.getTargets(human);

            targetsCreateTime = GameServer.INSTANCE.getCurrentTime();
        }

        // 将targets信息发给客户端
        human.sendMessage(ClientToMapBuilder.buildBaseTargets(buildBaseTargetsProto()));
    }
}
