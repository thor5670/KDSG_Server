/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.base;

import com.icee.myth.protobuf.ExternalCommonProtocol.BaseTargetProto;
import com.icee.myth.server.actor.Human;

/**
 *
 * @author liuxianke
 */
public class BaseTarget {
    public final String name; // 目标玩家姓名
    public final int id;      // 目标玩家id（为0时为机器人，否则为玩家id）
    public final int level;   // 目标玩家等级
    public final int rank;    // 目标玩家军衔
    public final int leaderCardId;  // 目标玩家队长卡片号
    public final int leaderCardLevel;  // 目标玩家队长卡片等级
    public final boolean hasKing;   // 目标玩家是否有君主

    public BaseTarget(String name, int id, int level, int rank, int leaderCardId, int leaderCardLevel, boolean hasKing) {
        this.name = name;
        this.id = id;
        this.level = level;
        this.rank = rank;
        this.leaderCardId = leaderCardId;
        this.leaderCardLevel = leaderCardLevel;

        this.hasKing = hasKing;
    }

    public BaseTarget(Human human, boolean hasKing) {
        // 用城墙中阵型来产生TargetSandbox
        this(human.name, human.id, human.lv, human.rankLv, human.sandbox.slots[human.sandbox.leader].staticInfo.id, human.sandbox.slots[human.sandbox.leader].level, hasKing);
    }

    public BaseTargetProto buildBaseTargetProto() {
        BaseTargetProto.Builder builder = BaseTargetProto.newBuilder();
        builder.setName(name);
        builder.setId(id);
        builder.setLevel(level);
        builder.setRank(rank);
        builder.setHasKing(hasKing);

        builder.setLeaderCardId(leaderCardId);
        builder.setLeaderCardLevel(leaderCardLevel);

        return builder.build();
    }
}
