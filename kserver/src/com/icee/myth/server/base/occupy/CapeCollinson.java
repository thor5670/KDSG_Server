/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.base.occupy;

import com.icee.myth.config.MapConfig;
import com.icee.myth.protobuf.InternalCommonProtocol.DBCapeCollinsonProto;
import com.icee.myth.server.GameServer;

/**
 *
 * @author liuxianke
 */
public class CapeCollinson {
    public final int id;            // 被臣服的玩家id
    public final int production;    // 产量
    public long startTime;          // 被臣服时间
    public long freeTime;           // 获得自由的时间（0表示未自由）

    public CapeCollinson(int id, int production) {
        this.id = id;
        this.production = production;
        startTime = GameServer.INSTANCE.getCurrentTime();
    }

    public CapeCollinson(DBCapeCollinsonProto capeCollinsonProto) {
        id = capeCollinsonProto.getId();
        production = capeCollinsonProto.getProduction();
        startTime = capeCollinsonProto.getStartTime();
        freeTime = capeCollinsonProto.getFreeTime();
    }

    public DBCapeCollinsonProto buildDBCapeCollinsonProto() {
        DBCapeCollinsonProto.Builder builder = DBCapeCollinsonProto.newBuilder();

        builder.setId(id);
        builder.setProduction(production);
        builder.setStartTime(startTime);
        if (freeTime != 0) {
            builder.setFreeTime(freeTime);
        }

        return builder.build();
    }

    public boolean isFree() {
        return (freeTime != 0) || (GameServer.INSTANCE.getCurrentTime() - startTime >= MapConfig.INSTANCE.maxOccupyTime);
    }
}
