/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.gm;

import com.icee.myth.common.messageQueue.ServerMessageQueue;
import com.icee.myth.log.GameLogger;
import com.icee.myth.log.message.FileDebugGameLogMessage;
import com.icee.myth.log.message.builder.GameLogMessageBuilder;
import com.icee.myth.server.card.cardDraw.CardDrawItem;
import com.icee.myth.server.card.cardDraw.CardDrawStaticInfo;
import com.icee.myth.server.card.cardDraw.CardDrawsConfig;
import com.icee.myth.server.message.serverMessage.builder.MapMessageBuilder;
import java.util.TreeMap;

/**
 *
 * @author liuxianke
 */
public class GMCalculateCardDrawRate implements Runnable {

    public int cardDrawId;
    public int drawTimes;

    public GMCalculateCardDrawRate(int cardDrawId, int drawTimes) {
        this.cardDrawId = cardDrawId;
        this.drawTimes = drawTimes;
    }

    public void run() {
        TreeMap<Integer, Integer> result = new TreeMap<Integer, Integer>();
        CardDrawStaticInfo cardDrawStaticInfo = CardDrawsConfig.INSTANCE.getCardDrawStaticInfo(cardDrawId);
        if (cardDrawStaticInfo != null) {
            for (int i=0; i<drawTimes; i++) {
                CardDrawItem cardDrawItem = cardDrawStaticInfo.draw();
                Integer value = result.get(cardDrawItem.cardId);
                if (value == null) {
                    result.put(cardDrawItem.cardId, 1);
                } else {
                    result.put(cardDrawItem.cardId, value+1);
                }
            }

            // 发送计算结果消息给主逻辑线程
            ServerMessageQueue.queue().offer(MapMessageBuilder.buildCalculateCardDrawRateResultMessage(result));
        } else {
            GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(
                    FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_ERROR,
                    "Calculate card draw rate error because card draw[" + cardDrawId + "] not found."));
        }
    }
}
