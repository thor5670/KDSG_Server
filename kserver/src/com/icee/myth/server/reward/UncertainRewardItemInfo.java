/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.reward;

/**
 * 不确定奖励项：按一定概率掉落处于最小和最大数量间的数量的指定物品
 * @author liuxianke
 */
public class UncertainRewardItemInfo {
    public RewardItemInfo itemInfo;  // 掉落信息
    public int propability;          // 掉落概率(上限)
}
