/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.dbMessage;

import com.icee.myth.common.message.dbMessage.SimpleDBMessage;

/**
 *
 * @author liuxianke
 */
public class GetNewMailDBMessage extends SimpleDBMessage {
    public final int playerId;
    public final long lastGlobalMaxMailId;

    public GetNewMailDBMessage(int playerId, long lastGlobalMaxMailId) {
        super(DBMessageType.GET_NEW_MAIL);
        this.playerId = playerId;
        this.lastGlobalMaxMailId = lastGlobalMaxMailId;
    }
}
