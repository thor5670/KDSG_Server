/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.dbMessage;

import com.icee.myth.common.message.dbMessage.SimpleDBMessage;
import com.icee.myth.protobuf.ExternalCommonProtocol.RewardProto;

/**
 *
 * @author liuxianke
 */
public class SaveMailDBMessage extends SimpleDBMessage{
    public final long mailId;
    public final int targetId;
    public final String title;
    public final String description;
    public final RewardProto rewardInfo;

    public SaveMailDBMessage(long mailId, int targetId, String title, String description, RewardProto rewardInfo) {
        super(DBMessageType.SAVE_MAIL);
        this.mailId = mailId;
        this.targetId = targetId;
        this.title = title;
        this.description = description;
        this.rewardInfo = rewardInfo;
    }
}
