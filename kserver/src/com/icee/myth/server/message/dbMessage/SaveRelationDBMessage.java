/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.dbMessage;

import com.icee.myth.common.message.dbMessage.SimpleDBMessage;
import com.icee.myth.protobuf.InternalCommonProtocol.DBRelationProto;

/**
 *
 * @author liuxianke
 */
public class SaveRelationDBMessage extends SimpleDBMessage {
    public final int playerId;
    public final DBRelationProto relationProto;

    public SaveRelationDBMessage(int playerId, DBRelationProto relationProto) {
        super(DBMessageType.SAVE_RELATION);
        this.playerId = playerId;
        this.relationProto = relationProto;
    }
}
