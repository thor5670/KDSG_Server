/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.dbMessage;

import com.icee.myth.common.message.dbMessage.SimpleDBMessage;

/**
 *
 * @author yangyi
 */
public class GetCouponDBMessage extends SimpleDBMessage{
    public final int playerId;
    public final String passport;
    public final String code;

    public GetCouponDBMessage(int playerId, String passport, String code) {
        super(DBMessageType.GET_COUPON);
        this.playerId = playerId;
        this.passport = passport;
        this.code = code;
    }
}
