/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.dbMessage;

import com.icee.myth.common.message.dbMessage.SimpleDBMessage;
import com.icee.myth.server.bill.Order;

/**
 *
 * @author yangyi
 */
public class SaveBillDBMessage extends SimpleDBMessage{
    public final int playerId;
    public final String passport;
    public final Order order;
    public final int step;
    public final int status;
    
    public SaveBillDBMessage(int playerId, String passport, Order order,int status, int step) {
        super(DBMessageType.SAVE_BILL);
        this.playerId = playerId;
        this.passport = passport;
        this.order = order;
        this.step = step;
        this.status = status;
    }
}
