/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.dbMessage;

import com.icee.myth.common.message.dbMessage.SimpleDBMessage;
import com.icee.myth.server.message.serverMessage.gmMessage.DeleteNormalActivityMessage;

/**
 *
 * @author yangyi
 */
public class DeleteNormalActivityDBMessage extends SimpleDBMessage{
    public final DeleteNormalActivityMessage deleteNormalActivityMessage;

    public DeleteNormalActivityDBMessage(DeleteNormalActivityMessage deleteNormalActivityMessage) {
        super(DBMessageType.DELETE_NORMALACTIVITY);
        this.deleteNormalActivityMessage = deleteNormalActivityMessage;
    }

}
