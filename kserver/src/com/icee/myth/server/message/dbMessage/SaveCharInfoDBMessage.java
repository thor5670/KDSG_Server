/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.dbMessage;

import com.icee.myth.common.charInfo.CharDetailInfo;
import com.icee.myth.common.message.dbMessage.SimpleDBMessage;

/**
 *
 * @author liuxianke
 */
public class SaveCharInfoDBMessage extends SimpleDBMessage {
    public final int playerId;
    public final CharDetailInfo charDetailInfo;

    public SaveCharInfoDBMessage(int playerId, CharDetailInfo charDetailInfo) {
        super(DBMessageType.SAVE_CHAR_INFO);
        this.playerId = playerId;
        this.charDetailInfo = charDetailInfo;
    }
}
