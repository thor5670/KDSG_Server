/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.dbMessage;

import com.icee.myth.common.message.dbMessage.SimpleDBMessage;

/**
 *
 * @author liuxianke
 */
public class GetBattleResultDBMessage extends SimpleDBMessage {
    public final int playerId;
    public final long battleId;

    public GetBattleResultDBMessage(int playerId, long battleId) {
        super(DBMessageType.GET_BATTLE_RESULT);
        this.playerId = playerId;
        this.battleId = battleId;
    }
}
