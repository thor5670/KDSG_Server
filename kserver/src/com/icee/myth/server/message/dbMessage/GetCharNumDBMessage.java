/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.dbMessage;

import com.icee.myth.common.message.dbMessage.SimpleDBMessage;

/**
 *
 * @author liuxianke
 */
public class GetCharNumDBMessage extends SimpleDBMessage {
    public final int playerId;

    public GetCharNumDBMessage(int playerId) {
        super(DBMessageType.GET_CHAR_NUM);
        this.playerId = playerId;
    }
}
