/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.dbMessage;

import com.icee.myth.common.message.dbMessage.SimpleDBMessage;

/**
 *
 * @author liuxianke
 */
public class GetRelationDBMessage extends SimpleDBMessage {
    public final int playerId;

    public GetRelationDBMessage(int playerId) {
        super(DBMessageType.GET_RELATION);
        this.playerId = playerId;
    }
}
