/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.serverMessage;

import com.icee.myth.common.message.serverMessage.InternalPlayerMessage;

/**
 *
 * @author liuxianke
 */
public class CreateCharRetMessage extends InternalPlayerMessage {
    public final int result;

    public CreateCharRetMessage(int playerId, int result) {
        super(MessageType.MAP_CREATE_CHAR_RET, playerId);

        this.result = result;
    }
}
