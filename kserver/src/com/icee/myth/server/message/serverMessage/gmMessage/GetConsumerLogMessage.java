/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icee.myth.server.message.serverMessage.gmMessage;

import com.icee.myth.protobuf.RpcServiceProtocol.GMConsumerLogProto;

/**
 *
 * @author lidonglin
 */
public class GetConsumerLogMessage extends GMMessage {

    public final GMConsumerLogProto consumerLog;
    public String response;

    public GetConsumerLogMessage(GMConsumerLogProto consumerLog) {
        super(GMMessageType.GM_GET_CONSUMER_LOG);
        this.consumerLog = consumerLog;
    }
}
