/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.serverMessage;

import com.icee.myth.common.message.serverMessage.InternalPlayerMessage;
import com.icee.myth.server.mail.Mail;
import java.util.LinkedList;

/**
 *
 * @author liuxianke
 */
public class GetNewMailRetMessage extends InternalPlayerMessage {
    public final LinkedList<Mail> mails;

    public GetNewMailRetMessage(int playerId, LinkedList<Mail> mails) {
        super(MessageType.MAP_GET_NEW_MAIL_RET, playerId);

        this.mails = mails;
    }
}
