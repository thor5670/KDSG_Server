/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.serverMessage;

import com.icee.myth.common.message.serverMessage.InternalPlayerMessage;

/**
 *
 * @author liuxianke
 */
public class BuyTokenMessage extends InternalPlayerMessage {
    public final int num;

    public BuyTokenMessage(int playerId, int num) {
        super(MessageType.MAP_BUY_TOKEN, playerId);

        this.num = num;
    }
}
