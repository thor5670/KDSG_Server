/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.serverMessage;

import com.icee.myth.common.message.serverMessage.InternalPlayerMessage;
import com.icee.myth.server.social.BriefPlayerInfo;
import java.util.LinkedList;

/**
 *
 * @author liuxianke
 */
public class GetBriefPlayerInfosRetMessage extends InternalPlayerMessage {
    public final LinkedList<BriefPlayerInfo> briefPlayerInfos;

    public GetBriefPlayerInfosRetMessage(int playerId, LinkedList<BriefPlayerInfo> briefPlayerInfos) {
        super(MessageType.MAP_GET_BRIEF_PLAYER_INFOS_RET, playerId);

        this.briefPlayerInfos = briefPlayerInfos;
    }

}
