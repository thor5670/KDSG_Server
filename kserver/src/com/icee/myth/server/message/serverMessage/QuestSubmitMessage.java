/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.serverMessage;

import com.icee.myth.common.message.serverMessage.InternalPlayerMessage;

/**
 *
 * @author liuxianke
 */
public class QuestSubmitMessage extends InternalPlayerMessage {
    public final int questId;

    public QuestSubmitMessage(int playerId, int questId) {
        super(MessageType.MAP_QUEST_SUBMIT, playerId);
        this.questId = questId;
    }
}
