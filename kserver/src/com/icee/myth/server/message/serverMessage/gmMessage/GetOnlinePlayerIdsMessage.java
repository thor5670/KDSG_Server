/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.serverMessage.gmMessage;

import com.icee.myth.protobuf.ExternalCommonProtocol.IntValuesProto;

/**
 *
 * @author liuxianke
 */
public class GetOnlinePlayerIdsMessage extends GMMessage {
    public IntValuesProto.Builder response = IntValuesProto.newBuilder();

    public GetOnlinePlayerIdsMessage() {
        super(GMMessageType.GM_GET_ONLINE_PLAYER_IDS);
    }

    public void addPlayerId(int playerId) {
        response.addValues(playerId);
    }
}
