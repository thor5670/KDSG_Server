/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.serverMessage;

import com.icee.myth.common.message.serverMessage.InternalPlayerMessage;

/**
 *
 * @author liuxianke
 */
public class BaseResistanceMessage extends InternalPlayerMessage{

    public final boolean stimulate;

    public BaseResistanceMessage(int playerId, boolean stimulate) {
        super(MessageType.MAP_BASE_RESISTANCE, playerId);
        this.stimulate = stimulate;
    }
}
