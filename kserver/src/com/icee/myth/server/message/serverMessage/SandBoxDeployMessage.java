/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.serverMessage;

import com.icee.myth.common.message.serverMessage.InternalPlayerMessage;
import com.icee.myth.protobuf.ExternalCommonProtocol.SandboxProto;

/**
 *
 * @author yangyi
 */
public class SandBoxDeployMessage extends InternalPlayerMessage{

    public final SandboxProto sandBoxProto;

    public SandBoxDeployMessage(int playerId, SandboxProto sandBoxProto) {
        super(MessageType.MAP_SANDBOX_DEPLOY, playerId);
        this.sandBoxProto = sandBoxProto;
    }
}
