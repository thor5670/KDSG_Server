/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.serverMessage;

import com.icee.myth.common.message.serverMessage.InternalPlayerMessage;

/**
 * GM协议 - 跳过新手引导
 * @author lidonglin
 */
public class GMIgnoreGuideStepMessage extends InternalPlayerMessage{
    
    public GMIgnoreGuideStepMessage(int playerId) {
        super(MessageType.MAP_GM_IGNORE_GUIDE_STEP, playerId);
    }
}
