/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.serverMessage;

import com.icee.myth.common.message.serverMessage.InternalPlayerMessage;

/**
 *
 * @author yangyi
 */
public class ContSignReceiveLivenessRewardMessage extends InternalPlayerMessage{
    public final int index;

    public ContSignReceiveLivenessRewardMessage(int playerId, int index) {
        super(MessageType.MAP_CONTSIGN_RECEIVE_LIVENESS_REWARD, playerId);

        this.index = index;
    }

}
