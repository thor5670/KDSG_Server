/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.serverMessage;

import com.icee.myth.common.message.serverMessage.SimpleMessage;

/**
 *
 * @author liuxianke
 */
public class CalculateWinRateBattleResultMessage extends SimpleMessage {
    public final int winNum;
    public final int lostNum;
    public final int drawNum;

    public CalculateWinRateBattleResultMessage(int winNum, int lostNum, int drawNum) {
        super(MessageType.ALL_CALCULATE_WIN_RATE_RESULT);
        this.winNum = winNum;
        this.lostNum = lostNum;
        this.drawNum = drawNum;
    }
}
