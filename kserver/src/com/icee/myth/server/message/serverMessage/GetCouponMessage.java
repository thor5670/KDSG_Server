/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.serverMessage;

import com.icee.myth.common.message.serverMessage.InternalPlayerMessage;

/**
 *
 * @author yangyi
 */
public class GetCouponMessage extends InternalPlayerMessage{
    public final String couponCode;
    
    public GetCouponMessage(int playerId, String couponCode) {
        super(MessageType.MAP_GET_COUPON, playerId);
        this.couponCode = couponCode;
    }

}
