/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.serverMessage;

import com.icee.myth.common.message.serverMessage.InternalPlayerMessage;

/**
 *
 * @author yangyi
 */
public class SocialAddConcernMessage extends InternalPlayerMessage{
    public final int concernId;

    public SocialAddConcernMessage(int playerId, int concernId) {
        super(MessageType.MAP_SOCIAL_ADD_CONCERN, playerId);
        this.concernId = concernId;
    }
}
