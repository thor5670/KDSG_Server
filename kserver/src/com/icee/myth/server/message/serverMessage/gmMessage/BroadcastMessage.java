/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.serverMessage.gmMessage;

/**
 *
 * @author liuxianke
 */
public class BroadcastMessage extends GMMessage{
    public final String message;
    
    public BroadcastMessage(String message) {
        super(GMMessageType.GM_BROADCAST);
        this.message = message;
    }
}
