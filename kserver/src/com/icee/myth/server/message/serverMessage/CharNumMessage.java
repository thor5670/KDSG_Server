/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.serverMessage;

import com.icee.myth.common.message.serverMessage.InternalPlayerMessage;

/**
 *
 * @author liuxianke
 */
public class CharNumMessage extends InternalPlayerMessage {
    public final int charNum;

    public CharNumMessage(int playerId, int charNum) {
        super(MessageType.MAP_CHAR_NUM, playerId);

        this.charNum = charNum;
    }
}
