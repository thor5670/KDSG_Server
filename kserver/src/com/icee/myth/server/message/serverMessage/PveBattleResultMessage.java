/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.serverMessage;

import com.icee.myth.common.message.serverMessage.InternalPlayerMessage;
import com.icee.myth.protobuf.ExternalCommonProtocol.BattleResultProto;
import com.icee.myth.server.reward.RewardItemInfo;
import java.util.LinkedList;

/**
 *
 * @author liuxianke
 */
public class PveBattleResultMessage extends InternalPlayerMessage {
    public final int stageId;
    public final int battleId;
    public final LinkedList<RewardItemInfo> rewardItemInfos;
    public final BattleResultProto battleResult;

    public PveBattleResultMessage(int playerId, int stageId, int battleId, LinkedList<RewardItemInfo> rewardItemInfos, BattleResultProto battleResult) {
        super(MessageType.MAP_PVE_BATTLE_RESULT, playerId);

        this.stageId = stageId;
        this.battleId = battleId;
        this.rewardItemInfos = rewardItemInfos;
        this.battleResult = battleResult;
    }
}
