/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.serverMessage;

import com.icee.myth.common.message.serverMessage.InternalPlayerMessage;

/**
 *
 * @author liuxianke
 */
public class HegemonyOneTokenFightMessage extends InternalPlayerMessage {
    public final int targetSlot;

    public HegemonyOneTokenFightMessage(int playerId, int targetSlot) {
        super(MessageType.MAP_HEGEMONY_ONE_TOKEN_FIGHT, playerId);

        this.targetSlot = targetSlot;
    }
}
