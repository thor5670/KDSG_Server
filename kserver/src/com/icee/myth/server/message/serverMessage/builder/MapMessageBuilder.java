/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icee.myth.server.message.serverMessage.builder;

import java.util.LinkedList;
import java.util.List;
import java.util.TreeMap;

import org.jboss.netty.channel.Channel;

import com.icee.myth.common.charInfo.CharDetailInfo;
import com.icee.myth.common.charInfo.CharOccupyInfo;
import com.icee.myth.common.message.serverMessage.InternalPlayerMessage;
import com.icee.myth.common.message.serverMessage.Message;
import com.icee.myth.common.message.serverMessage.Message.MessageType;
import com.icee.myth.common.message.serverMessage.builder.MessageBuilder;
import com.icee.myth.protobuf.ExternalCommonProtocol.BattleResultProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.C2STalkProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.CreateCharProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.IntValuesProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.SandboxProto;
import com.icee.myth.protobuf.InternalCommonProtocol.DBRelationProto;
import com.icee.myth.server.bill.Order;
import com.icee.myth.server.mail.Mail;
import com.icee.myth.server.message.serverMessage.BaseAttackBattleResultMessage;
import com.icee.myth.server.message.serverMessage.BaseAttackMessage;
import com.icee.myth.server.message.serverMessage.BaseBarrackDeployMessage;
import com.icee.myth.server.message.serverMessage.BaseCouncilDeployMessage;
import com.icee.myth.server.message.serverMessage.BaseHarvestCapeMineMessage;
import com.icee.myth.server.message.serverMessage.BaseMineDeployMessage;
import com.icee.myth.server.message.serverMessage.BaseMineProductionChangeMessage;
import com.icee.myth.server.message.serverMessage.BaseOrdnanceDeployMessage;
import com.icee.myth.server.message.serverMessage.BaseResistanceBattleResultMessage;
import com.icee.myth.server.message.serverMessage.BaseResistanceMessage;
import com.icee.myth.server.message.serverMessage.BaseTrainingDeployMessage;
import com.icee.myth.server.message.serverMessage.BillResultMessage;
import com.icee.myth.server.message.serverMessage.BuyTokenMessage;
import com.icee.myth.server.message.serverMessage.CalculateCardDrawRateResultMessage;
import com.icee.myth.server.message.serverMessage.CalculateWinRateBattleResultMessage;
import com.icee.myth.server.message.serverMessage.CardDrawMessage;
import com.icee.myth.server.message.serverMessage.CardSoldMessage;
import com.icee.myth.server.message.serverMessage.CardStrengthenMessage;
import com.icee.myth.server.message.serverMessage.CardTransformMessage;
import com.icee.myth.server.message.serverMessage.CharNumMessage;
import com.icee.myth.server.message.serverMessage.ContSignReceiveConsecutiveSignRewardMessage;
import com.icee.myth.server.message.serverMessage.ContSignReceiveLivenessRewardMessage;
import com.icee.myth.server.message.serverMessage.CouponResultMessage;
import com.icee.myth.server.message.serverMessage.CreateCharMessage;
import com.icee.myth.server.message.serverMessage.CreateCharRetMessage;
import com.icee.myth.server.message.serverMessage.EnterActivityStageMessage;
import com.icee.myth.server.message.serverMessage.EnterStageMessage;
import com.icee.myth.server.message.serverMessage.FightingOccupyInfoMessage;
import com.icee.myth.server.message.serverMessage.GMAddEnergyMessage;
import com.icee.myth.server.message.serverMessage.GMAddGoldenMessage;
import com.icee.myth.server.message.serverMessage.GMIgnoreGuideStepMessage;
import com.icee.myth.server.message.serverMessage.GetBriefPlayerInfosRetMessage;
import com.icee.myth.server.message.serverMessage.GetCharDetailRetMessage;
import com.icee.myth.server.message.serverMessage.GetCharOccupyInfoRetMessage;
import com.icee.myth.server.message.serverMessage.GetCouponMessage;
import com.icee.myth.server.message.serverMessage.GetMailInfoMessage;
import com.icee.myth.server.message.serverMessage.GetMailRewardMessage;
import com.icee.myth.server.message.serverMessage.GetNewMailRetMessage;
import com.icee.myth.server.message.serverMessage.GetNormalActivityItemListMessage;
import com.icee.myth.server.message.serverMessage.GetNormalActivityRewardMessage;
import com.icee.myth.server.message.serverMessage.GetRelationRetMessage;
import com.icee.myth.server.message.serverMessage.GuideNextMessage;
import com.icee.myth.server.message.serverMessage.HegemonyBattleResultMessage;
import com.icee.myth.server.message.serverMessage.HegemonyOneTokenFightMessage;
import com.icee.myth.server.message.serverMessage.HegemonyRefreshMessage;
import com.icee.myth.server.message.serverMessage.HegemonyThreeTokenFightMessage;
import com.icee.myth.server.message.serverMessage.ItemCombineMessage;
import com.icee.myth.server.message.serverMessage.LoginMessage;
import com.icee.myth.server.message.serverMessage.LookOtherPlayerInfoMessage;
import com.icee.myth.server.message.serverMessage.PveBattleResultMessage;
import com.icee.myth.server.message.serverMessage.QuestSubmitMessage;
import com.icee.myth.server.message.serverMessage.RemoveMailMessage;
import com.icee.myth.server.message.serverMessage.SandBoxDeployMessage;
import com.icee.myth.server.message.serverMessage.SocialAddConcernMessage;
import com.icee.myth.server.message.serverMessage.SocialConcernNoteMessage;
import com.icee.myth.server.message.serverMessage.SocialRemoveConcernMessage;
import com.icee.myth.server.message.serverMessage.TalkMessage;
import com.icee.myth.server.message.serverMessage.UninitOccupyInfoMessage;
import com.icee.myth.server.message.serverMessage.VipGiftReceiveMessage;
import com.icee.myth.server.reward.CertainRewardInfo;
import com.icee.myth.server.reward.RewardItemInfo;
import com.icee.myth.server.social.BriefPlayerInfo;

/**
 *
 * @author liuxianke
 */
public class MapMessageBuilder extends MessageBuilder {

    public static Message buildLoginMessage(Channel channel, int playerId, String passport, boolean auth, int privilege,int endForbidTalkTime) {
        return new LoginMessage(channel, playerId, passport, auth, privilege, endForbidTalkTime);
    }

    public static Message buildCharNumMessage(int playerId, int charNum) {
        return new CharNumMessage(playerId, charNum);
    }

    public static Message buildCreateCharMessage(int playerId, CreateCharProto charInfo) {
        return new CreateCharMessage(playerId, charInfo);
    }

    public static Message buildCreateCharRetMessage(int playerId, int result) {
        return new CreateCharRetMessage(playerId, result);
    }

    public static Message buildGetCharDetailRetMessage(int playerId, CharDetailInfo charDetailInfo) {
        return new GetCharDetailRetMessage(playerId, charDetailInfo);
    }

    public static Message buildGetCharOccupyInfoRetMessage(int playerId, CharOccupyInfo charOccupyInfo) {
        return new GetCharOccupyInfoRetMessage(playerId, charOccupyInfo);
    }

    public static Message buildGuideNextMessage(int playerId, int curGuideStep) {
        return new GuideNextMessage(playerId, curGuideStep);
    }

    public static Message buildCalculateWinRateBattleResultMessage(int winNum, int lostNum, int drawNum) {
        return new CalculateWinRateBattleResultMessage(winNum, lostNum, drawNum);
    }

    public static Message buildPveBattleResultMessage(int playerId, int stageId, int battleId, LinkedList<RewardItemInfo> rewardItemInfos, BattleResultProto battleResult) {
        return new PveBattleResultMessage(playerId, stageId, battleId, rewardItemInfos, battleResult);
    }
    
    public static Message buildStartPveBattleMessage(int playerId) {
        return new InternalPlayerMessage(MessageType.MAP_START_PVE_BATTLE, playerId);
    }

    public static Message buildStageContinueMessage(int playerId) {
        return new InternalPlayerMessage(MessageType.MAP_STAGE_CONTINUE, playerId);
    }

    public static Message buildLeavePveBattleMessage(int playerId) {
        return new InternalPlayerMessage(MessageType.MAP_LEAVEPVEBATTLE, playerId);
    }

    public static Message buildLeaveStageMessage(int playerId) {
        return new InternalPlayerMessage(MessageType.MAP_STAGE_LEAVE, playerId);
    }

    public static Message buildEnterStageMessage(int playerId, int fubenId, boolean isBigStage, int helper) {
        return new EnterStageMessage(playerId, fubenId, isBigStage, helper);
    }

    public static Message buildStageReviveMessage(int playerId) {
        return new InternalPlayerMessage(MessageType.MAP_STAGE_REVIVE, playerId);
    }

    public static Message buildQuestSubmitMessage(int playerId, int questId) {
        return new QuestSubmitMessage(playerId, questId);
    }

    public static Message buildTalkMessage(int playerId, C2STalkProto talkProto) {
        return new TalkMessage(playerId, talkProto);
    }

    public static Message buildContSignReceiveCumulativeSignRewardMessage(int playerId) {
        return new InternalPlayerMessage(MessageType.MAP_CONTSIGN_RECEIVE_CUMULATIVE_SIGN_REWARD, playerId);
    }

    public static Message buildContSignReceiveConsecutiveSignRewardMessage(int playerId, int day) {
        return new ContSignReceiveConsecutiveSignRewardMessage(playerId, day);
    }

    public static Message buildContSignReceiveLivenessRewardMessage(int playerId, int index) {
        return new ContSignReceiveLivenessRewardMessage(playerId, index);
    }

    public static Message buildVipGiftReceiveMessage(int playerId, int vip) {
        return new VipGiftReceiveMessage(playerId, vip);
    }

    public static Message buildGetRelationRetMessage(int playerId, DBRelationProto relationProto, boolean foundPlayer) {
        return new GetRelationRetMessage(playerId, relationProto, foundPlayer);
    }

    public static Message buildGetBriefPlayerInfosRetMessage(int playerId, LinkedList<BriefPlayerInfo> briefPlayerInfos) {
        return new GetBriefPlayerInfosRetMessage(playerId, briefPlayerInfos);
    }

    public static Message buildSocialAddConcernMessage(int playerId, int concernId) {
        return new SocialAddConcernMessage(playerId, concernId);
    }

    public static Message buildSocialRemoveConcernMessage(int playerId, int concernId) {
        return new SocialRemoveConcernMessage(playerId, concernId);
    }

    public static Message buildLookOtherPlayerInfoMessage(int playerId, int otherPlayerId) {
        return new LookOtherPlayerInfoMessage(playerId, otherPlayerId);
    }

    public static Message buildSocialConcernNoteMessage(int playerId, int targetPlayerId) {
        return new SocialConcernNoteMessage(playerId, targetPlayerId);
    }

    public static Message buildBuyEnergyMessage(int playerId) {
        return new InternalPlayerMessage(MessageType.MAP_BUY_ENERGY, playerId);
    }

    public static Message buildBuyTokenMessage(int playerId, int num) {
        return new BuyTokenMessage(playerId, num);
    }

    public static Message buildItemCombineMessage(int playerId, int itemId) {
        return new ItemCombineMessage(playerId, itemId);
    }

    public static Message buildBillResultMessage(int playerId, String passport, Order order, int status) {
        return new BillResultMessage(playerId, passport, order, status);
    }

    public static Message buildGetBillMessage(int playerId) {
        return new InternalPlayerMessage(MessageType.MAP_GET_BILL, playerId);
    }

    public static Message buildGetCouponMessage(int playerId, String couponCode) {
        return new GetCouponMessage(playerId, couponCode);
    }

    public static Message buildCouponResultMessage(int playerId, String code, CertainRewardInfo reward) {
        return new CouponResultMessage(playerId, code, reward);
    }

    public static Message buildNormalActivityGetListMessage(int playerId) {
        return new InternalPlayerMessage(MessageType.MAP_NORMAL_ACTIVITY_GET_LIST, playerId);
    }

    public static Message buildNormalActivityGetItemListMessage(int playerId, int activityId) {
        return new GetNormalActivityItemListMessage(playerId, activityId);
    }

    public static Message buildEnterActivityStageMessage(int playerId, int activityId, int itemId, int helperId) {
        return new EnterActivityStageMessage(playerId, activityId, itemId, helperId);
    }

    public static Message buildGetNormalActivityRewardMessage(int playerId, int activityId, int itemId) {
        return new GetNormalActivityRewardMessage(playerId, activityId, itemId);
    }

    public static Message buildCardDrawActivityGetListMessage(int playerId) {
        return new InternalPlayerMessage(MessageType.MAP_CARD_DRAW_ACTIVITY_GET_LIST, playerId);
    }

    public static Message buildStageActivityGetListMessage(int playerId) {
        return new InternalPlayerMessage(MessageType.MAP_STAGE_ACTIVITY_GET_LIST, playerId);
    }

    public static Message buildSandBoxDeployMessage(int playerId, SandboxProto sandBoxProto) {
        return new SandBoxDeployMessage(playerId, sandBoxProto);
    }

    public static Message buildCardStrengthenMessage(int playerId, int cardInstId, List<Integer> foodCardInstIds) {
        return new CardStrengthenMessage(playerId, cardInstId, foodCardInstIds);
    }

    public static Message buildCardTransformMessage(int playerId, int cardInstId, List<Integer> foodCardInstIds) {
        return new CardTransformMessage(playerId, cardInstId, foodCardInstIds);
    }

    public static Message buildCardSoldMessage(int playerId, IntValuesProto intValuesProto) {
        return new CardSoldMessage(playerId, intValuesProto);
    }

    public static Message buildCardDrawMessage(int playerId, int drawId, int drawType) {
        return new CardDrawMessage(playerId, drawId, drawType);
    }

    public static Message buildCalculateCardDrawRateResultMessage(TreeMap<Integer, Integer> result) {
        return new CalculateCardDrawRateResultMessage(result);
    }

    public static Message buildGetNewMailRetMessage(int playerId, LinkedList<Mail> mails) {
        return new GetNewMailRetMessage(playerId, mails);
    }

    public static Message buildGetMailListMessage(int playerId) {
        return new InternalPlayerMessage(MessageType.MAP_MAIL_GET_LIST, playerId);
    }
    
    public static Message buildGetMailInfoMessage(int playerId, long mailId) {
        return new GetMailInfoMessage(playerId, mailId);
    }

    public static Message buildGetMailRewardMessage(int playerId, long mailId) {
        return new GetMailRewardMessage(playerId, mailId);
    }

    public static Message buildRemoveMailMessage(int playerId, long mailId) {
        return new RemoveMailMessage(playerId, mailId);
    }

    public static Message buildHegemonyOneTokenFightMessage(int playerId, int targetSlot) {
        return new HegemonyOneTokenFightMessage(playerId, targetSlot);
    }

    public static Message buildHegemonyThreeTokenFightMessage(int playerId, int targetSlot) {
        return new HegemonyThreeTokenFightMessage(playerId, targetSlot);
    }

    public static Message buildHegemonyBattleResultMessage(int playerId, int targetSlot, int targetPower, boolean stimulate, BattleResultProto battleResult) {
        return new HegemonyBattleResultMessage(playerId, targetSlot, targetPower, stimulate, battleResult);
    }

    public static Message buildHegemonyRefreshMessage(int playerId, int count) {
        return new HegemonyRefreshMessage(playerId, count);
    }

    public static Message buildHegemonyGetPayMessage(int playerId) {
        return new InternalPlayerMessage(MessageType.MAP_HEGEMONY_GET_PAY, playerId);
    }

    public static Message buildBarrackDeployMessage(int playerId, IntValuesProto intValuesProto) {
        return new BaseBarrackDeployMessage(playerId, intValuesProto);
    }

    public static Message buildOrdnanceDeployMessage(int playerId, IntValuesProto intValuesProto) {
        return new BaseOrdnanceDeployMessage(playerId, intValuesProto);
    }

    public static Message buildCouncilDeployMessage(int playerId, IntValuesProto intValuesProto) {
        return new BaseCouncilDeployMessage(playerId, intValuesProto);
    }

    public static Message buildBaseTrainingDeployMessage(int playerId, IntValuesProto intValuesProto) {
        return new BaseTrainingDeployMessage(playerId, intValuesProto);
    }

    public static Message buildMineDeployMessage(int playerId, IntValuesProto intValuesProto) {
        return new BaseMineDeployMessage(playerId, intValuesProto);
    }

    public static Message buildBaseGetMineIncomeInfoMessage(int playerId) {
        return new InternalPlayerMessage(MessageType.MAP_BASE_GET_MINE_INCOME_INFO, playerId);
    }

    public static Message buildBaseHarvestSelfMineIncomeMessage(int playerId) {
        return new InternalPlayerMessage(MessageType.MAP_BASE_HARVEST_SELF_MINE, playerId);
    }

    public static Message buildBaseHarvestCapeMineIncomeMessage(int playerId, int capeId) {
        return new BaseHarvestCapeMineMessage(playerId, capeId);
    }

    public static Message buildBaseGetOccupyInfoMessage(int playerId) {
        return new InternalPlayerMessage(MessageType.MAP_BASE_GET_OCCUPY_INFO, playerId);
    }

    public static Message buildBaseGetTargetsMessage(int playerId) {
        return new InternalPlayerMessage(MessageType.MAP_BASE_GET_TARGETS, playerId);
    }

    public static Message buildBaseAttackMessage(int playerId, int targetIndex, int targetId) {
        return new BaseAttackMessage(playerId, targetIndex, targetId);
    }

    public static Message buildBaseAttackBattleResultMessage(int playerId, int targetIndex, int targetId, BattleResultProto battleResultProto) {
        return new BaseAttackBattleResultMessage(playerId, targetIndex, targetId, battleResultProto);
    }

    public static Message buildBaseResistanceMessage(int playerId, boolean stimulate) {
        return new BaseResistanceMessage(playerId,stimulate);
    }

    public static Message buildGMAddEnergyMessage(int playerId) {
        return new GMAddEnergyMessage(playerId);
    }
    
    public static Message buildGMIgnoreGuideStepMessage(int playerId) {
        return new GMIgnoreGuideStepMessage(playerId);
    }

    public static Message buildGMAddGoldenMessage(int playerId, int num) {
        return new GMAddGoldenMessage(playerId, num);
    }

    public static Message buildBaseResistanceBattleResultMessage(int playerId, int king, boolean stimulate, BattleResultProto battleResultProto) {
        return new BaseResistanceBattleResultMessage(playerId, king, stimulate, battleResultProto);
    }

    public static Message buildBaseMineProductionChangeMessage(int playerId, int production) {
        return new BaseMineProductionChangeMessage(playerId, production);
    }

    public static Message buildUninitOccupyInfoMessage(LinkedList<Integer> result) {
        return new UninitOccupyInfoMessage(result);
    }

    public static Message buildFightingOccupyInfoMessage(LinkedList<Integer> result) {
        return new FightingOccupyInfoMessage(result);
    }
}
