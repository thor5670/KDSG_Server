/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.serverMessage;

import com.icee.myth.common.message.serverMessage.InternalPlayerMessage;

/**
 *
 * @author liuxianke
 */
public class GetNormalActivityRewardMessage extends InternalPlayerMessage{
    public final int activityId;
    public final int itemId;

    public GetNormalActivityRewardMessage(int playerId, int activityId, int itemId) {
        super(MessageType.MAP_GET_NORMAL_ACTIVITY_REWARD, playerId);
        this.activityId = activityId;
        this.itemId = itemId;
    }

}
