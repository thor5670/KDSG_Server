/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icee.myth.server.message.serverMessage.gmMessage;

import com.icee.myth.server.rpc.handler.ChangePlayerNameHandler;
import com.google.protobuf.GeneratedMessage;
import com.icee.myth.server.rpc.handler.AddCardRpcHandler;
import com.icee.myth.server.rpc.handler.AddItemRpcHandler;
import com.icee.myth.server.rpc.handler.ChangeSandboxRpcHandler;
import com.icee.myth.server.rpc.handler.EditGold1RpcHandler;
import com.icee.myth.server.rpc.handler.EditGold2RpcHandler;
import com.icee.myth.server.rpc.handler.RemoveCardRpcHandler;
import com.icee.myth.server.rpc.handler.RemoveItemRpcHandler;
import com.icee.myth.server.rpc.handler.RpcModifyPlayerHandler;
import com.icee.myth.server.rpc.handler.SetSilverRpcHandler;
import com.icee.myth.server.rpc.handler.SetEnergyRpcHandler;
import com.icee.myth.server.rpc.handler.SetGold1RpcHandler;
import com.icee.myth.server.rpc.handler.SetGold2RpcHandler;
import com.icee.myth.server.rpc.handler.SetLevelRpcHandler;
import com.icee.myth.server.rpc.handler.SetVipExpRpcHandler;

/**
 *
 * @author yangyi
 */
public class RpcModifyPlayerMessage extends GMMessage {

    public final int playerId;
    public final GeneratedMessage proto;
    public RpcModifyPlayerHandler handler;
    public boolean result;

    public RpcModifyPlayerMessage(GMMessageType gmSubType, int playerId, GeneratedMessage proto) {
        super(gmSubType);
        this.playerId = playerId;
        this.proto = proto;
        switch (gmSubType) {
            case GM_SET_SILVER: {
                handler = SetSilverRpcHandler.INSTANCE;
                break;
            }
            case GM_SET_GOLD1: {
                handler = SetGold1RpcHandler.INSTANCE;
                break;
            }
            case GM_SET_GOLD2: {
                handler = SetGold2RpcHandler.INSTANCE;
                break;
            }
            case GM_EDIT_GOLD1: {
                handler = EditGold1RpcHandler.INSTANCE;
                break;
            }
            case GM_EDIT_GOLD2: {
                handler = EditGold2RpcHandler.INSTANCE;
                break;
            }
            case GM_SET_ENERGY: {
                handler = SetEnergyRpcHandler.INSTANCE;
                break;
            }
            case GM_SET_VIP_EXP: {
                handler = SetVipExpRpcHandler.INSTANCE;
                break;
            }
            case GM_SET_LEVEL: {
                handler = SetLevelRpcHandler.INSTANCE;
                break;
            }
            case GM_ADD_ITEM: {
                handler = AddItemRpcHandler.INSTANCE;
                break;
            }
            case GM_REMOVE_ITEM: {
                handler = RemoveItemRpcHandler.INSTANCE;
                break;
            }
            case GM_ADD_CARD: {
                handler = AddCardRpcHandler.INSTANCE;
                break;
            }
            case GM_REMOVE_CARD: {
                handler = RemoveCardRpcHandler.INSTANCE;
                break;
            }
            case GM_CHANGE_SANDBOX: {
                handler = ChangeSandboxRpcHandler.INSTANCE;
                break;
            }
            case GM_CHANGE_PLAYER_NAME: {
                handler = ChangePlayerNameHandler.INSTANCE;
                break;
            }
        }
    }
}
