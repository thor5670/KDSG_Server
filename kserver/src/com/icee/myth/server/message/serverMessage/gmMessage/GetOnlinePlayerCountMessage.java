/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.serverMessage.gmMessage;

import com.icee.myth.protobuf.ExternalCommonProtocol.IntValueProto;

/**
 *
 * @author liuxianke
 */
public class GetOnlinePlayerCountMessage extends GMMessage {
    public IntValueProto response;

    public GetOnlinePlayerCountMessage() {
        super(GMMessageType.GM_GET_ONLINE_PLAYER_COUNT);
    }

    public void setValue(int count) {
        IntValueProto.Builder builder = IntValueProto.newBuilder();
        builder.setValue(count);

        response = builder.build();
    }
}
