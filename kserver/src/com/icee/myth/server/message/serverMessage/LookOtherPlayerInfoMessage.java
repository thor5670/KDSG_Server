/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icee.myth.server.message.serverMessage;

import com.icee.myth.common.message.serverMessage.InternalPlayerMessage;

/**
 *
 * @author yangyi
 */
public class LookOtherPlayerInfoMessage extends InternalPlayerMessage {

    public final int otherPlayerId;

    public LookOtherPlayerInfoMessage(int playerId, int otherPlayerId) {
        super(MessageType.MAP_SOCIAL_OTHERPLAYERINFO, playerId);
        this.otherPlayerId = otherPlayerId;
    }
}
