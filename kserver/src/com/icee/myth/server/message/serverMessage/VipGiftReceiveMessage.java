/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.serverMessage;

import com.icee.myth.common.message.serverMessage.InternalPlayerMessage;

/**
 *
 * @author yangyi
 */
public class VipGiftReceiveMessage extends InternalPlayerMessage {
    public final int vip;

    public VipGiftReceiveMessage(int playerId,int vip) {
        super(MessageType.MAP_VIPGIFT_RECEIVE, playerId);
        this.vip = vip;
    }

}
