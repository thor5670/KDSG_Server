/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.serverMessage;

import com.icee.myth.common.message.serverMessage.SimpleMessage;
import java.util.LinkedList;

/**
 *
 * @author liuxianke
 */
public class FightingOccupyInfoMessage extends SimpleMessage {
    public final LinkedList<Integer> result;

    public FightingOccupyInfoMessage(LinkedList<Integer> result) {
        super(MessageType.ALL_FIGHTING_OCCUPY_INFO_MESSAGE);
        this.result = result;
    }
}
