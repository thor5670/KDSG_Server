/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.serverMessage;

import com.icee.myth.common.message.serverMessage.InternalPlayerMessage;

/**
 *
 * @author liuxianke
 */
public class GetMailRewardMessage extends InternalPlayerMessage {
    public final long mailId;

    public GetMailRewardMessage(int playerId, long mailId) {
        super(MessageType.MAP_MAIL_GET_REWARD, playerId);

        this.mailId = mailId;
    }
}
