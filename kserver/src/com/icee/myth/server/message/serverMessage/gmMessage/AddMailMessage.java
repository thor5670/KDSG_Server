/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.serverMessage.gmMessage;

import com.icee.myth.protobuf.RpcServiceProtocol.GMMailProto;

/**
 *
 * @author liuxianke
 */
public class AddMailMessage extends GMMessage {
    public final GMMailProto mail;

    public AddMailMessage(GMMailProto mail) {
        super(GMMessageType.GM_ADD_MAIL);
        this.mail = mail;
    }
}
