/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.serverMessage;

import com.icee.myth.common.message.serverMessage.InternalPlayerMessage;
import com.icee.myth.protobuf.ExternalCommonProtocol.C2STalkProto;

/**
 *
 * @author yangyi
 */
public class TalkMessage extends InternalPlayerMessage {
    public final C2STalkProto talkProto;

    public TalkMessage(int playerId, C2STalkProto talkProto) {
        super(MessageType.MAP_TALK, playerId);
        this.talkProto = talkProto;
    }
}