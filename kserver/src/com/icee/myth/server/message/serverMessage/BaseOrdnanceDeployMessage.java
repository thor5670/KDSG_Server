/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.serverMessage;

import com.icee.myth.common.message.serverMessage.InternalPlayerMessage;
import com.icee.myth.protobuf.ExternalCommonProtocol.IntValuesProto;

/**
 *
 * @author liuxianke
 */
public class BaseOrdnanceDeployMessage extends InternalPlayerMessage{

    public final IntValuesProto intValuesProto;

    public BaseOrdnanceDeployMessage(int playerId, IntValuesProto intValuesProto) {
        super(MessageType.MAP_BASE_ORDNANCE_DEPLOY, playerId);
        this.intValuesProto = intValuesProto;
    }
}
