/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.serverMessage;

import com.icee.myth.common.message.serverMessage.InternalPlayerMessage;

/**
 * GM - 增加体力值
 * @author lidonglin
 */
public class GMAddEnergyMessage extends InternalPlayerMessage{
    
    public GMAddEnergyMessage(int playerId) {
        super(MessageType.MAP_GM_ADD_ENERGY, playerId);
    }
}
