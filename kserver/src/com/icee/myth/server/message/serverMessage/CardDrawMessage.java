/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.serverMessage;

import com.icee.myth.common.message.serverMessage.InternalPlayerMessage;

/**
 *
 * @author liuxianke
 */
public class CardDrawMessage extends InternalPlayerMessage {
    public final int drawId;
    public final int drawType;

    public CardDrawMessage(int playerId, int drawId, int drawType) {
        super(MessageType.MAP_CARD_DRAW, playerId);
        this.drawId = drawId;
        this.drawType = drawType;
    }
}
