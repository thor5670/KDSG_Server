/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.serverMessage;

import com.icee.myth.common.message.serverMessage.InternalPlayerMessage;

/**
 *
 * @author liuxianke
 */
public class GuideNextMessage extends InternalPlayerMessage {
    public final int curGuildStep;

    public GuideNextMessage(int playerId, int curGuildStep) {
        super(MessageType.MAP_GUIDE_NEXT, playerId);

        this.curGuildStep = curGuildStep;
    }
}
