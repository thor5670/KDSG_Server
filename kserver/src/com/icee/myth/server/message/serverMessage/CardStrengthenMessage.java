/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.serverMessage;

import com.icee.myth.common.message.serverMessage.InternalPlayerMessage;
import java.util.List;

/**
 *
 * @author liuxianke
 */
public class CardStrengthenMessage extends InternalPlayerMessage {
    public final int cardInstId;
    public final List<Integer> foodCardInstIds;

    public CardStrengthenMessage(int playerId, int cardInstId, List<Integer> foodCardInstIds) {
        super(MessageType.MAP_CARD_STRENGTHEN, playerId);
        this.cardInstId = cardInstId;
        this.foodCardInstIds = foodCardInstIds;
    }
}
