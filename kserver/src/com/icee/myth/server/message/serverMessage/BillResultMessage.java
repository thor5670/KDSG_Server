/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.serverMessage;

import com.icee.myth.common.message.serverMessage.InternalPlayerMessage;
import com.icee.myth.server.bill.Order;

/**
 *
 * @author liuxianke
 */
public class BillResultMessage  extends InternalPlayerMessage{
    public final String passport;
    public final Order order;
    public final int status;
    
    public BillResultMessage(int playerId, String passport, Order order, int status) {
        super(MessageType.MAP_BILL_RESULT, playerId);
        this.passport = passport;
        this.order = order;
        this.status = status;
    }
}
