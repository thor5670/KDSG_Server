/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.serverMessage;

import com.icee.myth.common.message.serverMessage.InternalPlayerMessage;

/**
 *
 * @author lidonglin
 */
public class GMAddGoldenMessage extends InternalPlayerMessage{
    public final int num;
    public GMAddGoldenMessage(int playerId, int num) {
        super(MessageType.MAP_GM_ADD_GOLDEN, playerId);
        this.num = num;
    }

}
