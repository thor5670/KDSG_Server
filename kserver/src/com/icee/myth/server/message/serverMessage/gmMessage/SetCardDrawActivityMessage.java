/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.serverMessage.gmMessage;

/**
 *
 * @author liuxianke
 */
public class SetCardDrawActivityMessage extends GMMessage {
    public final String activityJsonString;
    public boolean response = false;

    public SetCardDrawActivityMessage(String activityJsonString) {
        super(GMMessageType.GM_SET_CARD_DRAW_ACTIVITY);
        this.activityJsonString = activityJsonString;
    }
}
