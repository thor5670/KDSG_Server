/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.social.message;

/**
 *
 * @author liuxianke
 */
public class SocialMessage {
    public enum SocialMessageType {SMT_GET_INFO, SMT_ADD_RELATIONSHIP, SMT_REMOVE_RELATIONSHIP};

    public final SocialMessageType type;

    public SocialMessage(SocialMessageType type) {
        this.type = type;
    }
}
