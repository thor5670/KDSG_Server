/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.social;

import com.icee.myth.config.MapConfig;
import com.icee.myth.log.GameLogger;
import com.icee.myth.log.message.FileDebugGameLogMessage;
import com.icee.myth.log.message.builder.GameLogMessageBuilder;
import com.icee.myth.server.GameServer;
import java.util.LinkedList;
import java.util.NavigableMap;
import java.util.TreeMap;

/**
 *
 * @author liuxianke
 */
public class BriefPlayerInfosOfLevel {
    public final TreeMap<Integer,BriefPlayerInfo> playerId2InfoMap = new TreeMap<Integer,BriefPlayerInfo>();

    // lastSearchPlayerId记录上次查询助战玩家到达的id。每次查询助战列表时，都从lastSearchPlayerId开始向后查询playerId2InfoMap表，若查到表末则循环从表头开始找，
    // 但每次查询最多从lastSearchPlayerId+1到表末，再从表头到lastSearchPlayerId
    public int lastSearchPlayerId;

    public void addBriefPlayerInfo(BriefPlayerInfo briefPlayerInfo) {
        if (!playerId2InfoMap.containsKey(briefPlayerInfo.id)) {
            playerId2InfoMap.put(briefPlayerInfo.id, briefPlayerInfo);
        } else {
            GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(
                    FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_ERROR,
                    "Multi-add brief player info of player[" + briefPlayerInfo.id + "]."));
        }
    }

    public void removeBriefPlayerInfo(BriefPlayerInfo briefPlayerInfo) {
        if (playerId2InfoMap.remove(briefPlayerInfo.id) != briefPlayerInfo) {
            GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(
                    FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_ERROR,
                    "Remove brief player info of player[" + briefPlayerInfo.id + "] error."));
        }
    }
    
    /**
     * 获得该等级下除了exceptId外的指定数量的玩家列表
     * @param playerId 求助者
     * @param friends 求助者的朋友（需要排除的对象）
     * @param infos 结果放置的表
     * @return 数量是否已满
     */
    public boolean getBriefPlayerInfos(int playerId, TreeMap<Integer, Long> friends, LinkedList<BriefPlayerInfo> infos) {
        // 先从lastSearchPlayerId开始向后查询
        NavigableMap<Integer, BriefPlayerInfo> tailMap = playerId2InfoMap.tailMap(lastSearchPlayerId, false);
        for (BriefPlayerInfo briefPlayerInfo : tailMap.values()) {
            if ((briefPlayerInfo.id != playerId) && !friends.containsKey(briefPlayerInfo.id)) {
                infos.add(briefPlayerInfo);
                briefPlayerInfo.lastVisitTime = GameServer.INSTANCE.getCurrentTime();
                if (infos.size() >= MapConfig.INSTANCE.maxStrangeHelperNum) {
                    lastSearchPlayerId = briefPlayerInfo.id;
                    return true;
                }
            }
        }

        // 再从头到lastSearchPlayerId查询
        NavigableMap<Integer, BriefPlayerInfo> headMap = playerId2InfoMap.headMap(lastSearchPlayerId, true);
        for (BriefPlayerInfo briefPlayerInfo : headMap.values()) {
            if ((briefPlayerInfo.id != playerId) && !friends.containsKey(briefPlayerInfo.id)) {
                infos.add(briefPlayerInfo);
                briefPlayerInfo.lastVisitTime = GameServer.INSTANCE.getCurrentTime();
                if (infos.size() >= MapConfig.INSTANCE.maxStrangeHelperNum) {
                    lastSearchPlayerId = briefPlayerInfo.id;
                    return true;
                }
            }
        }

        return false;
    }
}
