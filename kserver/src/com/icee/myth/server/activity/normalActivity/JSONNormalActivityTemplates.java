/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.activity.normalActivity;

import java.util.ArrayList;

/**
 *
 * @author liuxianke
 */
public class JSONNormalActivityTemplates {
    public ArrayList<NormalActivityTemplate> normalActivityTemplates;

    public JSONNormalActivityTemplates(){
        normalActivityTemplates = new ArrayList<NormalActivityTemplate>();
    }

    public void buildNormalActivityTemplates() {
        NormalActivityTemplates.INSTANCE.init(normalActivityTemplates);
    }
}
