/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.activity.normalActivity;

import com.icee.myth.log.GameLogger;
import com.icee.myth.log.message.FileDebugGameLogMessage;
import com.icee.myth.log.message.builder.GameLogMessageBuilder;
import com.icee.myth.protobuf.ExternalCommonProtocol.NormalActivitiesProto;
import com.icee.myth.server.GameServer;
import com.icee.myth.utils.Consts;
import java.util.Iterator;
import java.util.List;
import java.util.TreeMap;

/**
 *
 * @author liuxianke
 */
public class NormalActivityTemplates {
    public final TreeMap<Integer, NormalActivityTemplate> templates = new TreeMap<Integer, NormalActivityTemplate>();

    public static final NormalActivityTemplates INSTANCE = new NormalActivityTemplates();

    public long nextRefreshTime = 0;

    private NormalActivityTemplates() {
    }

    public NormalActivitiesProto buildNormalActivitiesProto() {
        NormalActivitiesProto.Builder builder = NormalActivitiesProto.newBuilder();
        // 注意：过期活动已在主循环中删除
        for (NormalActivityTemplate normalActivityTemplate : templates.values()) {
            NormalActivityStaticInfo normalActivityStaticInfo = normalActivityTemplate.staticInfo;

            builder.addActivities(normalActivityStaticInfo.buildNormalActivityProto());
        }

        return builder.build();
    }

    public NormalActivityTemplate getNormalActivityTemplate(int id) {
        return templates.get(id);
    }

    protected void init(List<NormalActivityTemplate> normalActivityTemplates) {
        long currentTime = System.currentTimeMillis();
        for (NormalActivityTemplate normalActivityTemplate : normalActivityTemplates) {
            // 只有未结束的活动才会进入活动列表
            NormalActivityStaticInfo staticInfo = normalActivityTemplate.staticInfo;
            if (staticInfo.forever) {
                templates.put(staticInfo.id, normalActivityTemplate);
            } else {
                long activityEndTime = (staticInfo.endTime != null)?staticInfo.endTime.getTime():GameServer.INSTANCE.openTime.getTime() + staticInfo.endRelativeTime;

                // 只记录未结束的或未发奖的活动
                if (activityEndTime > currentTime) {
                    templates.put(staticInfo.id, normalActivityTemplate);
                }
            }
        }
    }

    /**
     * 刷新活动列表，删除过期活动
     * @param curTime 当前时间
     */
    public void refresh(long curTime) {
        if (curTime > nextRefreshTime) {
            for (Iterator<NormalActivityTemplate> itor = templates.values().iterator(); itor.hasNext();) {
                NormalActivityTemplate activityTemplate = itor.next();
                NormalActivityStaticInfo staticInfo = activityTemplate.staticInfo;
                if (!staticInfo.forever) {
                    long activityEndTime = (staticInfo.endTime != null)?staticInfo.endTime.getTime():GameServer.INSTANCE.openTime.getTime() + staticInfo.endRelativeTime;
                    if (activityEndTime <= GameServer.INSTANCE.getCurrentTime()) {
                        itor.remove();
                        GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(
                                FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_INFO,
                                "Activity[" + staticInfo.id + "] run out of time."));
                    }
                }
            }

        } else {
            nextRefreshTime = curTime + Consts.NORMAL_ACTIVITY_REFRESH_PERIOD;
        }
    }
}
