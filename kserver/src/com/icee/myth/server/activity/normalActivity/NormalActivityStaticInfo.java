/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icee.myth.server.activity.normalActivity;

import com.icee.myth.log.GameLogger;
import com.icee.myth.log.message.FileDebugGameLogMessage;
import com.icee.myth.log.message.builder.GameLogMessageBuilder;
import com.icee.myth.protobuf.ExternalCommonProtocol.NormalActivityItemProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.NormalActivityItemsProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.NormalActivityProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.VariableValueProto;
import com.icee.myth.server.GameServer;
import com.icee.myth.utils.Consts;
import com.icee.myth.utils.IdValue;
import java.util.Date;

/**
 *
 * @author liuxianke
 */
public class NormalActivityStaticInfo {

    public int id;                  // 活动号（注意：不能与任何曾经做过的活动号重复）
    public int recommend;           // 推荐标识
    public String title;            // 标题
    public String icon;             // 活动图标
    public Date startTime;          // 开始时间
    public Date endTime;            // 结束时间
    public long startRelativeTime;  // 相当开服的开始时间
    public long endRelativeTime;    // 相对开服的结束时间
    public boolean forever;         // 是否永久
    public NormalActivityItem[] items;  // 活动项目表

    public NormalActivityProto buildNormalActivityProto() {
        NormalActivityProto.Builder builder = NormalActivityProto.newBuilder();
        builder.setId(id);
        builder.setRecommend(recommend);

        if (title != null) {
            builder.setTitle(title);
        }

        if (icon != null) {
            builder.setIcon(icon);
        }

        if (startTime != null) {
            builder.setStartTime(startTime.getTime());
        } else {
            builder.setStartTime(GameServer.INSTANCE.openTime.getTime() + startRelativeTime);
        }

        if (!forever) {
            if (endTime != null) {
                builder.setEndTime(endTime.getTime());
            } else {
                builder.setEndTime(GameServer.INSTANCE.openTime.getTime() + endRelativeTime);
            }
        }

        return builder.build();
    }

    public NormalActivityItemsProto buildNormalActivityItemsProto(NormalActivityStatus normalActivityStatus) {
        NormalActivityItemsProto.Builder builder1 = NormalActivityItemsProto.newBuilder();

        builder1.setId(id);
        if (items != null) {
            for (int itemId = 0; itemId < items.length; itemId++) {
                NormalActivityItem normalActivityItem = items[itemId];
                NormalActivityItemProto.Builder builder2 = NormalActivityItemProto.newBuilder();
                builder2.setId(normalActivityItem.id);
                builder2.setType(normalActivityItem.type);
                builder2.setTitle(normalActivityItem.title);
                builder2.setIcon(normalActivityItem.icon);

                if (normalActivityItem.reward != null) {
                    builder2.setReward(normalActivityItem.reward.buildRewardProto());
                }

                NormalActivityItemStatus normalActivityItemStatus = (normalActivityStatus != null)?normalActivityStatus.itemStatuses.get(itemId):null;

                switch (normalActivityItem.type) {
                    case Consts.NORMAL_ACTIVITY_ITEM_TYPE_STAGE: {
                        builder2.setValue1((normalActivityItemStatus != null)?normalActivityItem.value1-normalActivityItemStatus.value:normalActivityItem.value1);
                        builder2.setValue2(normalActivityItem.value2);  // 注意：normalActivityItem.value1是关卡号，normalActivityItem.value2是总次数
                        break;
                    }
                    case Consts.NORMAL_ACTIVITY_ITEM_TYPE_CHARGE:
                    case Consts.NORMAL_ACTIVITY_ITEM_TYPE_CONSUME: {
                        builder2.setValue1(normalActivityItem.value1);
                        builder2.setValue2((normalActivityItemStatus != null)?normalActivityItemStatus.value:0);
                        break;
                    }
                    case Consts.NORMAL_ACTIVITY_ITEM_TYPE_EXCHANGE: {
                        if ((normalActivityItem.exchangeItems != null) && (normalActivityItem.exchangeItems.length > 0)) {
                            for (IdValue exchangeItem : normalActivityItem.exchangeItems) {
                                VariableValueProto.Builder builder3 = VariableValueProto.newBuilder();
                                builder3.setId(exchangeItem.id);
                                builder3.setValue(exchangeItem.value);

                                builder2.addExchangeItems(builder3);
                            }
                        } else {
                            GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(
                                    FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_ERROR,
                                    "Normal activity[" + id + "] item exchange item["+itemId+"] has no exchange items."));
                        }
                        break;
                    }
                }

                builder2.setGotten((normalActivityItemStatus != null) && normalActivityItemStatus.gotten);

                builder1.addItems(builder2);
            }
        }

        return builder1.build();
    }
}
