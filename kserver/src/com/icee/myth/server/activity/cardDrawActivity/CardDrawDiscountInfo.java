/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.activity.cardDrawActivity;

import com.icee.myth.protobuf.ExternalCommonProtocol.CardDrawDiscountProto;
import java.util.Date;

/**
 * 抽卡打折
 * @author liuxianke
 */
public class CardDrawDiscountInfo {
    public int discountId;  // 折扣号
    public Date startTime;  // 开始时间
    public Date endTime;    // 结束时间

    public CardDrawDiscountProto buildCardDrawDiscountProto() {
        CardDrawDiscountProto.Builder builder = CardDrawDiscountProto.newBuilder();
        builder.setDiscountId(discountId);
        builder.setStartTime(startTime.getTime());
        builder.setEndTime(endTime.getTime());

        return builder.build();
    }
}
