/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.activity.cardDrawActivity;

import com.icee.myth.protobuf.ExternalCommonProtocol.CardDrawActivitiesProto;
import java.util.TreeMap;

/**
 *
 * @author liuxianke
 */
public class CardDrawActivityTemplates {
    public final TreeMap<Integer, CardDrawActivityTemplate> cardDrawActivities = new TreeMap<Integer, CardDrawActivityTemplate>();

    public static final CardDrawActivityTemplates INSTANCE = new CardDrawActivityTemplates();

    public CardDrawActivitiesProto cardDrawActivitiesProto = null;

    private CardDrawActivityTemplates() {
    }

    public void clear() {
        cardDrawActivities.clear();
        cardDrawActivitiesProto = null;
    }

    public void addCardDrawActivityTemplate(CardDrawActivityTemplate template) {
        cardDrawActivities.put(template.cardDrawId, template);
    }

    public CardDrawActivityTemplate getCardDrawActivityTemplate(int cardDrawId) {
        return cardDrawActivities.get(cardDrawId);
    }

    public CardDrawActivitiesProto buildCardDrawActivitiesProto() {
        if (cardDrawActivitiesProto == null) {
            CardDrawActivitiesProto.Builder builder = CardDrawActivitiesProto.newBuilder();

            for (CardDrawActivityTemplate template : cardDrawActivities.values()) {
                builder.addCardDrawActivities(template.buildCardDrawActivityProto());
            }

            cardDrawActivitiesProto = builder.build();
        }

        return cardDrawActivitiesProto;
    }
}
