/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.activity.cardDrawActivity;

/**
 *
 * @author liuxianke
 */
public class JSONCardDrawActivityTemplates {
    public CardDrawActivityTemplate[] templates;

    public void buildCardDrawActivityTemplates() {
        CardDrawActivityTemplates.INSTANCE.clear();
        if ((templates != null) && (templates.length > 0)) {
            for (CardDrawActivityTemplate template : templates) {
                CardDrawActivityTemplates.INSTANCE.addCardDrawActivityTemplate(template);
            }
        }
    }
}
