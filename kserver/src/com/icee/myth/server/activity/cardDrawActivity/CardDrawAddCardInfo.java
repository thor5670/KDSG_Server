/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.activity.cardDrawActivity;

import com.icee.myth.protobuf.ExternalCommonProtocol.CardDrawAddCardProto;
import java.util.Date;

/**
 * 抽卡概率提升
 * @author liuxianke
 */
public class CardDrawAddCardInfo {
    public CardDrawAddCardItem[] cardItems; // 添加的卡片列表
    public Date startTime;    // 开始时间
    public Date endTime;    // 结束时间
    
    public CardDrawAddCardProto buildCardDrawAddCardProto() {
        CardDrawAddCardProto.Builder builder = CardDrawAddCardProto.newBuilder();
        
        if (cardItems != null) {
            for (CardDrawAddCardItem cardItem : cardItems) {
                builder.addCardItems(cardItem.buildCardDrawAddCardItemProto());
            }
        }

        builder.setStartTime(startTime.getTime());
        builder.setEndTime(endTime.getTime());

        return builder.build();
    }
}
