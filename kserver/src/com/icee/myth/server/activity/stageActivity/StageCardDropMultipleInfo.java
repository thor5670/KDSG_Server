/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.activity.stageActivity;

import com.icee.myth.protobuf.ExternalCommonProtocol.StageCardDropMultipleProto;
import java.util.Date;

/**
 *
 * @author liuxianke
 */
public class StageCardDropMultipleInfo {
    public int multiple;    // 掉卡倍数
    public Date startTime;  // 开始时间
    public Date endTime;    // 结束时间

    public StageCardDropMultipleProto buildStageCardDropMultipleProto() {
        StageCardDropMultipleProto.Builder builder = StageCardDropMultipleProto.newBuilder();

        builder.setMultiple(multiple);
        builder.setStartTime(startTime.getTime());
        builder.setEndTime(endTime.getTime());

        return builder.build();
    }
}
