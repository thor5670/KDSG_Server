/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.activity.stageActivity;

import com.icee.myth.protobuf.ExternalCommonProtocol.StageActivityProto;

/**
 *
 * @author liuxianke
 */
public class StageActivityTemplate {
    public int stageId;     // 关卡号

    public StageCardDropMultipleInfo cardDropMultipleInfo;  // 掉卡倍数信息
    public StageHalfEnergyInfo halfEnergyInfo;      // 体力减半信息

    public StageActivityProto buildStageActivityProto() {
        StageActivityProto.Builder builder = StageActivityProto.newBuilder();
        builder.setStageId(stageId);

        if (cardDropMultipleInfo != null) {
            builder.setCardDropMultipleInfo(cardDropMultipleInfo.buildStageCardDropMultipleProto());
        }

        if (halfEnergyInfo != null) {
            builder.setHalfEnergyInfo(halfEnergyInfo.buildStageHalfEnergyProto());
        }

        return builder.build();
    }
}
