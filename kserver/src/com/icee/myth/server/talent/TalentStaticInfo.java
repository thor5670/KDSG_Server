/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.talent;

/**
 *
 * @author liuxianke
 */
public class TalentStaticInfo {
    public int id;              // talent ID（注意：该id就是在talentTemplates数组中的下标）
    public int campMask;        // 起效阵营标签
    public long typeMask;       // 起效兵种标签
    public float atkSkill;      // 队长技能攻击加成
    public float dexSkill;      // 队长技能机动加成
    public float hpSkill;       // 队长技能生命加成
    public int hitSkill;        // 队长技能命中加成（千分比）
    public int criSkill;        // 队长技能暴击加成（千分比）
    public int dodSkill;        // 队长技能闪避加成（千分比）
    public int tenSkill;        // 队长技能韧性加成（千分比）
    public float cureUpSkill;   // 队长技能治疗加成
    public float damResPSkill;  // 队长技能物伤减免
    public float damResMSkill;  // 队长技能魔伤减免
}
