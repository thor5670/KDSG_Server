/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.talent;

import com.icee.myth.server.GameServer;
import com.icee.myth.utils.Consts;
import com.icee.myth.utils.JSONHelper;

/**
 *
 * @author liuxianke
 */
public class TalentsConfig {
    public TalentStaticInfo[] talentStaticInfos;

    public static final TalentsConfig INSTANCE = JSONHelper.parseFileNoException(GameServer.INSTANCE.serverId + "/" + Consts.TALENTSCONFIG_FILEPATH, TalentsConfig.class);

    private TalentsConfig() {
    }
}
