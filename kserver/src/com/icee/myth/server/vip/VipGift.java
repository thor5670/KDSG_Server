/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icee.myth.server.vip;

import com.icee.myth.config.MapConfig;
import com.icee.myth.log.GameLogger;
import com.icee.myth.log.message.FileDebugGameLogMessage;
import com.icee.myth.log.message.GameLogMessage.GameLogType;
import com.icee.myth.log.message.builder.GameLogMessageBuilder;
import com.icee.myth.protobuf.builder.ClientToMapBuilder;
import com.icee.myth.server.actor.Human;
import com.icee.myth.utils.Consts;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author yangyi
 */
public class VipGift {

    public final Human human;
    public int receiveFlag;              //是否领取，第0位表示vip1的奖励是否领取

    public VipGift(Human human, int receiveFlag) {
        this.human = human;
        this.receiveFlag = receiveFlag;
    }

    public void receive(int vip) {
        if (vip > 0 && vip <= human.vip.level) {
            if ((receiveFlag & (1 << (vip - 1))) == 0) {
                human.digestCertainReward(VipGiftTemplates.INSTANCE.rewardInfos[vip - 1], Consts.SOUL_CHANGE_LOG_TYPE_VIP_REWARD, vip);
                receiveFlag |= (1 << (vip - 1));
                human.sendMessage(ClientToMapBuilder.buildVipReceived(receiveFlag));

                // 记录行为日志
                if (MapConfig.INSTANCE.useCYLog == true) {
                    List<String> cyLogList = new ArrayList<String>();
                    cyLogList.add("behaviorMC");
                    cyLogList.add(String.valueOf(MapConfig.INSTANCE.gameId));
                    cyLogList.add("KDTY");
                    cyLogList.add(String.valueOf(human.id));
                    cyLogList.add(human.name);
                    cyLogList.add("");
                    cyLogList.add("VipGiftRewardDBBehavior");
                    cyLogList.add(vip + " ");
                    cyLogList.add("");
                    cyLogList.add("");
                    cyLogList.add("");
                    cyLogList.add("");
                    cyLogList.add("");
                    GameLogger.getlogger().log(GameLogMessageBuilder.buildFileCYGameLogMessage(cyLogList, GameLogType.GAMELOGTYPE_CY_BEHAVIOR));
                }
                GameLogger.getlogger().log(GameLogMessageBuilder.buildVipGiftRewardDBBehaviorGameLogMessage(human.id, vip));
            } else {
                GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(
                        FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_ERROR,
                        "Player[" + human.id + "] can't get receive vip gift[" + vip + "] reward because has received."));
            }
        } else {
            GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(
                    FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_ERROR,
                    "Player[" + human.id + "] can't get receive vip gift[" + vip + "] reward because of error vip."));
        }
    }
}
