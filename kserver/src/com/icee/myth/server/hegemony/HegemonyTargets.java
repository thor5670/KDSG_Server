/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.hegemony;

import com.icee.myth.protobuf.ExternalCommonProtocol.HegemonyTargetsProto;
import com.icee.myth.protobuf.InternalCommonProtocol.DBHegemonyTargetProto;
import com.icee.myth.protobuf.InternalCommonProtocol.DBHegemonyTargetsProto;
import com.icee.myth.protobuf.builder.ClientToMapBuilder;
import com.icee.myth.server.actor.Human;
import com.icee.myth.utils.Consts;
import java.util.List;

/**
 * 争霸目标列表
 * @author liuxianke
 */
public class HegemonyTargets {
    public final Human human;
    public HegemonyTarget[] targets;

    public boolean isFighting = false;   // 为true表示正在计算战斗过程

    public HegemonyTargets(Human human, DBHegemonyTargetsProto hegemonyTargetsProto) {
        this.human = human;

        if (hegemonyTargetsProto != null) {
            targets = new HegemonyTarget[Consts.MAX_HEGEMONY_TARGET_NUM];
            List<DBHegemonyTargetProto> hegemonyTargetProtos = hegemonyTargetsProto.getTargetsList();
            assert (hegemonyTargetProtos.size() == Consts.MAX_HEGEMONY_TARGET_NUM);
            int i = 0;
            for (DBHegemonyTargetProto hegemonyTargetProto : hegemonyTargetProtos) {
                targets[i] = new HegemonyTarget(hegemonyTargetProto);
                i++;
            }
        } else {
            targets = Hegemony.INSTANCE.getTargets(human);
        }
    }

    public HegemonyTargetsProto buildHegemonyTargetsProto() {
        HegemonyTargetsProto.Builder builder = HegemonyTargetsProto.newBuilder();

        for (int i=0; i<Consts.MAX_HEGEMONY_TARGET_NUM; i++) {
            builder.addTargets(targets[i].buildHegemonyTargetProto());
        }

        return builder.build();
    }

    public DBHegemonyTargetsProto buildDBHegemonyTargetsProto() {
        DBHegemonyTargetsProto.Builder builder = DBHegemonyTargetsProto.newBuilder();

        for (int i=0; i<Consts.MAX_HEGEMONY_TARGET_NUM; i++) {
            builder.addTargets(targets[i].buildDBHegemonyTargetProto());
        }

        return builder.build();
    }

    public int getWinNum() {
        int winNum = 0;
        for (int i=0; i<Consts.MAX_HEGEMONY_TARGET_NUM; i++) {
            if (targets[i].status == Consts.HEGEMONY_TARGET_STATUS_WIN) {
                winNum++;
            }
        }
        return winNum;
    }

    public boolean isAllWin() {
        for (int i=0; i<Consts.MAX_HEGEMONY_TARGET_NUM; i++) {
            if (targets[i].status != Consts.HEGEMONY_TARGET_STATUS_WIN) {
                return false;
            }
        }
        return true;
    }

    public void refresh() {
        targets = Hegemony.INSTANCE.getTargets(human);

        // 通知玩家争霸列表改变
        human.sendMessage(ClientToMapBuilder.buildHegemonyTargetsChange(buildHegemonyTargetsProto()));
    }
}
