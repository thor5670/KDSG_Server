/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.hegemony;

import com.icee.myth.server.actor.Human;
import com.icee.myth.server.levelup.HumanRanksConfig;
import com.icee.myth.server.index.PlayerIdsOfRank;
import com.icee.myth.server.index.PlayerIdsOfRanks;
import com.icee.myth.utils.Consts;
import com.icee.myth.utils.RandomGenerator;
import java.util.ArrayList;

/**
 * 争霸(PVP)
 * @author liuxianke
 */
public class Hegemony {
    public static final Hegemony INSTANCE = new Hegemony();

    private Hegemony() {
    }

    public HegemonyTarget[] getTargets(Human human) {
        // 为玩家生成争霸目标列表
        ArrayList<HegemonyTarget> targets = new ArrayList<HegemonyTarget>(Consts.MAX_HEGEMONY_TARGET_NUM);
        // 1.根据玩家军衔随机选出5级军衔内目标军衔人数分布
        int range = (human.rankLv<5)?human.rankLv+4:((human.rankLv>HumanRanksConfig.INSTANCE.maxRank-4)?HumanRanksConfig.INSTANCE.maxRank-human.rankLv+4:9);
        int[] nums = new int[range];
        for (int i=0; i<Consts.MAX_HEGEMONY_TARGET_NUM; i++) {
            nums[RandomGenerator.INSTANCE.generator.nextInt(range)]++;
        }

        // 2.在选出的对应军衔内选取所需数量的目标
        int currentTargetNum = 0;  // 已有目标数
        for (int i=0; i<range; i++) {
            if (nums[i] > 0) {
                int currentRank = (human.rankLv<5)?i+1:human.rankLv-4+i;
                getTargetsFromRank(human, currentRank, targets, nums[i]);
                currentTargetNum += nums[i];

                if (currentTargetNum >= Consts.MAX_HEGEMONY_TARGET_NUM) {
                    break;
                }
            }
        }

        HegemonyTarget[] results = new HegemonyTarget[targets.size()];
        results = targets.toArray(results);
        return results;
    }

    /**
     * 从军衔rank中选取needNum数量目标放入currentTargetNum下标开始的resulTargets数组中，目标不能为human本人
     * @param human
     * @param rank
     * @param resultTargets
     * @param currentTargetNum
     * @param needNum
     */
    private void getTargetsFromRank(Human human, int rank, ArrayList<HegemonyTarget> resultTargets, int needNum) {
        // 根据该军衔等级下玩家数量和机器人数的关系随机选择目标
        // 若选到的目标已选择，改为从机器人中选
        int playerNum = 0;
        PlayerIdsOfRank playerIdsOfRank = PlayerIdsOfRanks.INSTANCE.getPlayerIdsOfRank(rank);
        if (playerIdsOfRank != null) {
            playerNum = playerIdsOfRank.count();
            if (human.rankLv == rank) {
                playerNum--;    // 排除玩家本人
            }
        }

        boolean noMorePlayer = playerNum == 0;

        for (int i = 0; i<needNum; i++) {
            if (noMorePlayer || (RandomGenerator.INSTANCE.generator.nextInt(playerNum + Consts.ROBOT_NUM_PER_RANK) < Consts.ROBOT_NUM_PER_RANK)) {
                // 选择机器人
                resultTargets.add(RobotsConfig.INSTANCE.generateHegemonyTarget(rank));
            } else {
                // 选玩家
                if (!playerIdsOfRank.getHegemonyTarget(human, resultTargets)) {
                    noMorePlayer = true;
                    
                    // 选择机器人
                    resultTargets.add(RobotsConfig.INSTANCE.generateHegemonyTarget(rank));
                }
            }
        }
    }
}
