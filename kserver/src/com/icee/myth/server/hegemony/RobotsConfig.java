/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.hegemony;

import com.icee.myth.config.MapConfig;
import com.icee.myth.server.sandbox.TargetSandboxCard;
import com.icee.myth.server.sandbox.TargetSandbox;
import com.icee.myth.server.GameServer;
import com.icee.myth.server.card.CardFactories;
import com.icee.myth.server.card.CardFactory;
import com.icee.myth.server.nameGenerator.NameGenerator;
import com.icee.myth.utils.Consts;
import com.icee.myth.utils.JSONHelper;
import com.icee.myth.utils.RandomGenerator;

/**
 * PVP机器人配置
 * @author liuxianke
 */
public class RobotsConfig {
    public RobotCardsOfRank[] robotCardsOfRanks;    // 军衔对应的机器人卡片表

    public static final RobotsConfig INSTANCE = JSONHelper.parseFileNoException(GameServer.INSTANCE.serverId + "/" + Consts.ROBOTSCONFIG_FILEPATH, RobotsConfig.class);

    private RobotsConfig() {
    }

    /**
     * 产生一个rank军衔的机器人目标
     * @param rank 指定的军衔
     * @return 机器人目标
     */
	public HegemonyTarget generateHegemonyTarget(int rank) {
        int totalLeaderPoint = 0;   // 阵型总领导力，用于计算机器人等级

        // 产生阵型
        TargetSandbox sandbox = new TargetSandbox();

        // 随机产生leader位置
        sandbox.leader = RandomGenerator.INSTANCE.generator.nextInt(Consts.MAX_SANDBOX_SLOT_NUM);
        // 根据leader位置所属前后排生成leader卡片
        // 从rank到rank+deltaRank（10）范围的配置中随机找到所属配置
        int targetCardRank = rank + RandomGenerator.INSTANCE.generator.nextInt(10);
        // 从对应的配置中随机找出所要的卡片
        TargetSandboxCard[] leaderCards = (sandbox.leader < Consts.HALF_SANDBOX_SLOT_NUM)?robotCardsOfRanks[targetCardRank-1].frontLeaderCards:robotCardsOfRanks[targetCardRank-1].backLeaderCards;
        sandbox.slots[sandbox.leader] = leaderCards[RandomGenerator.INSTANCE.generator.nextInt(leaderCards.length)];

        // 产生友军位置（空位）
        int helper = (sandbox.leader + RandomGenerator.INSTANCE.generator.nextInt(Consts.MAX_SANDBOX_SLOT_NUM-1)) % Consts.MAX_SANDBOX_SLOT_NUM;

        // 随机产生其他位置卡片
        // 产生前排卡片
        TargetSandboxCard[] frontCards = robotCardsOfRanks[targetCardRank-1].frontCards;
        for (int i=0; i<Consts.HALF_SANDBOX_SLOT_NUM; i++) {
            if (i != helper) {  // 空出友军位置
                if (sandbox.slots[i] == null) { // 绕开leader
                    sandbox.slots[i] = frontCards[RandomGenerator.INSTANCE.generator.nextInt(frontCards.length)];
                }

                CardFactory cardFactory = CardFactories.INSTANCE.getCardFactory(sandbox.slots[i].cardId);
                totalLeaderPoint += cardFactory.staticInfo.leadPoint;
            }
        }
        // 产生后排卡片
        TargetSandboxCard[] backCards = robotCardsOfRanks[targetCardRank-1].backCards;
        for (int i=Consts.HALF_SANDBOX_SLOT_NUM; i<Consts.MAX_SANDBOX_SLOT_NUM; i++) {
            if (i != helper) {  // 空出友军位置
                if (sandbox.slots[i] == null) { // 绕开leader
                    sandbox.slots[i] = backCards[RandomGenerator.INSTANCE.generator.nextInt(backCards.length)];
                }

                CardFactory cardFactory = CardFactories.INSTANCE.getCardFactory(sandbox.slots[i].cardId);
                totalLeaderPoint += cardFactory.staticInfo.leadPoint;
            }
        }

        // 根据总领导力计算机器人等级(注意：领导力=28+等级/2)（加入5级以内的delta看起来更真）
        int level = (totalLeaderPoint-28)*2;
        if (level >= MapConfig.INSTANCE.openHegemonyLevel) {
            level += RandomGenerator.INSTANCE.generator.nextInt(5);
        } else {
            level = MapConfig.INSTANCE.openHegemonyLevel + RandomGenerator.INSTANCE.generator.nextInt(5);
        }

        return new HegemonyTarget(NameGenerator.INSTANCE.generateName(), 0, level, rank, 0f, 0f, 0, 0, sandbox);
    }
}
