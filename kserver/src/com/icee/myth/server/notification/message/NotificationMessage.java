/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.notification.message;

import com.icee.myth.protobuf.builder.ClientToMapBuilder;
import com.icee.myth.protobuf.ExternalCommonProtocol.NoteProto;
import com.icee.myth.server.actor.Human;

/**
 *
 * @author liuxianke
 */
public abstract class NotificationMessage {
    public final int type;    // 通知消息类型

    public NotificationMessage(int type) {
        this.type = type;
    }

    public abstract NoteProto buildNoteProto();

    public void handle(Human human) {
        human.sendMessage(ClientToMapBuilder.buildNotification(buildNoteProto()));
    }
}
