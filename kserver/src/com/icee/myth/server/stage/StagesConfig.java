/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.stage;

import com.icee.myth.server.GameServer;
import com.icee.myth.utils.Consts;
import com.icee.myth.utils.JSONHelper;

/**
 *
 * @author liuxianke
 */
public class StagesConfig {
    public StageStaticInfo[] normalStageStaticInfos;
    public StageStaticInfo[] bigStageStaticInfos;
    public StageStaticInfo[] activityStageStaticInfos;

    public static final StagesConfig INSTANCE = JSONHelper.parseFileNoException(GameServer.INSTANCE.serverId + "/" + Consts.STAGESCONFIG_FILEPATH, StagesConfig.class);

    private StagesConfig() {
    }
}
