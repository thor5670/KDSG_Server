/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.stage;

/**
 *
 * @author liuxianke
 */
public class StageRewardInfo {
    public int experience;  // 经验获得
    public int minSilver;  // 白银获得
    public int maxSilver;  // 白银获得
    public int goldProbability;   // 黄金获得概率
    public int gold;  // 黄金获得
}
