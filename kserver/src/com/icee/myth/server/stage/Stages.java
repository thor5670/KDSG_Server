/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.stage;

import com.icee.myth.config.MapConfig;
import com.icee.myth.protobuf.ExternalCommonProtocol.StagesProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.VariableValueProto;
import com.icee.myth.protobuf.builder.ClientToMapBuilder;
import com.icee.myth.server.actor.Human;
import com.icee.myth.server.GameServer;
import com.icee.myth.utils.Consts;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.TreeMap;

/**
 *
 * @author liuxianke
 */
public class Stages {
    private final Human human;

    public int currentNormalStageId;    // 当前普通关卡
    public int currentBigStageId;       // 当前精英关卡

    public int lastBigStageDay;         // 最后一次通过精英副本的日期
    public final TreeMap<Integer, Integer> lastBigStages = new TreeMap<Integer, Integer>(); // key为在lastBigStageDay所在的一天里通过的精英副本，value为次数
    
    public Stages(Human human, StagesProto stagesProto) {
        this.human = human;
        
        if (stagesProto != null) {
            int currDay = (int) ((GameServer.INSTANCE.getCurrentTime() + Consts.JET_LAG) / Consts.MILSECOND_ONE_DAY);
        
            currentNormalStageId = stagesProto.getCurrentNormalStageId();
            currentBigStageId = stagesProto.getCurrentBigStageId();

            if (currDay != stagesProto.getLastBigStageDay()) {
                lastBigStageDay = 0;
            } else {
                lastBigStageDay = currDay;
                List<VariableValueProto> lastBigStagesList = stagesProto.getLastBigStagesList();
                if (!lastBigStagesList.isEmpty()) {
                    for (VariableValueProto lastBigStage : lastBigStagesList) {
                        lastBigStages.put(lastBigStage.getId(), (int)lastBigStage.getValue());
                    }
                }
            }
        }
    }
    
    // 创建给客户端的关卡信息
    public StagesProto buildStagesProto() {
        StagesProto.Builder builder1 = StagesProto.newBuilder();

        builder1.setCurrentNormalStageId(currentNormalStageId);
        builder1.setCurrentBigStageId(currentBigStageId);

        int currDay = (int) ((GameServer.INSTANCE.getCurrentTime() + Consts.JET_LAG) / Consts.MILSECOND_ONE_DAY);
        
        if (lastBigStageDay == currDay) {
            builder1.setLastBigStageDay(lastBigStageDay);

            for (Iterator<Entry<Integer, Integer>> iter = lastBigStages.entrySet().iterator(); iter.hasNext(); ) {
                Entry<Integer, Integer> entry = iter.next();
                VariableValueProto.Builder builder2 = VariableValueProto.newBuilder();
                builder2.setId(entry.getKey());
                builder2.setValue(entry.getValue());

                builder1.addLastBigStages(builder2);
            }
        }

        return builder1.build();
    }

    public boolean isStagePassed(int stageId, boolean isBigStage) {
        if (isBigStage) {
            return stageId < currentBigStageId;
        } else {
            return stageId < currentNormalStageId;
        }
    }

    public boolean canEnterBigStage(int bigStageId) {
        int currDay = (int) ((GameServer.INSTANCE.getCurrentTime() + Consts.JET_LAG) / Consts.MILSECOND_ONE_DAY);

        if (lastBigStageDay != currDay) {
            return true;
        } else {
            Integer enterNum = lastBigStages.get(bigStageId);

            return (enterNum == null) || (enterNum < MapConfig.INSTANCE.bigStageEnterNumPerDay);
        }
    }

    public void finishState(int stageId, boolean isBigStage, boolean needSend) {
        if (isBigStage) {
            if (stageId == currentBigStageId) {
                currentBigStageId++;

                if (needSend) {
                    // 通知客户端开启新关卡
                    human.sendMessage(ClientToMapBuilder.buildCurrentBigStageIdChange(currentBigStageId));
                }
            }

            int currDay = (int) ((GameServer.INSTANCE.getCurrentTime() + Consts.JET_LAG) / Consts.MILSECOND_ONE_DAY);
            if (lastBigStageDay != currDay) {
                lastBigStages.clear();
                lastBigStageDay = currDay;
            }

            Integer value = lastBigStages.get(stageId);
            int num = (value == null)?1:value+1;
            lastBigStages.put(stageId, num);

            if (needSend) {
                // 通知客户端精英关卡完成（信息中包含lastBigStageDay和stageId）
                human.sendMessage(ClientToMapBuilder.buildBigStageStatusChange(stageId, num, currDay));
            }
        } else {
            if (stageId == currentNormalStageId) {
                currentNormalStageId++;

                if (needSend) {
                    // 通知客户端开启新关卡
                    human.sendMessage(ClientToMapBuilder.buildCurrentNormalStageIdChange(currentNormalStageId));
                }
            }
        }

        if (isBigStage) {
            human.contSign.liveness.heroStage(1, needSend);
        } else {
            human.contSign.liveness.normalStage(1, needSend);
        }
        human.quests.finishState(stageId, isBigStage, needSend);
    }
}
