/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.skill;

import java.util.HashMap;

/**
 *
 * @author liuxianke
 */
public class Skills {
    public final static Skills INSTANCE = new Skills();
    private final HashMap<Integer, Skill> factories = new HashMap<Integer, Skill>();

    private Skills() {
    }

    public void addSkill(Skill skill) {
        assert (!factories.containsKey(skill.staticInfo.id));

        factories.put(skill.staticInfo.id, skill);
    }

    public Skill getSkill(int id) {
        return factories.get(id);
    }
}
