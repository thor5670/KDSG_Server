/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.rpc;

import com.google.protobuf.RpcController;
import com.google.protobuf.ServiceException;
import com.icee.myth.common.messageQueue.ServerMessageQueue;
import com.icee.myth.protobuf.RpcServiceProtocol.GMCardProto;
import com.icee.myth.protobuf.RpcServiceProtocol.GMConsumerLogProto;
import com.icee.myth.protobuf.RpcServiceProtocol.GMMailProto;
import com.icee.myth.protobuf.RpcServiceProtocol.GMSandboxProto;
import com.icee.myth.protobuf.RpcServiceProtocol.ManagerControlService;
import com.icee.myth.protobuf.RpcServiceProtocol.VoidProto;
import com.icee.myth.server.message.serverMessage.gmMessage.GetOnlinePlayerCountMessage;
import com.icee.myth.log.GameLogger;
import com.icee.myth.log.message.FileDebugGameLogMessage;
import com.icee.myth.log.message.builder.GameLogMessageBuilder;
import com.icee.myth.protobuf.ExternalCommonProtocol.BoolValueProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.IntStringProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.IntValueProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.IntValuesProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.StringValueProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.VariableValueProto;
import com.icee.myth.protobuf.RpcServiceProtocol.GMItemProto;
import com.icee.myth.server.message.serverMessage.gmMessage.AddMailMessage;
import com.icee.myth.server.message.serverMessage.gmMessage.AddNormalActivityMessage;
import com.icee.myth.server.message.serverMessage.gmMessage.BroadcastMessage;
import com.icee.myth.server.message.serverMessage.gmMessage.DeleteNormalActivityMessage;
import com.icee.myth.server.message.serverMessage.gmMessage.GMMessage;
import com.icee.myth.server.message.serverMessage.gmMessage.GetConsumerLogMessage;
import com.icee.myth.server.message.serverMessage.gmMessage.GetOnlinePlayerIdsMessage;
import com.icee.myth.server.message.serverMessage.gmMessage.KickPlayerMessage;
import com.icee.myth.server.message.serverMessage.gmMessage.RpcModifyPlayerMessage;
import com.icee.myth.server.message.serverMessage.gmMessage.SetCardDrawActivityMessage;
import com.icee.myth.server.message.serverMessage.gmMessage.SetStageActivityMessage;
import com.icee.myth.server.message.serverMessage.gmMessage.ShutUpMessage;
import com.icee.myth.utils.StackTraceUtil;

/**
 *
 * @author liuxianke
 */
public class ManagerControlServiceImpl implements ManagerControlService.BlockingInterface {

    public IntValueProto getOnlinePlayerCount(RpcController controller, VoidProto request) throws ServiceException {
        GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_INFO, "Receive rpc [getOnlinePlayerCount] call."));
        GetOnlinePlayerCountMessage message = new GetOnlinePlayerCountMessage();

        synchronized(message) {
            // 向主线程发包请求获取在线人数
            ServerMessageQueue.queue().offer(message);
            try {
                // 等待结果
                message.wait();

                return message.response;
            } catch (InterruptedException ex) {
                GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(
                        FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_ERROR,
                        StackTraceUtil.getStackTrace(ex)));
                throw new ServiceException(StackTraceUtil.getStackTrace(ex));
            }
        }
    }

    public BoolValueProto kickPlayer(RpcController controller, IntValueProto request) throws ServiceException {
        GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_INFO, "Receive rpc [kickPlayer] call."));
        KickPlayerMessage message = new KickPlayerMessage(request.getValue());
        synchronized(message) {
            ServerMessageQueue.queue().offer(message);
            try {
                // 等待结果
                message.wait();
                BoolValueProto.Builder builder = BoolValueProto.newBuilder();
                builder.setValue(true);
                return builder.build();
            } catch (InterruptedException ex) {
                GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(
                        FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_ERROR,
                        StackTraceUtil.getStackTrace(ex)));
                throw new ServiceException(StackTraceUtil.getStackTrace(ex));
            }
        }
    }

    public BoolValueProto shutUp(RpcController controller, VariableValueProto request) throws ServiceException {
        GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_INFO, "Receive rpc [shutUp] call."));
        ShutUpMessage message = new ShutUpMessage(request.getId(), (int)request.getValue());
        synchronized(message) {
            ServerMessageQueue.queue().offer(message);
            try {
                // 等待结果
                message.wait();
                BoolValueProto.Builder builder = BoolValueProto.newBuilder();
                builder.setValue(true);
                return builder.build();
            } catch (InterruptedException ex) {
                GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(
                        FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_ERROR,
                        StackTraceUtil.getStackTrace(ex)));
                throw new ServiceException(StackTraceUtil.getStackTrace(ex));
            }
        }
    }

    public IntValuesProto getOnlinePlayerIds(RpcController controller, VoidProto request) throws ServiceException {
        GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_INFO, "Receive rpc [getOnlinePlayerIds] call."));
        GetOnlinePlayerIdsMessage message = new GetOnlinePlayerIdsMessage();

        synchronized(message) {
            // 向主线程发包请求获取在线人数
            ServerMessageQueue.queue().offer(message);
            try {
                // 等待结果
                message.wait();

                return message.response.build();
            } catch (InterruptedException ex) {
                GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(
                        FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_ERROR,
                        StackTraceUtil.getStackTrace(ex)));
                throw new ServiceException(StackTraceUtil.getStackTrace(ex));
            }
        }
    }

    public BoolValueProto broadcast(RpcController controller, StringValueProto request) throws ServiceException {
        GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_INFO, "Receive rpc [broadcast] call."));
        BroadcastMessage message = new BroadcastMessage(request.getValue());
        synchronized(message) {
            ServerMessageQueue.queue().offer(message);
            try {
                // 等待结果
                message.wait();
                BoolValueProto.Builder builder = BoolValueProto.newBuilder();
                builder.setValue(true);
                return builder.build();
            } catch (InterruptedException ex) {
                GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(
                        FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_ERROR,
                        StackTraceUtil.getStackTrace(ex)));
                throw new ServiceException(StackTraceUtil.getStackTrace(ex));
            }
        }
    }

    public BoolValueProto addMail(RpcController controller, GMMailProto request) throws ServiceException {
        GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_INFO, "Receive rpc [addMail] call."));
        AddMailMessage message = new AddMailMessage(request);
        synchronized(message) {
            ServerMessageQueue.queue().offer(message);
            try {
                // 等待结果
                message.wait();
                BoolValueProto.Builder builder = BoolValueProto.newBuilder();
                builder.setValue(true);
                return builder.build();
            } catch (InterruptedException ex) {
                GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(
                        FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_ERROR,
                        StackTraceUtil.getStackTrace(ex)));
                throw new ServiceException(StackTraceUtil.getStackTrace(ex));
            }
        }
    }

    private BoolValueProto syncRpcModifyPlayerMessage(RpcModifyPlayerMessage message) throws ServiceException{
        synchronized(message) {
            ServerMessageQueue.queue().offer(message);
            try {
                // 等待结果
                message.wait();
                BoolValueProto.Builder builder = BoolValueProto.newBuilder();
                builder.setValue(message.result);
                return builder.build();
            } catch (InterruptedException ex) {
                GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(
                        FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_ERROR,
                        StackTraceUtil.getStackTrace(ex)));
                throw new ServiceException(StackTraceUtil.getStackTrace(ex));
            }
        }
    }

    public BoolValueProto setSilver(RpcController controller, VariableValueProto request) throws ServiceException {
        GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_INFO, "Receive rpc [setBluesoul] call."));
        RpcModifyPlayerMessage message = new RpcModifyPlayerMessage(GMMessage.GMMessageType.GM_SET_SILVER, request.getId(), request);
        return syncRpcModifyPlayerMessage(message);
    }

    public BoolValueProto setGold1(RpcController controller, VariableValueProto request) throws ServiceException {
        GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_INFO, "Receive rpc [setGold1] call."));
        RpcModifyPlayerMessage message = new RpcModifyPlayerMessage(GMMessage.GMMessageType.GM_SET_GOLD1, request.getId(), request);
        return syncRpcModifyPlayerMessage(message);
    }

    public BoolValueProto setGold2(RpcController controller, VariableValueProto request) throws ServiceException {
        GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_INFO, "Receive rpc [setGold2] call."));
        RpcModifyPlayerMessage message = new RpcModifyPlayerMessage(GMMessage.GMMessageType.GM_SET_GOLD2, request.getId(), request);
        return syncRpcModifyPlayerMessage(message);
    }

    public BoolValueProto editGold1(RpcController controller, VariableValueProto request) throws ServiceException {
        GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_INFO, "Receive rpc [editGold1] call."));
        RpcModifyPlayerMessage message = new RpcModifyPlayerMessage(GMMessage.GMMessageType.GM_EDIT_GOLD1, request.getId(), request);
        return syncRpcModifyPlayerMessage(message);
    }

    public BoolValueProto editGold2(RpcController controller, VariableValueProto request) throws ServiceException {
        GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_INFO, "Receive rpc [editGold2] call."));
        RpcModifyPlayerMessage message = new RpcModifyPlayerMessage(GMMessage.GMMessageType.GM_EDIT_GOLD2, request.getId(), request);
        return syncRpcModifyPlayerMessage(message);
    }

    public BoolValueProto setEnergy(RpcController controller, VariableValueProto request) throws ServiceException {
        GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_INFO, "Receive rpc [setOracle] call."));
        RpcModifyPlayerMessage message = new RpcModifyPlayerMessage(GMMessage.GMMessageType.GM_SET_ENERGY, request.getId(), request);
        return syncRpcModifyPlayerMessage(message);
    }

    public BoolValueProto setVipExp(RpcController controller, VariableValueProto request) throws ServiceException {
        GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_INFO, "Receive rpc [setVipExp] call."));
        RpcModifyPlayerMessage message = new RpcModifyPlayerMessage(GMMessage.GMMessageType.GM_SET_VIP_EXP, request.getId(), request);
        return syncRpcModifyPlayerMessage(message);
    }

    public BoolValueProto addItem(RpcController controller, GMItemProto request) throws ServiceException {
        GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_INFO, "Receive rpc [addItem] call."));
        RpcModifyPlayerMessage message = new RpcModifyPlayerMessage(GMMessage.GMMessageType.GM_ADD_ITEM, request.getCid(),  request);
        return syncRpcModifyPlayerMessage(message);
    }

    public BoolValueProto removeItem(RpcController controller, GMItemProto request) throws ServiceException {
        GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_INFO, "Receive rpc [removeItem] call."));
        RpcModifyPlayerMessage message = new RpcModifyPlayerMessage(GMMessage.GMMessageType.GM_REMOVE_ITEM, request.getCid(),  request);
        return syncRpcModifyPlayerMessage(message);
    }

    public BoolValueProto changePlayerName(RpcController controller, IntStringProto request) throws ServiceException {
        GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_INFO, "Receive rpc [changePlayername] call."));
        RpcModifyPlayerMessage message = new RpcModifyPlayerMessage(GMMessage.GMMessageType.GM_CHANGE_PLAYER_NAME, request.getIntValue(),  request);
        return syncRpcModifyPlayerMessage(message);
    }

    public BoolValueProto addCard(RpcController controller, GMCardProto request) throws ServiceException {
        GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_INFO, "Receive rpc [addCard] call."));
        RpcModifyPlayerMessage message = new RpcModifyPlayerMessage(GMMessage.GMMessageType.GM_ADD_CARD, request.getCid(), request);
        return syncRpcModifyPlayerMessage(message);
    }

    public BoolValueProto removeCard(RpcController controller, VariableValueProto request) throws ServiceException {
        GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_INFO, "Receive rpc [removeCard] call."));
        RpcModifyPlayerMessage message = new RpcModifyPlayerMessage(GMMessage.GMMessageType.GM_REMOVE_CARD, request.getId(), request);
        return syncRpcModifyPlayerMessage(message);
    }

    public BoolValueProto changeSandbox(RpcController controller, GMSandboxProto request) throws ServiceException {
        GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_INFO, "Receive rpc [changeSandbox] call."));
        RpcModifyPlayerMessage message = new RpcModifyPlayerMessage(GMMessage.GMMessageType.GM_CHANGE_SANDBOX, request.getCid(), request);
        return syncRpcModifyPlayerMessage(message);
    }

    public BoolValueProto addNormalActivity(RpcController controller, StringValueProto request) throws ServiceException {
        GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_INFO, "Receive rpc [addNormalActivity] call."));
        AddNormalActivityMessage message = new AddNormalActivityMessage(request.getValue());
        synchronized(message) {
            // 向主线程发包添加活动
            ServerMessageQueue.queue().offer(message);
            try {
                // 等待结果
                message.wait();
                BoolValueProto.Builder builder = BoolValueProto.newBuilder();
                builder.setValue(message.response);
                return builder.build();
            } catch (InterruptedException ex) {
                GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(
                        FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_ERROR,
                        StackTraceUtil.getStackTrace(ex)));
                throw new ServiceException(StackTraceUtil.getStackTrace(ex));
            }
        }
    }

    public BoolValueProto deleteNormalActivity(RpcController controller, IntValueProto request) throws ServiceException {
        GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_INFO, "Receive rpc [deleteNormalActivity] call."));
        DeleteNormalActivityMessage message = new DeleteNormalActivityMessage(request.getValue());
        synchronized(message) {
            // 向主线程发包请求获取在线人数
            ServerMessageQueue.queue().offer(message);
            try {
                // 等待结果
                message.wait();
                BoolValueProto.Builder builder = BoolValueProto.newBuilder();
                builder.setValue(message.response);
                return builder.build();
            } catch (InterruptedException ex) {
                GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(
                        FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_ERROR,
                        StackTraceUtil.getStackTrace(ex)));
                throw new ServiceException(StackTraceUtil.getStackTrace(ex));
            }
        }
    }

    public BoolValueProto setCardDrawActivity(RpcController controller, StringValueProto request) throws ServiceException {
        GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_INFO, "Receive rpc [setCardDrawActivity] call."));
        SetCardDrawActivityMessage message = new SetCardDrawActivityMessage(request.getValue());
        synchronized(message) {
            // 向主线程发包添加活动
            ServerMessageQueue.queue().offer(message);
            try {
                // 等待结果
                message.wait();
                BoolValueProto.Builder builder = BoolValueProto.newBuilder();
                builder.setValue(message.response);
                return builder.build();
            } catch (InterruptedException ex) {
                GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(
                        FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_ERROR,
                        StackTraceUtil.getStackTrace(ex)));
                throw new ServiceException(StackTraceUtil.getStackTrace(ex));
            }
        }
    }

    public BoolValueProto setStageActivity(RpcController controller, StringValueProto request) throws ServiceException {
        GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_INFO, "Receive rpc [setStageActivity] call."));
        SetStageActivityMessage message = new SetStageActivityMessage(request.getValue());
        synchronized(message) {
            // 向主线程发包添加活动
            ServerMessageQueue.queue().offer(message);
            try {
                // 等待结果
                message.wait();
                BoolValueProto.Builder builder = BoolValueProto.newBuilder();
                builder.setValue(message.response);
                return builder.build();
            } catch (InterruptedException ex) {
                GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(
                        FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_ERROR,
                        StackTraceUtil.getStackTrace(ex)));
                throw new ServiceException(StackTraceUtil.getStackTrace(ex));
            }
        }
    }

    public BoolValueProto setLevel(RpcController controller, VariableValueProto request) throws ServiceException {
        GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_INFO, "Receive rpc [setLevel] call."));
        RpcModifyPlayerMessage message = new RpcModifyPlayerMessage(GMMessage.GMMessageType.GM_SET_LEVEL, request.getId(), request);
        return syncRpcModifyPlayerMessage(message);
    }

    public StringValueProto consumerLog(RpcController controller, GMConsumerLogProto request) throws ServiceException {
        GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_INFO, "Receive rpc [getConsumerLog] call."));
        GetConsumerLogMessage message = new GetConsumerLogMessage(request);
        synchronized(message) {
            // 向主线程发包添加活动
            ServerMessageQueue.queue().offer(message);
            try {
                // 等待结果
                message.wait();
                StringValueProto.Builder builder = StringValueProto.newBuilder();
                builder.setValue(message.response);
                return builder.build();
            } catch (InterruptedException ex) {
                GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(
                        FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_ERROR,
                        StackTraceUtil.getStackTrace(ex)));
                throw new ServiceException(StackTraceUtil.getStackTrace(ex));
            }
        }
    }
}
