/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icee.myth.server.rpc.handler;

import com.google.protobuf.GeneratedMessage;
import com.icee.myth.protobuf.ExternalCommonProtocol.VariableValueProto;
import com.icee.myth.server.actor.Human;

/**
 *
 * @author yangyi
 */
public class SetEnergyRpcHandler implements RpcModifyPlayerHandler {

    public static final SetEnergyRpcHandler INSTANCE = new SetEnergyRpcHandler();

    private SetEnergyRpcHandler() {
    }

    public boolean handle(Human human, GeneratedMessage proto) {
        int num = (int) ((VariableValueProto) proto).getValue();
        int curNum = human.energy;
        int increaseNum = num - curNum;
        if (increaseNum > 0) {
            human.increaseEnergy(increaseNum, human.inGame);
        } else {
            human.decreaseEnergy(-increaseNum, human.inGame);
        }
        return true;
    }
}
