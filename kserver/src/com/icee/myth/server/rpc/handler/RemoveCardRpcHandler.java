/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icee.myth.server.rpc.handler;

import com.google.protobuf.GeneratedMessage;
import com.icee.myth.protobuf.ExternalCommonProtocol.VariableValueProto;
import com.icee.myth.server.actor.Human;

/**
 *
 * @author liuxianke
 */
public class RemoveCardRpcHandler implements RpcModifyPlayerHandler {

    public static final RemoveCardRpcHandler INSTANCE = new RemoveCardRpcHandler();

    private RemoveCardRpcHandler() {
    }

    public boolean handle(Human human, GeneratedMessage proto) {
        VariableValueProto variableValueProto = (VariableValueProto) proto;
        return human.cards.removeCard((int) variableValueProto.getValue(), human.inGame);
    }
}
