/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.rpc.handler;

import com.google.protobuf.GeneratedMessage;
import com.icee.myth.protobuf.ExternalCommonProtocol.VariableValueProto;
import com.icee.myth.server.actor.Human;
import com.icee.myth.server.levelup.HumanLevelsConfig;

/**
 *
 * @author lidonglin
 */
public class SetLevelRpcHandler implements RpcModifyPlayerHandler{
     public static final SetLevelRpcHandler INSTANCE = new SetLevelRpcHandler();

    private SetLevelRpcHandler() {
    }

    public boolean handle(Human human, GeneratedMessage proto) {
        int maxLevel = HumanLevelsConfig.INSTANCE.maxLevel;
        int level = (int) ((VariableValueProto) proto).getValue();
        if (level <= maxLevel) {
            if (level > human.lv) {
                human.levelup(level - human.lv, true);
            }
        }
        return true;
    }
}
