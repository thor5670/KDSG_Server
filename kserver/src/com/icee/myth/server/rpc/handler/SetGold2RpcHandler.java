/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icee.myth.server.rpc.handler;

import com.google.protobuf.GeneratedMessage;
import com.icee.myth.protobuf.ExternalCommonProtocol.VariableValueProto;
import com.icee.myth.server.actor.Human;
import com.icee.myth.utils.Consts;

/**
 *
 * @author yangyi
 */
public class SetGold2RpcHandler implements RpcModifyPlayerHandler {

    public static final SetGold2RpcHandler INSTANCE = new SetGold2RpcHandler();

    private SetGold2RpcHandler() {
    }

    public boolean handle(Human human, GeneratedMessage proto) {
        int num = (int) ((VariableValueProto) proto).getValue();
        int curNum = human.getGold2();
        int increaseNum = num - curNum;
        if (increaseNum > 0) {
            human.increaseGold(0, increaseNum, Consts.SOUL_CHANGE_LOG_TYPE_GM_INCREASE_YELLOWSOUL2, Consts.SOUL_CHANGE_LOG_SUBTYPE_NONE, human.inGame);
        } else {
            human.decreaseGold(-increaseNum, Consts.SOUL_CHANGE_LOG_TYPE_GM_DECREASE, Consts.SOUL_CHANGE_LOG_SUBTYPE_NONE, human.inGame, false);
        }
        return true;
    }
}
