/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.rpc.handler;

import com.google.protobuf.GeneratedMessage;
import com.icee.myth.server.actor.Human;

/**
 *
 * @author yangyi
 */
public interface  RpcModifyPlayerHandler {

    public boolean handle(Human human, GeneratedMessage proto);
}
