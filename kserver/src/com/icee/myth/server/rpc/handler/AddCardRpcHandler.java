/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icee.myth.server.rpc.handler;

import com.google.protobuf.GeneratedMessage;
import com.icee.myth.protobuf.RpcServiceProtocol.GMCardProto;
import com.icee.myth.server.actor.Human;

/**
 *
 * @author liuxianke
 */
public class AddCardRpcHandler implements RpcModifyPlayerHandler {

    public static final AddCardRpcHandler INSTANCE = new AddCardRpcHandler();

    private AddCardRpcHandler() {
    }

    public boolean handle(Human human, GeneratedMessage proto) {
        GMCardProto gmCardProto = (GMCardProto) proto;
        return human.cards.addCard(gmCardProto.getCardTypeId(), gmCardProto.getCardLevel(), 0, human.inGame) != null;
    }
}
