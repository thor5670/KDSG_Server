/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.card;

import com.icee.myth.log.GameLogger;
import com.icee.myth.log.message.FileDebugGameLogMessage;
import com.icee.myth.log.message.builder.GameLogMessageBuilder;
import java.util.HashMap;

/**
 *
 * @author liuxianke
 */
public class CardFactories {
    public final HashMap<Integer, CardFactory> factories = new HashMap<Integer, CardFactory>();

    public static final CardFactories INSTANCE = new CardFactories();

    private CardFactories() {
    }

    public void addCardFactory(CardFactory cardFactory) {
        assert (!factories.containsKey(cardFactory.staticInfo.id));
        factories.put(cardFactory.staticInfo.id, cardFactory);
    }

    public CardFactory getCardFactory(int id) {
        CardFactory cardFactory = factories.get(id);
        if (cardFactory == null) {
            GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(
                    FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_ERROR,
                    "Card["+id+"] info not found."));
        }
        return cardFactory;
    }
}
