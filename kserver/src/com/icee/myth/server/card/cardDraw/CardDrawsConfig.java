/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.card.cardDraw;

import com.icee.myth.server.GameServer;
import com.icee.myth.utils.Consts;
import com.icee.myth.utils.JSONHelper;

/**
 *
 * @author liuxianke
 */
public class CardDrawsConfig {
    public CardDrawStaticInfo[] cardDrawStaticInfos;    // 卡包列表

    public static final CardDrawsConfig INSTANCE = JSONHelper.parseFileNoException(GameServer.INSTANCE.serverId + "/" + Consts.CARDDRAWSCONFIG_FILEPATH, CardDrawsConfig.class);

    private CardDrawsConfig() {
    }

    public CardDrawStaticInfo getCardDrawStaticInfo(int drawId) {
        if ((drawId >= 0) && (drawId < CardDrawsConfig.INSTANCE.cardDrawStaticInfos.length)) {
            return cardDrawStaticInfos[drawId];
        }

        return null;
    }
}
