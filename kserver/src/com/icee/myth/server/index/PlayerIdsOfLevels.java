/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.index;

import com.icee.myth.log.GameLogger;
import com.icee.myth.log.message.FileDebugGameLogMessage;
import com.icee.myth.log.message.builder.GameLogMessageBuilder;
import com.icee.myth.server.levelup.HumanLevelsConfig;

/**
 * 按等级记录内存中玩家（全局索引）
 * @author liuxianke
 */
public class PlayerIdsOfLevels {
    public final PlayerIdsOfLevel[] playerIdsOfLevels;  // 以军衔为下标记录每个军衔中存在的内存中玩家id

    public static final PlayerIdsOfLevels INSTANCE = new PlayerIdsOfLevels();

    private PlayerIdsOfLevels() {
        playerIdsOfLevels = new PlayerIdsOfLevel[HumanLevelsConfig.INSTANCE.maxLevel];
    }

    public PlayerIdsOfLevel getPlayerIdsOfLevel(int level) {
        return playerIdsOfLevels[level - 1];
    }

    public void addPlayerId(int level, int playerId) {
        if ((level > 0) && (level <= HumanLevelsConfig.INSTANCE.maxLevel)) {
            if (playerIdsOfLevels[level - 1] == null) {
                playerIdsOfLevels[level - 1] = new PlayerIdsOfLevel();
            }

            playerIdsOfLevels[level - 1].add(playerId);
        } else {
            GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(
                    FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_ERROR,
                    "Player[" + playerId + "] level[" + level + "] out of range when add to level index."));
        }
    }

    public void removePlayerId(int level, int playerId) {
        if ((level > 0) && (level <= HumanLevelsConfig.INSTANCE.maxLevel)) {
            if (playerIdsOfLevels[level - 1] != null) {
                playerIdsOfLevels[level - 1].remove(playerId);

                if (playerIdsOfLevels[level - 1].count() == 0) {
                    playerIdsOfLevels[level - 1] = null;
                }
            } else {
                GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(
                        FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_ERROR,
                        "Player-ids of level[" + level + "] is empty when remove player[" + playerId + "]."));
            }
        } else {
            GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(
                    FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_ERROR,
                    "Player[" + playerId + "] level[" + level + "] out of range when remove from level index."));
        }
    }
}
