/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.levelup;

/**
 *
 * @author liuxianke
 */
public class HumanRankConfig {
    public int experience;  // 升下一级所需经验(功勋)
    public float hegemonySilverCoef;  // 争霸白银系数
    public float hegemonyRankExpCoef; // 争霸功勋系数
    public int paySilver;   // 军饷：白银
    public int payGold;     // 军饷：黄金

    public float atkUp;     // 攻击力上升
    public float hpUp;      // 生命上升
    // TODO: 其他属性或系数
}
