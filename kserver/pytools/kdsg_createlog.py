#!/usr/bin/python

__author__="liuxianke"
__date__ ="$2013-3-25 11:10:54$"

def createlogdb(logdbname, day) :
    import MySQLdb
    conn = MySQLdb.connect(host='192.168.0.150',user='mythtest',passwd='jkY5qmGKVcRs4nST')
    cursor = conn.cursor()
    try :
        sql = "SELECT COUNT(1) FROM information_schema.SCHEMATA WHERE SCHEMA_NAME = '" + logdbname + "'"
        cursor.execute(sql)
        cursor.scroll(0)
        retrow = cursor.fetchone()
        if (retrow[0] == 0):
            sql = " CREATE DATABASE if not exists " + logdbname + " default charset utf8 collate utf8_general_ci;"
            cursor.execute(sql)
            sql = " GRANT ALL ON " + logdbname + ".* TO 'mythtest'@'192.168.0.150';"
            cursor.execute(sql)
            conn.close()
            cursor.close()

        conn = MySQLdb.connect(host='192.168.0.150',user='mythtest', passwd='jkY5qmGKVcRs4nST', db=logdbname)
        cursor = conn.cursor()
        cursor.execute(sql)
        
        sql = """ CREATE TABLE if not exists `action""" + day + """` (
  `serverid` int(11) NOT NULL,
  `cid` int(11) NOT NULL,
  `action` int(11) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `intdata1` int(11) DEFAULT NULL,
  `intdata2` int(11) DEFAULT NULL,
  `intdata3` int(11) DEFAULT NULL,
  `intdata4` int(11) DEFAULT NULL,
  `intdata5` int(11) DEFAULT NULL,
  `intdata6` int(11) DEFAULT NULL,
  `intdata7` int(11) DEFAULT NULL,
  `intdata8` int(11) DEFAULT NULL,
  `intdata9` int(11) DEFAULT NULL,
  `intdata10` int(11) DEFAULT NULL,
  `longdata1` bigint(20) DEFAULT NULL,
  `longdata2` bigint(20) DEFAULT NULL,
  `stringdata1` varchar(120) DEFAULT NULL,
  `stringdata2` varchar(120) DEFAULT NULL,
  `blobdata` tinyblob
) ENGINE=MyISAM DEFAULT CHARSET=utf8; """
        cursor.execute(sql)

        sql = """ CREATE TABLE if not exists `cash""" + day + """` (
  `serverid` int(11) NOT NULL,
  `cid` int(11) NOT NULL,
  `cash` int(11) NOT NULL,
  `activity` int(11) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
"""
        cursor.execute(sql)

        sql = """ CREATE TABLE if not exists `createchar""" + day + """` (
  `serverid` int(11) NOT NULL,
  `cid` int(11) NOT NULL,
  `leaderCardId` int(11) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`serverid`,`cid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
"""
        cursor.execute(sql)

        sql = """ CREATE TABLE if not exists `loginlogout""" + day + """` (
  `serverid` int(11) NOT NULL,
  `cid` int(11) NOT NULL,
  `lv` int(11) NOT NULL,
  `islogin` tinyint(1) NOT NULL,
  `ip` int(11) NOT NULL,
  `duration` bigint(20) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  KEY `LoginIndex` (`serverid`,`cid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
"""
        cursor.execute(sql)

        sql = """ CREATE TABLE if not exists `onlinenum""" + day + """` (
  `serverid` int(11) NOT NULL,
  `onlinenum` int(11) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `OnlineIdex` (`serverid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
"""
        cursor.execute(sql)

        sql = """ CREATE TABLE if not exists `onlinetime""" + day + """` (
  `serverid` int(11) NOT NULL,
  `cid` int(11) NOT NULL,
  `onlinetime` int(11) NOT NULL,
  PRIMARY KEY (`serverid`,`cid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
"""
        cursor.execute(sql)

        sql = """ CREATE TABLE if not exists `gold""" + day + """` (
  `serverid` int(11) NOT NULL,
  `cid` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `subtype` int(11) NOT NULL,
  `change1` int(11) NOT NULL,
  `change2` int(11) NOT NULL,
  `remain1` int(11) NOT NULL,
  `remain2` int(11) NOT NULL,
  `lv` int(11) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
"""
        cursor.execute(sql)

        sql = """ CREATE TABLE if not exists `silver""" + day + """` (
  `serverid` int(11) NOT NULL,
  `cid` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `subtype` int(11) NOT NULL,
  `change` int(11) NOT NULL,
  `remain` bigint(20) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
"""
        cursor.execute(sql)
        
    except Exception, data:
        print Exception,":",data

if __name__ == "__main__":
    from datetime import *
    dt = datetime.today() + timedelta(days=1)
    day = dt.strftime("%Y%m%d")
    logdbname = "sanguolog"
    createlogdb(logdbname, day)
