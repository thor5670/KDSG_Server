/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.gm.controller;

import com.icee.myth.gm.GmServer;
import com.icee.myth.gm.channelhandler.HttpServerHandler;
import com.icee.myth.gm.client.GameServerClient;
import com.icee.myth.utils.Consts;

/**
 *
 * @author liuxianke
 */
public class RemoveCardController extends AbstractController{

    public RemoveCardController(int privilege){
        super(privilege);
    }

    @Override
    public void process() {
        Long serverId = (Long) jobj.get("server");
        Long cid = (Long)jobj.get("cid");
        Long cardInstId = (Long)jobj.get("cardinstid");
        GameServerClient gameServerClient = null;
        if (serverId != null) {
            gameServerClient = GmServer.INSTANCE.getServer(serverId.intValue());
        }
        if(gameServerClient != null && cid != null && cardInstId != null){
            if(gameServerClient.removeCard((int)(long)cid,(int)(long)cardInstId)){
                dbHandler.insertGmOperateLog(userData.userName,Consts.GMOPERATELOG_REMOVECARD,jobj.toJSONString());
                HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", "{\"result\":0}", null);
            } else{
                HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", "{\"result\":-1,\"error\":\"remove card error\"}", null);
            }
        } else{
            HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", "{\"result\":-1,\"error\":\"param error\"}", null);
        }
    }

}
