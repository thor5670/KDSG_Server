/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.gm.controller;

import com.icee.myth.gm.GmServer;
import com.icee.myth.gm.channelhandler.HttpServerHandler;
import com.icee.myth.gm.kf.JsonKFTemplates;
import com.icee.myth.utils.JSONHelper;

/**
 *
 * @author yangyi
 */
public class GetPostsController extends AbstractController{

    public GetPostsController(int privilege){
        super(privilege);
    }

    @Override
    public void process() {
        JsonKFTemplates kfTemplates = GmServer.INSTANCE.kfTemplates;
        String content = "{\"result\":0,\"content\":" + JSONHelper.toJSON(kfTemplates) + "}";
        HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", content, null);
    }
}
