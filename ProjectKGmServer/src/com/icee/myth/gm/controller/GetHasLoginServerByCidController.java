/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icee.myth.gm.controller;

import com.icee.myth.gm.GmServer;
import com.icee.myth.gm.api.PlatformUidTransform;
import com.icee.myth.gm.channelhandler.HttpServerHandler;
import com.icee.myth.gm.client.GameServerClient;
import com.icee.myth.gm.db.DbHandler;
import com.icee.myth.gm.player.PlayerInfo;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author yangyi
 */
public class GetHasLoginServerByCidController extends AbstractController {

    public GetHasLoginServerByCidController(int privilege) {
        super(privilege);
    }

    @Override
    public void process() {
        Long cid = (Long) jobj.get("cid");
        if (cid != null) {
            DbHandler managerDbHandler = new DbHandler(GmServer.INSTANCE.managerDbHost, GmServer.INSTANCE.managerDbName);
            PlayerInfo playerInfo = managerDbHandler.getPlayerInfoFromDBByCid((int)(long)cid);
            if (playerInfo != null) {
                List<Integer> servers = managerDbHandler.getServersHasLogin( playerInfo.thirdPartyUserId);
                if (servers != null) {
                    String puid = PlatformUidTransform.passportToPuid(playerInfo.thirdPartyUserId);
                    String content = "{\"result\":0,\"puid\":\"" + puid + "\",\"passport\":\"" + playerInfo.thirdPartyUserId + "\",\"cid\":" + playerInfo.cid + ",\"servers\":[";
                    boolean firstserver = true;
                    for (Iterator<Integer> it = servers.iterator(); it.hasNext();) {
                        Integer id = it.next();
                        GameServerClient tmpServerClient = GmServer.INSTANCE.getServer(id);
                        if (tmpServerClient != null) {
                            if (!firstserver) {
                                content += ",";
                            }
                            firstserver = false;
                            content += "{\"id\":" + tmpServerClient.getId() + ",\"name\":\"" + tmpServerClient.getName() + "\"}";
                        }
                    }
                    content += "],\"forbidtalkendtime\":" + playerInfo.forbiddenEndTalkTime + ",\"forbidloginendtime\":" + playerInfo.forbiddenEndLoginTime + "}";
                    HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", content, null);
                } else {
                    HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", "{\"result\":-1,\"error\":\"get servers list from db error\"}", null);
                }
            } else {
                HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", "{\"result\":-1,\"error\":\"can not find player info\"}", null);
            }
            managerDbHandler.close();
        } else {
            HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", "{\"result\":-1,\"error\":\"param error\"}", null);
        }
    }
}
