/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icee.myth.gm.controller;

import com.icee.myth.gm.GmServer;
import com.icee.myth.gm.channelhandler.HttpServerHandler;
import com.icee.myth.gm.message.AddPostMessage;
import com.icee.myth.utils.Consts;

/**
 *
 * @author yangyi
 */
public class AddPostController extends AbstractController {

    public AddPostController(int privilege) {
        super(privilege);
    }

    @Override
    public void process() {
        Long serverId = (Long) jobj.get("serverId");
        String startTime = (String) jobj.get("startTime");      // 开始时间，格式（yyyy-MM-dd HH:mm:ss）
        String endTime = (String) jobj.get("endTime");        // 结束时间，格式（yyyy-MM-dd HH:mm:ss）
        Long period = (Long) jobj.get("period");            // 周期（单位秒）
        String message = (String) jobj.get("message");        // 通告消息

        if (serverId != null && startTime != null && endTime != null && period != null && message != null && period > 0) {
            GmServer.INSTANCE.messageQueue.add(new AddPostMessage((int) (long) serverId, startTime, endTime, (int) (long) period, message));
            dbHandler.insertGmOperateLog(userData.userName, Consts.GMOPERATELOG_ADDPERIODPOST, jobj.toJSONString());
            HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", "{\"result\":0}", null);
        } else {
            HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", "{\"result\":-1,\"error\":\"param error\"}", null);
        }
    }
}
