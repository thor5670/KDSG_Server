/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.gm.controller;

import com.icee.myth.gm.GmServer;
import com.icee.myth.gm.api.PlatformUidTransform;
import com.icee.myth.gm.channelhandler.HttpServerHandler;
import com.icee.myth.gm.client.GameServerClient;
import com.icee.myth.gm.db.DbHandler;
import com.icee.myth.gm.player.PlayerInfo;
import com.icee.myth.utils.Escape;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author yangyi
 */
public class GetHasLoginServerByPuidController extends AbstractController{

    public GetHasLoginServerByPuidController(int privilege){
        super(privilege);
    }

    @Override
    public void process() {
        String puid = (String)jobj.get("puid");
        puid = Escape.unescape(puid);
        String passport = PlatformUidTransform.puidToPassport(puid);
        if (passport != null) {
            DbHandler managerDbHandler = new DbHandler(GmServer.INSTANCE.managerDbHost, GmServer.INSTANCE.managerDbName);
            List<Integer> servers = managerDbHandler.getServersHasLogin(passport);
            PlayerInfo playerInfo = managerDbHandler.getPlayerInfoFromDB( passport);
            managerDbHandler.close();
            if(servers != null && playerInfo != null){
                String content = "{\"result\":0,\"puid\":\"" + puid + "\",\"passport\":\"" + passport + "\",\"cid\":" + playerInfo.cid + ",\"servers\":[";
                boolean firstserver = true;
                for (Iterator<Integer> it = servers.iterator(); it.hasNext();) {
                    Integer id = it.next();
                    GameServerClient tmpServerClient = GmServer.INSTANCE.getServer(id);
                    if(tmpServerClient != null){
                        if(!firstserver){
                            content += "," ;
                        }
                        firstserver = false;
                        content += "{\"id\":" + tmpServerClient.getId() + ",\"name\":\"" + tmpServerClient.getName() + "\"}";
                    }
                }
                content += "],\"forbidtalkendtime\":" + playerInfo.forbiddenEndTalkTime + ",\"forbidloginendtime\":" + playerInfo.forbiddenEndLoginTime + "}";
                HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", content, null);
            } else if (servers == null) {
                HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", "{\"result\":-1,\"error\":\"get servers list from db error\"}", null);
            } else {
                HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", "{\"result\":-1,\"error\":\"can not find player info\"}", null);
            }
        } else{
            HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", "{\"result\":-1,\"error\":\"param error\"}", null);
        }
    }
}
