/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.gm.controller;

import com.icee.myth.gm.client.GameServerClient;
import com.icee.myth.utils.Consts;

/**
 *
 * @author yangyi
 */
public class SetVipExp extends RpcSetController{

    public SetVipExp(int privilege) {
        super(privilege);
    }

    @Override
    public boolean rpcSet(GameServerClient gameServerClient, int cid, int num) {
        if(gameServerClient.rpcSet(Consts.RPCSET_VIPEXP, cid, num)) {
            dbHandler.insertGmOperateLog(userData.userName, Consts.GMOPERATELOG_SETVIPEXP, jobj.toJSONString());
            return true;
        }
        return false;
    }

}
