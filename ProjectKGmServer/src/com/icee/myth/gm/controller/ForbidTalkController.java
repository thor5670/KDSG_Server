/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.gm.controller;

import com.icee.myth.gm.GmServer;
import com.icee.myth.gm.channelhandler.HttpServerHandler;
import com.icee.myth.gm.client.GameServerClient;
import com.icee.myth.gm.db.DbHandler;
import com.icee.myth.utils.Consts;

/**
 *
 * @author yangyi
 */
public class ForbidTalkController extends AbstractController{

    public ForbidTalkController(int privilege) {
        super(privilege);
    }

    public boolean  forbidTalk(String passport, int time) {
        DbHandler managerDbHandler = new DbHandler(GmServer.INSTANCE.managerDbHost, GmServer.INSTANCE.managerDbName);
        time = (int) (System.currentTimeMillis() / 1000 + time * 60);
        if(managerDbHandler.updateForbinTalkTime(passport, time)) {
            int cid = managerDbHandler.getCidByPassport(passport);
            for (GameServerClient gameServerClient : GmServer.INSTANCE.getGameServerClients().values()) {
                gameServerClient.forbidTalk(cid,time);
            }
            managerDbHandler.close();
            return true;
        }
        managerDbHandler.close();
        return false;
    }

    @Override
    public void process() {
        Long time = (Long) jobj.get("forbidtalktime");
        String passport = (String)jobj.get("passport");
        if(passport != null && time != null){
            if(forbidTalk(passport,(int)(long)time)){
                dbHandler.insertGmOperateLog(userData.userName, Consts.GMOPERATELOG_FORBIDTALK,jobj.toJSONString());
                HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", "{\"result\":0}", null);
            } else{
                HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", "{\"result\":-1,\"error\":\"forbit talk error\"}", null);
            }
        } else{
            HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", "{\"result\":-1,\"error\":\"param error\"}", null);
        }
    }
}
