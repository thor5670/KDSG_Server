/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.gm.controller;

import com.icee.myth.gm.channelhandler.HttpServerHandler;
import com.icee.myth.utils.Consts;
import com.icee.myth.utils.Escape;

/**
 * @author yangyi
 */
public class AdminModifyController extends AbstractController {

    public AdminModifyController(int privilege) {
        super(privilege);
    }

    @Override
    public void process() {
        String userName = (String) jobj.get("user");
        userName = Escape.unescape(userName);
        Long privlg = (Long) jobj.get("privilege");

        if (userName != null && privlg != null) {
            if (dbHandler.modifyUser(userName, privlg)) {
                dbHandler.insertGmOperateLog(userData.userName, Consts.GMOPERATELOG_MODFIYPRIVILEGE, jobj.toJSONString());
                HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", "{\"result\":0}", null);
            } else {
                HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", "{\"result\":-1,\"error\":\"modify user error\"}", null);
            }
        } else {
            HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", "{\"result\":-1,\"error\":\"param error\"}", null);
        }
    }
}
