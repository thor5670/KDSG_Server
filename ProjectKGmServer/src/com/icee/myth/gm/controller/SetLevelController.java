/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.gm.controller;

import com.icee.myth.gm.client.GameServerClient;
import com.icee.myth.utils.Consts;

/**
 *
 * @author lidonglin
 */
public class SetLevelController extends RpcSetController{

    public SetLevelController(int privilege) {
        super(privilege);
    }
    
    @Override
    public boolean rpcSet(GameServerClient gameServerClient, int cid, int level) {
         if(gameServerClient.rpcSet(Consts.RPCSET_LEVEL, cid, level)) {
            dbHandler.insertGmOperateLog(userData.userName, Consts.RPCSET_LEVEL, jobj.toJSONString());
            return true;
        }
        return false;
    }

}
