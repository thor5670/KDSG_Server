/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.gm.controller;

import java.util.List;

import com.icee.myth.gm.GmServer;
import com.icee.myth.gm.channelhandler.HttpServerHandler;
import com.icee.myth.gm.client.GameServerClient;
import com.icee.myth.gm.db.DbHandler;
import com.icee.myth.utils.Escape;
import com.icee.myth.utils.JSONHelper;

/**
 *
 * @author yangyi
 */
public class GetSimilarNamesController extends AbstractController{

    public GetSimilarNamesController(int privilege){
        super(privilege);
    }

    @Override
    public void process() {
         Long serverId = (Long) jobj.get("server");
        GameServerClient gameServerClient = null;
        if (serverId != null) {
            gameServerClient = GmServer.INSTANCE.getServer(serverId.intValue());
        }
        String playerName = (String)jobj.get("playername");
        playerName = Escape.unescape(playerName);
        if (gameServerClient != null && playerName != null) {
            DbHandler gameDbHandler = new DbHandler(gameServerClient.serverConfig.dbHost, gameServerClient.serverConfig.dbName);
            List<String> names = gameDbHandler.getSimilarNamesByPlayerName(playerName);
            gameDbHandler.close();
            String content = "{\"result\":0,\"names\":" + JSONHelper.toJSON(names) + "}";
            HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", content, null);
        } else{
            HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", "{\"result\":-1,\"error\":\"param error\"}", null);
        }
    }
}
