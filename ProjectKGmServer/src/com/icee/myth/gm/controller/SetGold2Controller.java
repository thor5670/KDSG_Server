/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.gm.controller;

import com.icee.myth.gm.client.GameServerClient;
import com.icee.myth.utils.Consts;

/**
 *
 * @author liuxianke
 */
public class SetGold2Controller extends RpcSetController{

    public SetGold2Controller(int privilege) {
        super(privilege);
    }

    @Override
    public boolean rpcSet(GameServerClient gameServerClient, int cid, int num) {
        if(gameServerClient.rpcSet(Consts.RPCSET_GOLD2, cid, num)) {
            dbHandler.insertGmOperateLog(userData.userName, Consts.GMOPERATELOG_SETGOLD2, jobj.toJSONString());
            return true;
        }
        return false;
    }

}
