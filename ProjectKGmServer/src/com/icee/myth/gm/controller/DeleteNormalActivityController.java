/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.gm.controller;

import com.icee.myth.gm.GmServer;
import com.icee.myth.gm.channelhandler.HttpServerHandler;
import com.icee.myth.gm.client.GameServerClient;
import com.icee.myth.utils.Consts;
import com.icee.myth.utils.JSONHelper;
import java.util.ArrayList;

/**
 *
 * @author liuxianke
 */
public class DeleteNormalActivityController extends AbstractController {

    public DeleteNormalActivityController(int privilege) {
        super(privilege);
    }

    @Override
    public void process() {
        Long serverId = (Long) jobj.get("server");
        Long activityId = (Long) jobj.get("activityid");
        
        if (serverId != null && activityId != null) {
            if (serverId >= 0) {
                GameServerClient gameServerClient = GmServer.INSTANCE.getServer(serverId.intValue());
                if (gameServerClient != null) {
                    boolean ret = gameServerClient.deleteNormalActivity((int)(long)activityId);
                    if (ret) {
                        dbHandler.insertGmOperateLog(userData.userName, Consts.GMOPERATELOG_DELETENORMALACTIVITY, jobj.toJSONString());
                        HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", "{\"result\":0}", null);
                    } else {
                        HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", "{\"result\":-1,\"error\":\"server error\"}", null);
                    }
                } else {
                    HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", "{\"result\":-1,\"error\":\"server not found\"}", null);
                }
            } else {
                ArrayList<Integer> failServerIds = new ArrayList<Integer>();
                for (GameServerClient client : GmServer.INSTANCE.getGameServerClients().values()) {
                    if (!client.deleteNormalActivity((int)(long)activityId)) {
                        failServerIds.add(client.getId());
                    }
                }
                dbHandler.insertGmOperateLog(userData.userName, Consts.GMOPERATELOG_DELETENORMALACTIVITY, jobj.toJSONString());
                HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", "{\"result\":0,\"failservers\":" + JSONHelper.toJSON(failServerIds) + "}", null);
            }
        } else {
            HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", "{\"result\":-1,\"error\":\"param error\"}", null);
        }
    }
}
