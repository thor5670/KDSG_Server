/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.gm.controller;

import com.icee.myth.gm.channelhandler.HttpServerHandler;
import com.icee.myth.utils.Consts;
import com.icee.myth.utils.Escape;

/**
 *
 * @author yangyi
 */
public class AdminAddController extends AbstractController{

     public AdminAddController(int privilege){
        super(privilege);
    }

    @Override
    public void process() {
        String userName = (String) jobj.get("user");
        userName = Escape.unescape(userName);
        String password = (String) jobj.get("password");
        Long privlg = (Long)jobj.get("privilege");

        if(userName != null && privlg != null){
            if(dbHandler.addUser(userName,password,(int)(long)privlg)){
                dbHandler.insertGmOperateLog(userData.userName,Consts.GMOPERATELOG_ADDUSE,jobj.toJSONString());
                HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", "{\"result\":0}", null);
            } else {
                HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", "{\"result\":-1,\"error\":\"add user error\"}", null);
            }
        } else{
            HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", "{\"result\":-1,\"error\":\"param error\"}", null);
        }
    }
}
