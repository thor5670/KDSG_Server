/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icee.myth.gm.controller;

import com.icee.myth.gm.GmServer;
import com.icee.myth.gm.channelhandler.HttpServerHandler;
import com.icee.myth.gm.kf.JsonKFTemplate;
import com.icee.myth.gm.message.DeletePostMessage;
import com.icee.myth.utils.Consts;
import java.util.Iterator;

/**
 *
 * @author yangyi
 */
public class DeletePostController extends AbstractController {

    public DeletePostController(int privilege) {
        super(privilege);
    }

    @Override
    public void process() {
        Long id = (Long) jobj.get("id");
        if (id != null) {
            boolean deleted = false;
            for (Iterator<JsonKFTemplate> it = GmServer.INSTANCE.kfTemplates.templates.iterator(); it.hasNext();) {
                JsonKFTemplate template = it.next();
                if (template.id == id) {
                    GmServer.INSTANCE.messageQueue.add(new DeletePostMessage((int) (long) id));
                    dbHandler.insertGmOperateLog(userData.userName, Consts.GMOPERATELOG_DELETEPERIODPOST,jobj.toJSONString());
                    HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", "{\"result\":0}", null);
                    deleted = true;
                    break;
                }
            }

            if (!deleted) {
                HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", "{\"result\":-1,\"error\":\"taget post not found\"}", null);
            }
        } else {
            HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", "{\"result\":-1,\"error\":\"param error\"}", null);
        }
    }
}
