/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icee.myth.gm.controller;

import com.icee.myth.gm.GmServer;
import com.icee.myth.gm.channelhandler.HttpServerHandler;
import com.icee.myth.gm.client.GameServerClient;
import com.icee.myth.utils.Consts;

/**
 *
 * @author lidonglin
 */
public class ConsumerLogController extends AbstractController {

    public ConsumerLogController(int privilege) {
        super(privilege);
    }

    @Override
    public void process() {
         //服务器Id  角色Id  货币类型  充值类型
        Long serverId = (Long) jobj.get("server");
        Long cid = (Long) jobj.get("cid");
        Long chargeType = (Long) jobj.get("chargetype");
        Long startTime = (Long)jobj.get("begintime");
        Long endTime = (Long)jobj.get("endtime");

        GameServerClient gameServerClient = null;
        if (serverId != null) {
            gameServerClient = GmServer.INSTANCE.getServer(serverId.intValue());
        }
        if (gameServerClient != null && cid != null && chargeType != null) {
            String resultRet = gameServerClient.consumerLog((int)(long)cid, (int)(long)chargeType, (long)startTime, (long)endTime);
            if (!resultRet.equals("")) {
                dbHandler.insertGmOperateLog(userData.userName, Consts.GMOPERATELOG_CHARGE_LOG, jobj.toJSONString());
                HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", "{\"result\":" + resultRet +"}", null);
            } else {
                HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", "{\"result\":-1,\"error\":\"change player name error\"}", null);
            }
        } else {
            HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", "{\"result\":-1,\"error\":\"param error\"}", null);
        }
    }
}