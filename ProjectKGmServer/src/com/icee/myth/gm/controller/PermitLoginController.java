/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.gm.controller;

import com.icee.myth.gm.GmServer;
import com.icee.myth.gm.channelhandler.HttpServerHandler;
import com.icee.myth.gm.db.DbHandler;
import com.icee.myth.utils.Consts;

/**
 *
 * @author yangyi
 */
public class PermitLoginController extends AbstractController{

    public PermitLoginController(int privilege) {
        super(privilege);
    }

    @Override
    public void process() {
        String passport = (String)jobj.get("passport");
        if(passport != null){
            DbHandler managerDbHandler = new DbHandler(GmServer.INSTANCE.managerDbHost, GmServer.INSTANCE.managerDbName);
            if(managerDbHandler.updateForbinLoginTime(passport, 0)){
                 dbHandler.insertGmOperateLog(userData.userName, Consts.GMOPERATELOG_PERMITLOGIN,jobj.toString());
                 HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", "{\"result\":0}", null);
            } else{
                HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", "{\"result\":-1,\"error\":\"permit login error\"}", null);
            }
            managerDbHandler.close();
        } else{
            HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", "{\"result\":-1,\"error\":\"param error\"}", null);
        }
    }

}
