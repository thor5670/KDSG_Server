/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icee.myth.gm.controller;

import com.icee.myth.utils.Consts;

/**
 * GM命令Controller 工场类
 * @author yangyi Stone Yang
 */
public class ControllerFactory {

    public static AbstractController buildController(String name) {
        if (name.equalsIgnoreCase("/logout")) {
            return new LogoutController(Consts.PRIVILEGE_NONE);
        } else if (name.equalsIgnoreCase("/getServerStatus")) {
            return new GetServerStatusController(Consts.PRIVILEGE_OPERRATE);
        } else if (name.equalsIgnoreCase("/getPlayerCount")) {
            return new GetPlayerCountController(Consts.PRIVILEGE_OPERRATE);
        } else if (name.equalsIgnoreCase("/post")) {
            return new PostController(Consts.PRIVILEGE_POST);
        } else if (name.equalsIgnoreCase("/getPosts")) {
            return new GetPostsController(Consts.PRIVILEGE_POST);
        } else if (name.equalsIgnoreCase("/addPost")) {
            return new AddPostController(Consts.PRIVILEGE_POST);
        } else if (name.equalsIgnoreCase("/deletePost")) {
            return new DeletePostController(Consts.PRIVILEGE_POST);
        } else if (name.equalsIgnoreCase("/getHasLoginServersByGameName")) {
            return new GetHasLoginServerByGameNameController(Consts.PRIVILEGE_QUERY);
        } else if (name.equalsIgnoreCase("/getHasLoginServersByPassport")) {
            return new GetHasLoginServerByPassportController(Consts.PRIVILEGE_QUERY);
        } else if (name.equalsIgnoreCase("/getHasLoginServersByCid")) {
            return new GetHasLoginServerByCidController(Consts.PRIVILEGE_QUERY);
        } else if (name.equalsIgnoreCase("/getHasLoginServersByPuid")) {
            return new GetHasLoginServerByPuidController(Consts.PRIVILEGE_QUERY);
        } else if (name.equalsIgnoreCase("/forbidTalk")) {
            return new ForbidTalkController(Consts.PRIVILEGE_FORBIDLOGINORTALK);
        } else if (name.equalsIgnoreCase("/permitTalk")) {
            return new PermitTalkController(Consts.PRIVILEGE_FORBIDLOGINORTALK);
        } else if (name.equalsIgnoreCase("/forbidLogin")) {
            return new ForbidLoginController(Consts.PRIVILEGE_FORBIDLOGINORTALK);
        } else if (name.equalsIgnoreCase("/permitLogin")) {
            return new PermitLoginController(Consts.PRIVILEGE_FORBIDLOGINORTALK);
        } else if (name.equalsIgnoreCase("/watchChar")) {
            return new WatchCharController(Consts.PRIVILEGE_QUERY);
        } else if (name.equalsIgnoreCase("/setSilver")) {
            return new SetSilverController(Consts.PRIVILEGE_MODIFY);
        } else if (name.equalsIgnoreCase("/setGold1")) {
            return new SetGold1Controller(Consts.PRIVILEGE_MODIFY);
        } else if (name.equalsIgnoreCase("/setGold2")) {
            return new SetGold2Controller(Consts.PRIVILEGE_MODIFY);
        } else if (name.equalsIgnoreCase("/editGold1")) {
            return new EditGold1Controller(Consts.PRIVILEGE_MODIFY);
        } else if (name.equalsIgnoreCase("/editGold2")) {
            return new EditGold2Controller(Consts.PRIVILEGE_MODIFY);
        } else if (name.equalsIgnoreCase("/setEnergy")) {
            return new SetEnergyController(Consts.PRIVILEGE_MODIFY);
        } else if (name.equalsIgnoreCase("/setVipExp")) {
            return new SetVipExp(Consts.PRIVILEGE_MODIFY);
        } else if (name.equalsIgnoreCase("/addItem")) {
            return new AddItemController(Consts.PRIVILEGE_MODIFY);
        } else if (name.equalsIgnoreCase("/setLevel")) {
            return new SetLevelController(Consts.PRIVILEGE_MODIFY);
        } else if (name.equalsIgnoreCase("/removeItem")) {
            return new RemoveItemController(Consts.PRIVILEGE_MODIFY);
        } else if (name.equalsIgnoreCase("/addCard")) {
            return new AddCardController(Consts.PRIVILEGE_MODIFY);
        } else if (name.equalsIgnoreCase("/removeCard")) {
            return new RemoveCardController(Consts.PRIVILEGE_MODIFY);
        } else if (name.equalsIgnoreCase("/changeSandbox")) {
            return new ChangeSandboxController(Consts.PRIVILEGE_MODIFY);
        } else if (name.equalsIgnoreCase("/changePlayerName")) {
            return new ChangePlayerNameController(Consts.PRIVILEGE_MODIFY);
        } else if (name.equalsIgnoreCase("/similarNames")) {
            return new GetSimilarNamesController(Consts.PRIVILEGE_QUERY);
        } else if (name.equalsIgnoreCase("/mail")) {
            return new MailController(Consts.PRIVILEGE_COMPOSATE_NOREWARD);
        } else if (name.equalsIgnoreCase("/addNormalActivity")) {
            return new AddNormalActivityController(Consts.PRIVILEGE_MODIFY);
        } else if (name.equalsIgnoreCase("/deleteNormalActivity")) {
            return new DeleteNormalActivityController(Consts.PRIVILEGE_MODIFY);
        } else if (name.equalsIgnoreCase("/setCardDrawActivity")) {
            return new SetCardDrawActivityController(Consts.PRIVILEGE_MODIFY);
        } else if (name.equalsIgnoreCase("/setStageActivity")) {
            return new SetStageActivityController(Consts.PRIVILEGE_MODIFY);
        } else if (name.equalsIgnoreCase("/admin/operateLog")) {
            return new OperateLogViewController(Consts.PRIVILEGE_QUERY);
        } else if (name.equalsIgnoreCase("/admin/add")) {
            return new AdminAddController(Consts.PRIVILEGE_ADMIN);
        } else if (name.equalsIgnoreCase("/admin/modify")) {
            return new AdminModifyController(Consts.PRIVILEGE_ADMIN);
        } else if (name.equalsIgnoreCase("/admin/view")) {
            return new AdminViewController(Consts.PRIVILEGE_ADMIN);
        } else if (name.equalsIgnoreCase("/admin/changePassword")) {
            return new AdminChangePasswordController(Consts.PRIVILEGE_NONE);
        } else if (name.equalsIgnoreCase("/character/change_logs")) {
            return new ConsumerLogController(Consts.PRIVILEGE_NONE);
        }
        return null;
    }
}
