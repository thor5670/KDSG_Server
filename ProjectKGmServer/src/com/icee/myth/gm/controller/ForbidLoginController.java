/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icee.myth.gm.controller;

import com.icee.myth.gm.GmServer;
import com.icee.myth.gm.channelhandler.HttpServerHandler;
import com.icee.myth.gm.client.GameServerClient;
import com.icee.myth.gm.db.DbHandler;
import com.icee.myth.utils.Consts;

/**
 *
 * @author yangyi
 */
public class ForbidLoginController extends AbstractController {

    public ForbidLoginController(int privilege) {
        super(privilege);
    }

    public boolean  forbidLogin(String passport, int time) {
        boolean result  = false;
        DbHandler managerDbHandler = new DbHandler(GmServer.INSTANCE.managerDbHost, GmServer.INSTANCE.managerDbName);
        time = (int) (System.currentTimeMillis() / 1000 + time * 60);
        if(managerDbHandler.updateForbinLoginTime(passport, time)) {
            int cid = managerDbHandler.getCidByPassport(passport);
            for (GameServerClient gameServerClient : GmServer.INSTANCE.getGameServerClients().values()) {
                gameServerClient.forbidLogin(cid);
            }
            result = true;
        }
        managerDbHandler.close();
        return result;
    }

    @Override
    public void process() {
        Long time = (Long) jobj.get("forbidlogintime");
        String passport = (String) jobj.get("passport");
        if (passport != null && time != null) {
            if (forbidLogin(passport, (int) (long) time)) {
                dbHandler.insertGmOperateLog(userData.userName, Consts.GMOPERATELOG_FORBIDLOGIN, jobj.toJSONString());
                HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", "{\"result\":0}", null);
            } else {
                HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", "{\"result\":-1,\"error\":\"forbit login error\"}", null);
            }
        } else {
            HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", "{\"result\":-1,\"error\":\"param error\"}", null);
        }
    }
}
