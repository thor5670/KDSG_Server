/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icee.myth.gm.controller;

import com.icee.myth.gm.GmServer;
import com.icee.myth.gm.channelhandler.HttpServerHandler;
import com.icee.myth.gm.client.GameServerClient;
import com.icee.myth.utils.Consts;

/**
 *
 * @author yangyi
 */
public class RemoveItemController extends AbstractController {

    public RemoveItemController(int privilege) {
        super(privilege);
    }

    @Override
    public void process() {
        Long serverId = (Long) jobj.get("server");
        Long cid = (Long) jobj.get("cid");
        Long itemId = (Long) jobj.get("itemid");
        Long itemNum = (Long) jobj.get("itemnum");
        GameServerClient gameServerClient = null;
        if (serverId != null) {
            gameServerClient = GmServer.INSTANCE.getServer(serverId.intValue());
        }
        if (gameServerClient != null && cid != null && itemId != null && itemNum != null) {
            if (gameServerClient.removeItem((int) (long) cid, (int) (long) itemId, (int) (long) itemNum)) {
                dbHandler.insertGmOperateLog(userData.userName, Consts.GMOPERATELOG_REMOVEITEM, jobj.toJSONString());
                HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", "{\"result\":0}", null);
            } else {
                HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", "{\"result\":-1,\"error\":\"remove item error\"}", null);
            }
        } else {
            HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", "{\"result\":-1,\"error\":\"param error\"}", null);
        }
    }
}
