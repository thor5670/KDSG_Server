/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.gm.controller;

import com.icee.myth.gm.channelhandler.HttpServerHandler;
import com.icee.myth.utils.Consts;

/**
 *
 * @author yangyi
 */
public class AdminChangePasswordController extends AbstractController{

     public AdminChangePasswordController(int privilege){
        super(privilege);
    }

    @Override
    public void process() {
        String password = (String) jobj.get("password");
       
        if(password != null){
            if(dbHandler.addUser(userData.userName,password,userData.privilege)){
                dbHandler.insertGmOperateLog(userData.userName,Consts.GMOPERATELOG_CHANGEPASSWODR,jobj.toJSONString());
                HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", "{\"result\":0}", null);
            } else {
                HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", "{\"result\":-1,\"error\":\"change password error\"}", null);
            }
        } else{
            HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", "{\"result\":-1,\"error\":\"param error\"}", null);
        }
    }
}
