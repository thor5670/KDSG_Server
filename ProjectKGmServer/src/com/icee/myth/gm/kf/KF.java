/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icee.myth.gm.kf;

import com.icee.myth.gm.GmServer;
import com.icee.myth.gm.activity.BroadcastInfo;
import com.icee.myth.gm.client.GameServerClient;
import java.util.List;

/**
 *
 * @author yangyi
 */
public class KF {

    public final int serverId;
    public BroadcastInfo broadcast;
    protected long prevTime;    // 最后一次update的时间

    public KF(KFStaticInfo staticInfo, long currentTime) {
        this.serverId = staticInfo.serverId;
        // 广播
        broadcast = staticInfo.broadcastInfo;
        this.prevTime = currentTime;
    }

    // 当活动结束返回fasle
    public boolean update(long currentTime) {
        List<Integer> onlinePlayerIds = null;

        // 处理广播
        if (broadcast != null) {
            if (prevTime < broadcast.endTime) { // 判断该广播是否结束
                if (prevTime >= broadcast.startTime) {   // 判断该广播是否开始（注意：不能用currentTime来作开始判断）
                    // 查看当前时间与先前时间是否跨越某一逻辑点
                    int prevLogicPoint = (int) ((prevTime - broadcast.startTime) / broadcast.period);
                    int currentLogicPoint = (int) ((currentTime - broadcast.startTime) / broadcast.period);
                    if (currentLogicPoint > prevLogicPoint) {
                        // 执行广播逻辑
                        if (serverId >= 0) {
                            GameServerClient gameServerClient = GmServer.INSTANCE.getServer(serverId);
                            if (gameServerClient != null) {
                                gameServerClient.broadcast(broadcast.message);
                            }
                        } else {
                            for (GameServerClient gameServerClient : GmServer.INSTANCE.getGameServerClients().values()) {
                                gameServerClient.broadcast(broadcast.message);
                            }
                        }
                    }
                }
            } else {
                broadcast = null;
            }
        }
        prevTime = currentTime;

        return (broadcast != null);
    }
}
