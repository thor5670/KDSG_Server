/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.gm.api;

import java.util.Properties;

import net.oauth.OAuthMessage;

import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import com.icee.myth.gm.GmServer;
import com.icee.myth.utils.LogConsts;
import com.icee.myth.utils.MLogger;
import com.icee.myth.utils.StackTraceUtil;

/**
 *
 * @author yangyi
 */
public class PlatformUidTransform {
    
    public static String puidToPassport(String puid){
        String passport = "";
        OAuthHelper oAuthHelper = GmServer.INSTANCE.oAuthHelper;
        Properties paramProps = new Properties();
        paramProps.setProperty("oauth_token", "");
        paramProps.setProperty("platform_code", GmServer.INSTANCE.platform);
        paramProps.setProperty("platform_uid", puid);

        try {
            MLogger.getlogger().debuglog(LogConsts.LOGLEVEL_DEBUG, "puid to passport " + puid + "url" + GmServer.INSTANCE.queryUseridUrl);

            OAuthMessage response = oAuthHelper.sendRequest(paramProps, GmServer.INSTANCE.queryUseridUrl);
            String content = response.readBodyAsString();
            JSONObject jobj = (JSONObject) JSONValue.parse(content);
            if (jobj != null) {
                Long statusObj = (Long) jobj.get("status");
                if(statusObj != null && statusObj == 0){
                    JSONObject data = (JSONObject) jobj.get("data");
                    if (data != null) {
                        passport = (String) data.get("userid");
                    }
                }
            }
            MLogger.getlogger().debuglog(LogConsts.LOGLEVEL_DEBUG, "puid to passport " + puid + "content " + content);
        } catch(Exception e){
            MLogger.getlogger().debuglog(LogConsts.LOGLEVEL_DEBUG,  StackTraceUtil.getStackTrace(e));
        }
        return passport;
    }

    public static String passportToPuid(String passport){
        String puid = "";
        OAuthHelper oAuthHelper = GmServer.INSTANCE.oAuthHelper;
        Properties paramProps = new Properties();
        paramProps.setProperty("oauth_token", "");
        paramProps.setProperty("userid", passport);
        
        try {
            MLogger.getlogger().debuglog(LogConsts.LOGLEVEL_DEBUG, "passport to puid " + passport);

            OAuthMessage response = oAuthHelper.sendRequest(paramProps, GmServer.INSTANCE.queryPlatformUidUrl);
            String content = response.readBodyAsString();
            JSONObject jobj = (JSONObject) JSONValue.parse(content);
            if (jobj != null) {
                Long statusObj = (Long) jobj.get("status");
                if(statusObj != null && statusObj == 0){
                    JSONObject data = (JSONObject) jobj.get("data");
                    if (data != null) {
                        puid = (String) data.get("platform_uid");
                    }
                }
            }
            MLogger.getlogger().debuglog(LogConsts.LOGLEVEL_DEBUG, "passport to puid " + passport + "content " + content) ;
        } catch(Exception e){
            MLogger.getlogger().debuglog(LogConsts.LOGLEVEL_DEBUG,  StackTraceUtil.getStackTrace(e));
        }
        return puid;
    }

}
