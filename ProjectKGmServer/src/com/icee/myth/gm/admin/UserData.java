/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.gm.admin;


/**
 *
 * @author liuxianke
 */
public class UserData {
    public final String userName;   // 用户名
    public final String passwd;     // 密码
    public final int privilege;     // 权限
    public int sessionId;   // 会话号
    public long sessionExpireTime;   // 会话过期时间

    public UserData(String userName, String passwd, int privilege) {
        this.userName = userName;
        this.passwd = passwd;
        this.privilege = privilege;
    }
}
