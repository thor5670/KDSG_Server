/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icee.myth.gm.activity;

/**
 *
 * @author liuxianke
 */
public class RewardItemInfo {

    public int itemType;    // 奖品类型（0：卡片 1：物品）
    public int itemId;      // 奖品id
    public int level;       // 等级（当类型为卡片时有效）
    public int num;         // 数量
}
