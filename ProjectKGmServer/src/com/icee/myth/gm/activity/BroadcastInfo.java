/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.gm.activity;

import com.icee.myth.utils.LogConsts;
import com.icee.myth.utils.MLogger;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 *
 * @author liuxianke
 */
public class BroadcastInfo {
    public long startTime;      // 开始时间（精确到毫秒）
    public long endTime;        // 结束时间（精确到毫秒）
    public long period;         // 周期
    public String message;      // 通告消息
    
    public BroadcastInfo(JSONBroadcastInfo jsonBroadcastInfo) {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            startTime = format.parse(jsonBroadcastInfo.startTime).getTime();
            endTime = format.parse(jsonBroadcastInfo.endTime).getTime();
        } catch (ParseException ex) {
            MLogger.getlogger().debuglog(LogConsts.LOGLEVEL_ERROR, "Parse broadcast info time format error.");
            startTime = Long.MAX_VALUE;
            endTime = 0;
        }
        
        period = jsonBroadcastInfo.period;
        message = jsonBroadcastInfo.message;
    }
}
