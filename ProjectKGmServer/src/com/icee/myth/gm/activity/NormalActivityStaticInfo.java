/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icee.myth.gm.activity;

import java.util.Date;

/**
 *
 * @author liuxianke
 */
public class NormalActivityStaticInfo {

    public int id;                  // 活动号（注意：不能与任何曾经做过的活动号重复）
    public int recommend;           // 推荐标识
    public String title;            // 标题
    public String icon;             // 活动图标
    public Date startTime;          // 开始时间
    public Date endTime;            // 结束时间
    public long startRelativeTime;  // 相当开服的开始时间
    public long endRelativeTime;    // 相对开服的结束时间
    public boolean forever;         // 是否永久
    public NormalActivityItem[] items;  // 活动项目表
}
