/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.gm.activity;

/**
 *
 * @author liuxianke
 */
public class ActivityStaticInfo {
    public final int serverId;    // 活动所在服务器
    public final BroadcastInfo[] broadcastInfos;  // 广播信息

    public ActivityStaticInfo(JSONActivityTemplate template) {
        serverId = template.serverId;

        if ((template.broadcastInfos != null) && (template.broadcastInfos.length > 0)) {
            broadcastInfos = new BroadcastInfo[template.broadcastInfos.length];
            for (int i=0; i<template.broadcastInfos.length; i++) {
                broadcastInfos[i] = new BroadcastInfo(template.broadcastInfos[i]);
            }
        } else {
            broadcastInfos = null;
        }
    }
}
