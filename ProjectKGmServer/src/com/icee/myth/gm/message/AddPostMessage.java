/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.gm.message;

/**
 *
 * @author yangyi
 */
public class AddPostMessage extends SimpleMessage{
    public final int serverId;
    public final String startTime;      // 开始时间，格式（yyyy-MM-dd HH:mm:ss）
    public final String endTime;        // 结束时间，格式（yyyy-MM-dd HH:mm:ss）
    public final int period;            // 周期（单位秒）
    public final String message;        // 通告消息
    
    public AddPostMessage(int serverId, String startTime, String endTime, int period, String message) {
        super(MessageType.ADDPOST);
        this.serverId = serverId;
        this.startTime = startTime;
        this.endTime = endTime;
        this.period = period;
        this.message = message;
    }
}
