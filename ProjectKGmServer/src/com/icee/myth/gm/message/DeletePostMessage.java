/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.gm.message;

/**
 *
 * @author yangyi
 */
public class DeletePostMessage extends SimpleMessage{
    public final int id;

    public DeletePostMessage(int id) {
        super(MessageType.DELETEPOST);
        this.id = id;
    }
}
