/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.gm.message;

/**
 *
 * @author yangyi
 */
public interface Message {

    public enum MessageType {
        ADDPOST,
        DELETEPOST
    }

    public MessageType getType();
}
