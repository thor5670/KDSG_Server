/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.gm.player;

import com.icee.myth.protobuf.ExternalCommonProtocol.ItemProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.ItemsProto;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author liuxianke
 */
public class Items {
    public final ArrayList<Item> items = new ArrayList<Item>();

    public Items(ItemsProto itemsProto) {
        List<ItemProto> itemProtos = itemsProto.getItemsList();
        for (ItemProto itemProto : itemProtos) {
            items.add(new Item(itemProto));
        }
    }
}
