/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.gm.player;

import com.icee.myth.protobuf.ExternalCommonProtocol.CardsProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.ItemsProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.SandboxProto;
import com.icee.myth.protobuf.InternalCommonProtocol.PlayerDetailProto;

/**
 *
 * @author liuxianke
 */
public class Human {
    public int id;
    public String name;
    public int lv;  // 等级
    public int exp; // 经验
    public int gold1;       // 黄金（充值）
    public int gold2;       // 黄金（非充值）
    public long silver;     // 白银
    public int energy;      // 体力
    public int vipLevel;
    public int vipExperience;
    
    public long totalOnlineTime;    // 累计在线时间
    public long createTime;         // 创建时间
    public long leaveTime;          // 最后一次离开时间

    public Items items;         // 物品
    public Cards cards;         // 卡片
    public SandBox sandBox;     // 阵型
    
    public Human(CharDetailInfo charDetailInfo){
        id = charDetailInfo.mid;
        name =  charDetailInfo.name;
        lv = charDetailInfo.level;
        exp = charDetailInfo.experience;
        gold1 = charDetailInfo.gold1;
        gold2 = charDetailInfo.gold2;
        silver = charDetailInfo.silver;
        energy = charDetailInfo.energy;
        vipLevel = charDetailInfo.vipLevel;
        vipExperience = charDetailInfo.vipExperience;

        totalOnlineTime = charDetailInfo.totalOnlineTime;
        createTime = charDetailInfo.createTime;
        leaveTime = charDetailInfo.leaveTime;

        PlayerDetailProto detail = charDetailInfo.detail;
        if(detail != null){
            CardsProto cardsProto = detail.getCards();
            cards = new Cards(cardsProto);

            ItemsProto itemsProto = detail.getItems();
            items = new Items(itemsProto);

            SandboxProto sandboxProto = detail.getSandbox();
            sandBox = new SandBox(sandboxProto);
        }
    }
}
