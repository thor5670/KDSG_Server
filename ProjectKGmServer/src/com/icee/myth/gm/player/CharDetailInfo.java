/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.gm.player;

import com.icee.myth.protobuf.InternalCommonProtocol.PlayerDetailProto;

/**
 * 玩家的详细角色信息
 * @author liuxianke
 */
public class CharDetailInfo {
    public int mid;
    public String name;
    public int gold1;
    public int gold2;
    public long silver;
    public int energy;
    public int level;
    public int experience;
    public int vipLevel;
    public int vipExperience;

    public long totalOnlineTime;    // 累计在线时间
    public long createTime;         // 创建时间
    public long leaveTime;          // 最后一次离开时间

    public PlayerDetailProto detail;
}
