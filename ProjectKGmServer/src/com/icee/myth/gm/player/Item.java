/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.gm.player;

import com.icee.myth.protobuf.ExternalCommonProtocol.ItemProto;

/**
 *
 * @author liuxianke
 */
public class Item {
    public int itemId;
    public int itemNum;

    Item(ItemProto itemProto) {
        this.itemId = itemProto.getId();
        this.itemNum = itemProto.getNum();
    }
}
