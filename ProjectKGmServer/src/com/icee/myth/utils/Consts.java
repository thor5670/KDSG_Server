/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icee.myth.utils;

/**
 *
 * @author liuxianke
 */
public class Consts {

    public static final int FIRST_HEARTBEAT_NUM = 99999;    //服务启动时，channel心跳初始值
    public static final int NORMAL_HEARTBEAT_NUM = 10;  //正常情况下，channel心跳最大值
    public static final int HEARTBEAT_PERIOD = 1000;    //心跳间隔时间，1秒
    public static final int RECONNECT_PERIOD = 2000;    //断线重连间隔，2秒
    public static final int DB_KEEPALIVE_INTERVAL = 3600000; // ms, 1 hour
    public static final String DB_KEEPALIVE_TEST_STATEMENT = "SELECT 1 FROM INFORMATION_SCHEMA.VIEWS";
    public static final int SESSION_EXPIRE_TIME = 60 * 60 * 1000;    // 1小时
    public static final int ALL_PLAYER_ID = -1;                    // 对服中所有玩家
    public static final String SERVERS_CONFIG_FILEPATH = "managerConfig.json";
    public static final String ACTIVITIES_CONFIG_FILEPATH = "activitiesConfig.json";
    public static final String KF_CONFIG_FILEPATH = "kfConfig.json";
//    public static final int BOOTSTRAP_TO_MANAGER_PORT = 15687;
    public static final int CONSOLE_COMMAND_STARTSERVER = 0;    // 控制台命令：启动服务
    public static final int CONSOLE_COMMAND_STOPSERVER = 1;     // 控制台命令：停止服务
    // TODO: 其他命令
    public static final int SERVICE_TYPE_CLUSTER = 0;   // 服务类型0：Cluster
    public static final int SERVICE_TYPE_LGW = 1;       // 服务类型1：LGW
    public static final int SERVICE_TYPE_GW = 2;        // 服务类型2：GW
    public static final int SERVICE_TYPE_MAP = 3;       // 服务类型3：Map
    public static final int SERVER_STATUS_UNKNOWN = 0;  //服务状态：未知
    public static final int SERVER_STATUS_BOOTING = 1;  //服务状态：正在启动
    public static final int SERVER_STATUS_RUNNING = 2;  //服务状态：运行
    public static final int SERVER_STATUS_CLOSING = 3;  //服务状态：正在关闭
    public static final int SERVER_STATUS_SHUTDOWN = 4;  //服务状态：关闭
    public static final int SERVER_STATUS_ABNORMALRUNNING = 5;  //服务状态：运行异常
    public static final int SERVER_STATUS_DUMMY = 6;    //服务状态：无状态
    public static final int ACTIVITY_PRIZE_PROBABILITY_RANGE = 10000;   //奖励概率范围
    public static final int ACTIVITY_TARGET_TYPE_ALLONLINE = 0; // 活动目标类型：所有在线玩家
    public static final int ACTIVITY_PRIZESGIVING_TYPE_PROBABILITY = 0;     // 活动奖励类型：概率奖励
    public static final int ACTIVITY_PRIZESGIVING_TYPE_FIXEDQUANTITY = 1;   // 活动奖励类型：定量奖励
    // TODO:其他活动目标类型定义
    public static final int REWARD_ITEM_TYPE_CARD = 0;  // 奖励物品类型：卡片
    public static final int REWARD_ITEM_TYPE_ITEM = 1;  // 奖励物品类型：卡片
    public static final int OK = 0;
    public static final int ERR = -1;
    public static final int PRIVILEGE_NONE = 0;
    public static final int PRIVILEGE_QUERY = 1;             //查询信息
    public static final int PRIVILEGE_MODIFY = 2;            //修改玩家信息
    public static final int PRIVILEGE_POST = 4;              //发公告
    public static final int PRIVILEGE_OPERRATE = 8;          //运维
    public static final int PRIVILEGE_FORBIDLOGINORTALK = 16;   //禁言，踢人
    public static final int PRIVILEGE_VIREOTHEROPERATELOG = 32;   //查看他人操作日志
    public static final int PRIVILEGE_ADMIN = 64;   //查看所有管理员并修改权限
    public static final int PRIVILEGE_COMPOSATE_NOREWARD = 128;   //发送补偿发送物品
//    public static final int GENERATETTIMERANDOM = 20;             //防止同时产生，移位后在后面加入随机数
//    public static final int PRODUCEKEYMAXRETRYTIMES = 2;
    //登录失败
    public static final int GETSESSION_ERR_FORBIDDEN_LOGIN = -1;    //玩家被禁止登陆
    public static final int GETSESSION_ERR_SERVER = -2;    //系统错误
    public static final int CHECKSESSION_ERR_SERVER = -1;    //没有这个session
    public static final int ERR_SERVER = -1;    //获取排名列表错误
    public static final int PARAMETER_SERVER = -100;
//    public static int MAXSESSIONNUM = 1000000;
//    public static int SESSIONPERIOD = 3600000;
    public static final int MAXSESSIONNUM = 3;
    public static final int SESSIONPERIOD = 360000;
    public static final int TOPLEVELNUM = 10;       //获取一个服里排名前几名的玩家
    public static final long LEVELRANKPERIOD = 5 * 60 * 1000;
    public static final int RPCSET_SILVER = 0;
    public static final int RPCSET_GOLD1 = 1;
    public static final int RPCSET_GOLD2 = 2;
    public static final int RPCSET_LEVEL = 3;
    public static final int RPCSET_ENERGY = 6;
    public static final int RPCSET_VIPEXP = 8;
    public static final int RPCEDIT_GOLD1 = 9;
    public static final int RPCEDIT_GOLD2 = 10;
    public static final int GMOPERATELOG_POST = 1;                              //发公告
    public static final int GMOPERATELOG_FORBIDTALK = 2;                        //禁言
    public static final int GMOPERATELOG_FORBIDLOGIN = 3;                       //禁止登陆
    public static final int GMOPERATELOG_PERMITLOGIN = 4;                       //允许登陆
    public static final int GMOPERATELOG_PERMITTALK = 5;                        //允许发言
    public static final int GMOPERATELOG_SETSILVER = 6;                         //修改白银
    public static final int GMOPERATELOG_SETGOLD1 = 7;                          //修改充值金币
    public static final int GMOPERATELOG_SETGOLD2 = 8;                          //修改系统金币
    public static final int GMOPERATELOG_SETENERGY = 9;                         //修改体力
    public static final int GMOPERATELOG_ADDITEM = 10;                          //增加物品
    public static final int GMOPERATELOG_REMOVEITEM = 11;                       //删除物品
    public static final int GMOPERATELOG_ADDCARD = 12;                          //增加卡片
    public static final int GMOPERATELOG_REMOVECARD = 13;                       //删除卡片
    public static final int GMOPERATELOG_CHANGE_SANDBOX = 14;                   //修改阵型
    public static final int GMOPERATELOG_ADDPERIODPOST = 16;                    //发周期性公告
    public static final int GMOPERATELOG_DELETEPERIODPOST = 17;                 //删除周期性公告
    public static final int GMOPERATELOG_MAIL = 18;                     //发补偿
    public static final int GMOPERATELOG_CHANGEPLAYERNAME = 19;                 //修改玩家名称
    public static final int GMOPERATELOG_MODFIYPRIVILEGE = 20;                  //修改管理员权限
    public static final int GMOPERATELOG_ADDUSE = 21;                           //增加管理员
    public static final int GMOPERATELOG_CHANGEPASSWODR = 22;                   //修改密码
    public static final int GMOPERATELOG_SETVIPEXP = 23;                        //修改Vip经验
    public static final int GMOPERATELOG_ADDNORMALACTIVITY = 24;                //添加活动
    public static final int GMOPERATELOG_DELETENORMALACTIVITY = 25;             //删除活动
    public static final int GMOPERATELOG_SET_CARD_DRAW_ACTIVITY = 26;           //添加卡包活动
    public static final int GMOPERATELOG_SET_STAGE_ACTIVITY = 27;               //添加关卡活动（关卡卡片掉率翻倍或体力减半）
    public static final int GMOPERATELOG_CHARGE_LOG = 28;                       //查询充值记录
    public static final int GMOPERATELOG_CONSUMER_LOG = 29;                     //查询消费记录

    public static final int FOSTERRANKNUM = 5;
}
