/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.utils;

/**
 *
 * @author liuxianke
 */
public class Consts {
    public static final int CONSOLE_COMMAND_RUN_SERVER = 0;      // 控制台命令：启动服务
    public static final int CONSOLE_COMMAND_SHUTDOWN_SERVER = 1; // 控制台命令：停止服务
    public static final int CONSOLE_COMMAND_SYNCCONFIG = 2;     // 控制台命令：同步配置文件
    public static final int CONSOLE_COMMAND_GET_SERVER_STATE = 3; // 控制台命令：获得服务状态
    public static final int CONSOLE_COMMAND_GM = 4;             // 控制台命令：gm指令
    public static final int CONSOLE_COMMAND_CLEAR_TEST_FLAG = 5;  // 控制台命令：服务器状态不为testing
    public static final int CONSOLE_COMMAND_SET_TEST_FLAG = 6;    // 控制台命令：服务器状态为testing
    public static final int CONSOLE_COMMAND_FORCE_SET_SERVER_TO_SHUTDOWN_STATUS = 7; // 控制台命令：强制设置服务器状态为关闭状态
    public static final int CONSOLE_COMMAND_LOAD_SERVER_CONFIG = 8; // 控制台命令：加载服务器配置
    public static final int CONSOLE_COMMAND_SET_DATE = 9;           // 控制台命令：设置机器系统日期
    public static final int CONSOLE_COMMAND_SET_TIME = 10;          // 控制台命令：设置机器系统时间
    public static final int CONSOLE_COMMAND_GET_DATETIME = 11;      // 控制台命令：获取机器系统日期和时间

    public static final int CONSOLE_COMMAND_DEBUG_RUNSERVER = 1000;  // 调试启动

    public static final int MILSECOND_8HOUR = 8*3600*1000;  // 8小时的毫秒数（用于时差）
    public static final int JET_LAG = MILSECOND_8HOUR;

    public static final int SERVER_STATUS_UNKNOWN = 0;  //服务状态：未知
    public static final int SERVER_STATUS_BOOTING = 1;  //服务状态：正在启动
    public static final int SERVER_STATUS_RUNNING = 2;  //服务状态：运行
    public static final int SERVER_STATUS_CLOSING = 3;  //服务状态：正在关闭
    public static final int SERVER_STATUS_SHUTDOWN = 4;  //服务状态：关闭
    public static final int SERVER_STATUS_ABNORMALRUNNING = 5;  //服务状态：运行异常
    public static final int SERVER_STATUS_DUMMY = 6;    //服务状态：无状态

    public static final int LOADSERVERCONFIG_ERROR_NOT_SHUTDOWN = 1;    // 加载服务器配置错误：未关闭
    public static final int LOADSERVERCONFIG_ERROR_NOT_FOUND = 2;       // 加载服务器配置错误：无配置
}
