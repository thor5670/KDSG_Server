/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.console.channelHandler;

import com.icee.myth.common.messageQueue.ServerMessageQueue;
import com.icee.myth.common.protobufMessage.ProtobufMessageType;
import com.icee.myth.console.message.serverMessage.builder.ConsoleMessageBuilder;
import com.icee.myth.protobuf.ConsoleToManagerProtocol.ServerStatuses;
import com.icee.myth.protobuf.ExternalCommonProtocol.IntValueProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.IntValuesProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.LongValueProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.VariableValueProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.VariableValuesProto;
import com.icee.myth.utils.LogConsts;
import com.icee.myth.utils.MLogger;
import com.icee.myth.utils.StackTraceUtil;
import java.io.IOException;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBufferInputStream;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.ChannelStateEvent;
import org.jboss.netty.channel.ExceptionEvent;
import org.jboss.netty.channel.MessageEvent;
import org.jboss.netty.channel.SimpleChannelUpstreamHandler;

/**
 *
 * @author liuxianke
 */
public class ConsoleToManagerHandler extends SimpleChannelUpstreamHandler {

    @Override
    public void channelConnected(
            ChannelHandlerContext ctx, ChannelStateEvent e) {
        // Add cluster channel connected message to message queue
        ServerMessageQueue.queue().offer(ConsoleMessageBuilder.buildManagerConnectMessage(e.getChannel()));
    }

    @Override
    public void channelClosed(
            ChannelHandlerContext ctx, ChannelStateEvent e) throws Exception {
        // Add cluster channel close message to message queue
        ServerMessageQueue.queue().offer(ConsoleMessageBuilder.buildManagerCloseMessage(e.getChannel()));
    }

    @Override
    public void messageReceived(
            ChannelHandlerContext ctx, MessageEvent e) {
        try {
            // add cluster channel received message to message queue
            ChannelBuffer cb = (ChannelBuffer) e.getMessage();
            short type = cb.readShort();
            int length = cb.readInt();

            switch (type) {
                case ProtobufMessageType.MANAGER2CONSOLE_SERVER_START_OK: {
                    IntValueProto intValue = IntValueProto.getDefaultInstance().newBuilderForType().mergeFrom(new ChannelBufferInputStream(cb)).build();
                    ServerMessageQueue.queue().offer(ConsoleMessageBuilder.buildServerStartOKMessage(intValue.getValue()));
                    break;
                }
                case ProtobufMessageType.MANAGER2CONSOLE_SERVER_SHUTDOWN_OK: {
                    IntValueProto intValue = IntValueProto.getDefaultInstance().newBuilderForType().mergeFrom(new ChannelBufferInputStream(cb)).build();
                    ServerMessageQueue.queue().offer(ConsoleMessageBuilder.buildServerShutdownOKMessage(intValue.getValue()));
                    break;
                }
                case ProtobufMessageType.MANAGER2CONSOLE_SYNCCONFIG_RETURN: {
                    IntValueProto intValue = IntValueProto.getDefaultInstance().newBuilderForType().mergeFrom(new ChannelBufferInputStream(cb)).build();
                    ServerMessageQueue.queue().offer(ConsoleMessageBuilder.buildSyncConfigReturnMessage(intValue.getValue()));
                    break;
                }
                case ProtobufMessageType.MANAGER2CONSOLE_GET_SERVER_STATUS_RETURN: {
                    ServerStatuses serverStatuses = ServerStatuses.getDefaultInstance().newBuilderForType().mergeFrom(new ChannelBufferInputStream(cb)).build();
                    ServerMessageQueue.queue().offer(ConsoleMessageBuilder.buildGetServerStatusReturnMessage(serverStatuses));
                    break;
                }
                case ProtobufMessageType.MANAGER2CONSOLE_FORCE_SET_SERVER_SHUTDOWN_STATUS_OK: {
                    IntValueProto intValue = IntValueProto.getDefaultInstance().newBuilderForType().mergeFrom(new ChannelBufferInputStream(cb)).build();
                    ServerMessageQueue.queue().offer(ConsoleMessageBuilder.buildForceSetServerShutdownStatusOKMessage(intValue.getValue()));
                    break;
                }
                case ProtobufMessageType.MANAGER2CONSOLE_FORCE_SET_SERVER_SHUTDOWN_STATUS_ERROR: {
                    IntValueProto intValue = IntValueProto.getDefaultInstance().newBuilderForType().mergeFrom(new ChannelBufferInputStream(cb)).build();
                    ServerMessageQueue.queue().offer(ConsoleMessageBuilder.buildForceSetServerShutdownStatusErrorMessage(intValue.getValue()));
                    break;
                }
                case ProtobufMessageType.MANAGER2CONSOLE_LOAD_SERVER_CONFIG_OK: {
                    IntValueProto intValue = IntValueProto.getDefaultInstance().newBuilderForType().mergeFrom(new ChannelBufferInputStream(cb)).build();
                    ServerMessageQueue.queue().offer(ConsoleMessageBuilder.buildLoadServerConfigOKMessage(intValue.getValue()));
                    break;
                }
                case ProtobufMessageType.MANAGER2CONSOLE_LOAD_SERVER_CONFIG_ERROR: {
                    VariableValueProto variableValue = VariableValueProto.getDefaultInstance().newBuilderForType().mergeFrom(new ChannelBufferInputStream(cb)).build();
                    ServerMessageQueue.queue().offer(ConsoleMessageBuilder.buildLoadServerConfigErrorMessage(variableValue.getId(), (int)variableValue.getValue()));
                    break;
                }
                case ProtobufMessageType.MANAGER2CONSOLE_BATTLE_WIN_RATE_RESULT: {
                    IntValuesProto intValues = IntValuesProto.getDefaultInstance().newBuilderForType().mergeFrom(new ChannelBufferInputStream(cb)).build();
                    ServerMessageQueue.queue().offer(ConsoleMessageBuilder.buildBattleWinRateResultMessage(intValues.getValues(0), intValues.getValues(1), intValues.getValues(2)));
                    break;
                }
                case ProtobufMessageType.MANAGER2CONSOLE_CARD_DRAW_RATE_RESULT: {
                    VariableValuesProto variableValuesProto = VariableValuesProto.getDefaultInstance().newBuilderForType().mergeFrom(new ChannelBufferInputStream(cb)).build();
                    ServerMessageQueue.queue().offer(ConsoleMessageBuilder.buildCardDrawRateResultMessage(variableValuesProto));
                    break;
                }
                case ProtobufMessageType.MANAGER2CONSOLE_UNINIT_OCCUPY_INFO: {
                    IntValuesProto intValues = IntValuesProto.getDefaultInstance().newBuilderForType().mergeFrom(new ChannelBufferInputStream(cb)).build();
                    ServerMessageQueue.queue().offer(ConsoleMessageBuilder.buildUninitOccupyInfoMessage(intValues));
                    break;
                }
                case ProtobufMessageType.MANAGER2CONSOLE_FIGHTING_OCCUPY_INFO: {
                    IntValuesProto intValues = IntValuesProto.getDefaultInstance().newBuilderForType().mergeFrom(new ChannelBufferInputStream(cb)).build();
                    ServerMessageQueue.queue().offer(ConsoleMessageBuilder.buildFightingOccupyInfoMessage(intValues));
                    break;
                }
                case ProtobufMessageType.MANAGER2CONSOLE_DATE_TIME: {
                    LongValueProto longValueProto = LongValueProto.getDefaultInstance().newBuilderForType().mergeFrom(new ChannelBufferInputStream(cb)).build();
                    ServerMessageQueue.queue().offer(ConsoleMessageBuilder.buildDateTimeMessage(longValueProto.getValue()));
                    break;
                }
                // TODO: other message
            }
        } catch (IOException ex) {
            MLogger.getlogger().log(LogConsts.LOGTYPE_NETERR, StackTraceUtil.getStackTrace(ex));
        }
    }

    @Override
    public void exceptionCaught(
            ChannelHandlerContext ctx, ExceptionEvent e) {

        MLogger.getlogger().log(LogConsts.LOGTYPE_NETERR, StackTraceUtil.getStackTrace(e.getCause()));
        // TODO: 是否需要关闭与Cluster的连接
//        e.getChannel().close();
    }
}
