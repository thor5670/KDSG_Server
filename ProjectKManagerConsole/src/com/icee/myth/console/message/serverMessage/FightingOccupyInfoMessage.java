/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.console.message.serverMessage;

import com.icee.myth.common.message.serverMessage.SimpleMessage;
import com.icee.myth.protobuf.ExternalCommonProtocol.IntValuesProto;

/**
 *
 * @author liuxianke
 */
public class FightingOccupyInfoMessage extends SimpleMessage {
    public final IntValuesProto intValuesProto;

    public FightingOccupyInfoMessage(IntValuesProto intValuesProto) {
        super(MessageType.CONSOLE_FIGHTING_OCCUPY_INFO);

        this.intValuesProto = intValuesProto;
    }
}
