/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.console.message.serverMessage.builder;

import com.icee.myth.common.message.serverMessage.InternalChannelMessage;
import com.icee.myth.common.message.serverMessage.Message;
import com.icee.myth.common.message.serverMessage.Message.MessageType;
import com.icee.myth.console.message.serverMessage.BattleWinRateResultMessage;
import com.icee.myth.console.message.serverMessage.CardDrawRateResultMessage;
import com.icee.myth.console.message.serverMessage.CommandConsoleMessage;
import com.icee.myth.console.message.serverMessage.DateTimeMessage;
import com.icee.myth.console.message.serverMessage.FightingOccupyInfoMessage;
import com.icee.myth.console.message.serverMessage.GetServerStatusReturnConsoleMessage;
import com.icee.myth.console.message.serverMessage.LoadServerConfigErrorConsoleMessage;
import com.icee.myth.console.message.serverMessage.ServerIdConsoleMessage;
import com.icee.myth.console.message.serverMessage.SyncConfigReturnConsoleMessage;
import com.icee.myth.console.message.serverMessage.UninitOccupyInfoMessage;
import com.icee.myth.protobuf.ConsoleToManagerProtocol.ServerStatuses;
import com.icee.myth.protobuf.ExternalCommonProtocol.IntValuesProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.VariableValuesProto;
import java.util.ArrayList;
import org.jboss.netty.channel.Channel;

/**
 *
 * @author liuxianke
 */
public class ConsoleMessageBuilder {

    public static Message buildManagerConnectMessage(Channel channel) {
        return new InternalChannelMessage(MessageType.CONSOLE_MANAGER_CONNECT, channel);
    }

    public static Message buildManagerCloseMessage(Channel channel) {
        return new InternalChannelMessage(MessageType.CONSOLE_MANAGER_CLOSE, channel);
    }

    public static Message buildCommandMessage(int cmdOpcode, ArrayList<String> strArgs) {
        return new CommandConsoleMessage(cmdOpcode, strArgs);
    }

    public static Message buildServerStartOKMessage(int serverId) {
        return new ServerIdConsoleMessage(MessageType.CONSOLE_SERVER_START_OK, serverId);
    }

    public static Message buildServerShutdownOKMessage(int serverId) {
        return new ServerIdConsoleMessage(MessageType.CONSOLE_SERVER_SHUTDOWN_OK, serverId);
    }

    public static Message buildSyncConfigReturnMessage(int result) {
        return new SyncConfigReturnConsoleMessage(result);
    }

    public static Message buildGetServerStatusReturnMessage(ServerStatuses statuses) {
        return new GetServerStatusReturnConsoleMessage(statuses);
    }

    public static Message buildForceSetServerShutdownStatusOKMessage(int serverId) {
        return new ServerIdConsoleMessage(MessageType.CONSOLE_FORCE_SET_SERVER_SHUTDOWN_STATUS_OK, serverId);
    }

    public static Message buildForceSetServerShutdownStatusErrorMessage(int serverId) {
        return new ServerIdConsoleMessage(MessageType.CONSOLE_FORCE_SET_SERVER_SHUTDOWN_STATUS_ERROR, serverId);
    }

    public static Message buildLoadServerConfigOKMessage(int serverId) {
        return new ServerIdConsoleMessage(MessageType.CONSOLE_LOAD_SERVER_CONFIG_OK, serverId);
    }

    public static Message buildLoadServerConfigErrorMessage(int serverId, int errorCode) {
        return new LoadServerConfigErrorConsoleMessage(serverId, errorCode);
    }

    public static Message buildBattleWinRateResultMessage(int winNum, int lostNum, int drawNum) {
        return new BattleWinRateResultMessage(winNum, lostNum, drawNum);
    }

    public static Message buildCardDrawRateResultMessage(VariableValuesProto result) {
        return new CardDrawRateResultMessage(result);
    }

    public static Message buildUninitOccupyInfoMessage(IntValuesProto intValuesProto) {
        return new UninitOccupyInfoMessage(intValuesProto);
    }

    public static Message buildFightingOccupyInfoMessage(IntValuesProto intValuesProto) {
        return new FightingOccupyInfoMessage(intValuesProto);
    }

    public static Message buildDateTimeMessage(long datetime) {
        return new DateTimeMessage(datetime);
    }
}
