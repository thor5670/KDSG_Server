/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.console.message.serverMessage;

import com.icee.myth.common.message.serverMessage.SimpleMessage;
import com.icee.myth.protobuf.ExternalCommonProtocol.VariableValuesProto;

/**
 *
 * @author liuxianke
 */
public class CardDrawRateResultMessage extends SimpleMessage {
    public final VariableValuesProto result;

    public CardDrawRateResultMessage(VariableValuesProto result) {
        super(MessageType.CONSOLE_CARD_DRAW_RATE_RESULT);

        this.result = result;
    }
}
