/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.console.message.serverMessage;

/**
 *
 * @author liuxianke
 */
public class LoadServerConfigErrorConsoleMessage extends ServerIdConsoleMessage {
    public final int errorCode;

    public LoadServerConfigErrorConsoleMessage(int serverId, int errorCode) {
        super(MessageType.CONSOLE_LOAD_SERVER_CONFIG_ERROR, serverId);

        this.errorCode = errorCode;
    }
}
