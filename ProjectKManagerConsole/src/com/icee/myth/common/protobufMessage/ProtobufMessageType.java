/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.common.protobufMessage;

/**
 *
 * @author liuxianke
 */
public class ProtobufMessageType {
    //TODO: 
    //values of type
    public static final int MANAGER2CONSOLE_SERVER_START_OK = 12000;
    public static final int MANAGER2CONSOLE_SERVER_SHUTDOWN_OK = 12001;
    public static final int MANAGER2CONSOLE_SYNCCONFIG_RETURN = 12002;
    public static final int MANAGER2CONSOLE_GET_SERVER_STATUS_RETURN = 12003;
    public static final int MANAGER2CONSOLE_FORCE_SET_SERVER_SHUTDOWN_STATUS_OK = 12004;
    public static final int MANAGER2CONSOLE_FORCE_SET_SERVER_SHUTDOWN_STATUS_ERROR = 12005;
    public static final int MANAGER2CONSOLE_LOAD_SERVER_CONFIG_OK = 12006;
    public static final int MANAGER2CONSOLE_LOAD_SERVER_CONFIG_ERROR = 12007;
    public static final int MANAGER2CONSOLE_BATTLE_WIN_RATE_RESULT = 12008;
    public static final int MANAGER2CONSOLE_CARD_DRAW_RATE_RESULT = 12009;
    public static final int MANAGER2CONSOLE_UNINIT_OCCUPY_INFO = 12010;
    public static final int MANAGER2CONSOLE_FIGHTING_OCCUPY_INFO = 12011;
    public static final int MANAGER2CONSOLE_DATE_TIME = 12012;

    public static final int CONSOLE2MANAGER_RUN_SERVER = 13000;
    public static final int CONSOLE2MANAGER_SHUTDOWN_SERVER = 13001;
    public static final int CONSOLE2MANAGER_DEBUG_RUN_SERVER = 13002;
    public static final int CONSOLE2MANAGER_SYNCCONFIG = 13003;
    public static final int CONSOLE2MANAGER_GET_SERVER_STATUS = 13004;
    public static final int CONSOLE2MANAGER_GM = 13005;
    public static final int CONSOLE2MANAGER_CLEAR_TEST_FLAG = 13006;
    public static final int CONSOLE2MANAGER_SET_TEST_FLAG = 13007;
    public static final int CONSOLE2MANAGER_FORCE_SET_SERVER_SHUTDOWN_STATUS = 13008;
    public static final int CONSOLE2MANAGER_LOAD_SERVER_CONFIG = 13009;
    public static final int CONSOLE2MANAGER_SET_SYSTEM_DATE = 13010;
    public static final int CONSOLE2MANAGER_SET_SYSTEM_TIME = 13011;
    public static final int CONSOLE2MANAGER_GET_SYSTEM_DATETIME = 13012;
}
